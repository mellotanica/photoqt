#ifndef MANAGETAGS_H
#define MANAGETAGS_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QUrl>
#include <QImageReader>

#ifdef EXIV2
#include <exiv2/image.hpp>
#include <exiv2/exif.hpp>
#endif

class ManageTags : public QObject {

    Q_OBJECT

public:
    explicit ManageTags(QObject *parent = 0);

    Q_INVOKABLE QStringList getExifCommentTags(QString path);
    Q_INVOKABLE bool setExifCommentTags(QString path, QStringList tags);
};


#endif // MANAGETAGS_H
