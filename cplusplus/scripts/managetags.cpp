#include "managetags.h"

ManageTags::ManageTags(QObject *parent) : QObject(parent) {

}


QStringList ManageTags::getExifCommentTags(QString path){

    QStringList tagList;

    // Clean path
    if(path.startsWith("image://full/"))
        path = path.remove(0,12);

    path = QUrl::fromPercentEncoding(path.toLatin1());
    QFileInfo info(path);

    if(!QFile(path).exists()) {

        tagList << "e";
        return tagList;

    } else {

        tagList << "v";

        // These formats known by PhotoQt are supported by exiv2
        QStringList formats;
        formats << "jpeg" << "jpg" << "tif" << "tiff"
            << "png" << "psd" << "jpeg2000" << "jp2"
            << "j2k" << "jpc" << "jpf" << "jpx"
            << "jpm" << "mj2" << "bmp" << "bitmap"
            << "gif" << "tga";

        // "Unsupported"
        if(!formats.contains(info.suffix().toLower())) {

            tagList << "u";
            return tagList;

        // "Supported"
        } else {

            tagList << "s";

#ifdef EXIV2

            // Obtain METADATA

            Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(path.toStdString());
            image->readMetadata();

            /*******************
            * Obtain EXIF data *
            ********************/

            Exiv2::ExifData &exifData = image->exifData();
            Exiv2::ExifData::const_iterator exifEnd = exifData.end();
            for (Exiv2::ExifData::const_iterator it_exif = exifData.begin(); it_exif != exifEnd; ++it_exif) {

                // Key/Value
                QString key = QString::fromStdString(it_exif->key());

                if(!key.compare("Exif.Photo.UserComment")){

                    QString value = QString::fromStdString(Exiv2::toString(it_exif->value())).replace(QRegExp("charset=\"?[A-Za-z]+\"? {"),"").replace('{',"");
                    tagList << value.split('}', QString::SkipEmptyParts);

                }

            }

#endif

            return tagList;

        }
    }
}

//TODO: replace return with meaningfull value
bool ManageTags::setExifCommentTags(QString path, QStringList tags){

    // Clean path
    if(path.startsWith("image://full/"))
        path = path.remove(0,12);

    path = QUrl::fromPercentEncoding(path.toLatin1());

    QString tagString = "";
    for(int i = 0; i < tags.size(); i++){
        tagString.append("{").append(tags.at(i)).append("}");
    }

#ifdef EXIV2

    Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(path.toStdString());
    image->readMetadata();

    Exiv2::ExifData &exifData = image->exifData();

    exifData["Exif.Photo.UserComment"] = tagString.toStdString();

    image->writeMetadata();

#endif

    return true;
}
