<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/fadein/About.qml" line="113"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>O PhotoQt é um visualizador de imagens criado com o intuito de ser elegante e ao mesmo tempo fácil de utilizar.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="115"/>
        <source>With PhotoQt I try to be different than other image viewers (after all, there are plenty of good image viewers already out there). Its interface is kept very simple, yet there is an abundance of settings to customize the look and feel to make PhotoQt YOUR image viewer.</source>
        <translation>Com o PhotoQt, tentámos fazer algo diferente (afinal de contas, existem imensos visualizadores de imagem com qualidade). A interface é simples mas existem diversas definições que podem ser alteradas para personalizar o aspeto e o comportamento e adaptá-lo ao utilizador.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="117"/>
        <source>I&apos;m not a trained programmer. I&apos;m a simple Maths student that loves doing stuff like this. Most of my programming knowledge I taught myself over the past 10-ish years, and it has been developing a lot since I started PhotoQt. During my studies in university I learned a lot about the basics of programming that I was missing. And simply working on PhotoQt gave me a lot of invaluable experience. So the code of PhotoQt might in places not quite be done in the best of ways, but I think it&apos;s getting better and better with each release.</source>
        <translation>Eu não sou um programador nato. Sou um estudante de matemática que gosta de aprender coisas novas. Maior parte dos meus conhecimentos de programação foram, em grande parte e nos últimos 10 anos, devidos ao desenvolvimento do PhotoQt. Durante a minha incursão na faculdade, aprendi um pouco de programação. Seguidamente, decidi iniciar a criação do PhotoQt, que me deu imenso prazer e conhecimento. No entanto, o código do PhotoQt pode, em algumas circunstâncias, não estar na melhor forma mas acho que a cada nova versão, tem vindo a melhorar.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="121"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>Não se esqueça de visitar o nosso sítio web:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="123"/>
        <source>If you find a bug or if you have a question or suggestion, tell me. I&apos;m open to any feedback I get :)</source>
        <translation>Se encontrar erros, tiver uma questão ou sugestão, escreva-me. Estou disponível para esclarecimentos :-)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="174"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don&apos;t forget to add the %1 in your translation!!</extracomment>
        <translation>Gostaria de integrar a equipa de desenvolvimento ou ajudar a traduzir o PhotoQt? Escreva-me uma mensagem (%1) ou consulte a nossa página de traduções no Transifex:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="119"/>
        <source>I heard a number of times people saying, that PhotoQt is a &apos;copy&apos; of Picasa&apos;s image viewer. Well, it&apos;s not. In fact, I myself have never used Picasa. I have seen it in use though by others, and I can&apos;t deny that it influenced the basic design idea a little. But I&apos;m not trying to do something &apos;like Picasa&apos;. I try to do my own thing, and to do it as good as I can.</source>
        <translation>Diversas vezes ouvi que o PhotoQt é uma cópia do visualizador de imagens Picasa. Posso afirmar que tal não corresponde à verdade. Nunca utilizei o Picasa mas já o vi a ser utilizado por outras pessoas e posso ter sido um pouco influenciado pela ideia do seu aspeto, mas não estou a tentar criar algo semelhante. Eu tento fazer as coisas à minha maneira e da melhor forma possível. </translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="158"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation>Muito obrigado a todas as pessoas que participaram no desenvolvimento ou tradução do PhotoQt!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>website:</source>
        <translation>sítio web:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>Licensed under GPLv2 or later, without any guarantee</source>
        <translation>Licenciado nos termos da GPLv2 ou mais recente mas sem qualquer garantia</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="52"/>
        <source>Move:</source>
        <extracomment>as in: &quot;Move file...&quot;</extracomment>
        <translation>Mover:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="62"/>
        <source>Previous</source>
        <extracomment>Go to previous file</extracomment>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="68"/>
        <source>Next</source>
        <extracomment>Go to next file</extracomment>
        <translation>Seguinte</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="99"/>
        <source>Rotate:</source>
        <extracomment>As in: Rotate file</extracomment>
        <translation>Rotação:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="104"/>
        <source>Left</source>
        <extracomment>As in: rotate LEFT</extracomment>
        <translation>Esquerda</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="111"/>
        <source>Right</source>
        <extracomment>As in: Rotate RIGHT</extracomment>
        <translation>Direita</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="140"/>
        <source>Flip:</source>
        <extracomment>As in: Flip file</extracomment>
        <translation>Inversão:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="145"/>
        <source>Horizontal</source>
        <extracomment>As in: Flip file HORIZONTALLY</extracomment>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="152"/>
        <source>Vertical</source>
        <extracomment>As in: Flip file VERTICALLY</extracomment>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="180"/>
        <source>Zoom:</source>
        <extracomment>Zoom file</extracomment>
        <translation>Ampliação:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="185"/>
        <source>In</source>
        <extracomment>As in: Zoom IN</extracomment>
        <translation>Ampliar</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="191"/>
        <source>Out</source>
        <translation>Reduzir</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="197"/>
        <source>Actual</source>
        <extracomment>As in: Zoom to ACTUAL size</extracomment>
        <translation>Atual</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="203"/>
        <source>Reset</source>
        <extracomment>As in: Reset zoom</extracomment>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="224"/>
        <source>Scale Image</source>
        <translation>Ajustar imagem</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="234"/>
        <source>Open in default File Manager</source>
        <translation>Abrir no gestor de ficheiros</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="266"/>
        <source>Rename File</source>
        <translation>Mudar nome</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="276"/>
        <source>Delete File</source>
        <translation>Eliminar ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="301"/>
        <source>Copy File</source>
        <translation>Copiar ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="312"/>
        <source>Move File</source>
        <translation>Mover ficheiro</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="11"/>
        <source>Confirm me?</source>
        <translation>Confirma?</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="12"/>
        <source>Do you really want to do this?</source>
        <translation>Deseja mesmo efetuar esta ação?</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="13"/>
        <source>Yes, do it</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="14"/>
        <source>No, don&apos;t</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="110"/>
        <source>Don&apos;t ask again</source>
        <translation>Não perguntar novamente</translation>
    </message>
</context>
<context>
    <name>CustomDetectShortcut</name>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="72"/>
        <source>Detect key combination</source>
        <translation>Detetar combinação de teclas</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="90"/>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="127"/>
        <source>Press keys</source>
        <translation>Prima as teclas</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="107"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>CustomExternalCommand</name>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="71"/>
        <source>External Command</source>
        <translation>Comando externo</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (with path)</source>
        <translation>ficheiro atual (com caminho)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (without path)</source>
        <translation>ficheiro atual (sem caminho)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current directory</source>
        <translation>diretório atual</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="122"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="135"/>
        <source>Save it</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="159"/>
        <source>Select Executeable</source>
        <translation>Selecionar executável</translation>
    </message>
</context>
<context>
    <name>CustomMouseShortcut</name>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="69"/>
        <source>Set Mouse Shortcut</source>
        <translation>Definir atalho do rato</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="114"/>
        <source>Don&apos;t set</source>
        <translation>Não definir</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="127"/>
        <source>Set Shortcut</source>
        <translation>Definir atalho</translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="68"/>
        <source>Delete File</source>
        <translation>Eliminar ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="98"/>
        <source>Do you really want to delete this file?</source>
        <translation>Deseja eliminar este ficheiro?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Move to Trash</source>
        <translation>Mover para o Lixo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="137"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="150"/>
        <source>Delete permanently</source>
        <translation>Eliminar permanentemente</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation>Enter = Mover para o lixo, Shift+Enter = Eliminar permanentemente, Escape = Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation>Enter = Eliminar, Escape = Cancelar</translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <location filename="../qml/mainview/Display.qml" line="481"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="507"/>
        <source>Open a file to begin</source>
        <translation>Abrir um ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="558"/>
        <source>No results found...</source>
        <translation>Sem resultados...</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="565"/>
        <source>Rotate Image?</source>
        <translation>Rodar imagem?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>The Exif data of this image says, that this image is supposed to be rotated.</source>
        <translation>Os dados Exif desta imagem indicam que a mesma é passível de rotação.</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>Do you want to apply the rotation?</source>
        <translation>Pretende aplicar a rotação?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="567"/>
        <source>Yes, do it</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="568"/>
        <source>No, don&apos;t</source>
        <translation>Não</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="68"/>
        <source>Filter images in current directory</source>
        <translation>Filtrar imagens no diretório atual</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="83"/>
        <source>Enter here the term you want to search for. Seperate multiple terms by a space.</source>
        <translation>Escreva aqui o termo de procura. Pode separar os termos utilizando um espaço.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="90"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>Para restringir o termo a extensões de ficheiro, adicione um ponto &apos;.&apos; antes do termo.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="137"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="145"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="160"/>
        <source>Remove Filter</source>
        <translation>Remover filtro</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="10"/>
        <source>Edit with</source>
        <translation>Editar com</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="13"/>
        <source>Open in</source>
        <translation>Abrir com</translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="317"/>
        <source>Unknown</source>
        <translation>Desconhecida</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="320"/>
        <source>Daylight</source>
        <translation>Luz do dia</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="323"/>
        <source>Fluorescent</source>
        <translation>Fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="326"/>
        <source>Tungsten (incandescent light)</source>
        <translation>Tungsténio (luz incandescente)</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="329"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="332"/>
        <source>Fine weather</source>
        <translation>Céu limpo</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="335"/>
        <source>Cloudy Weather</source>
        <translation>Céu nublado</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="338"/>
        <source>Shade</source>
        <translation>Escuro</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="341"/>
        <source>Daylight fluorescent</source>
        <translation>Luz natural fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="344"/>
        <source>Day white fluorescent</source>
        <translation>Dia branco fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="347"/>
        <source>Cool white fluorescent</source>
        <translation>Branco fluorescente frio</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="350"/>
        <source>White fluorescent</source>
        <translation>Branco fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="353"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="356"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="359"/>
        <source>Standard light</source>
        <translation>Luz padrão</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="371"/>
        <source>D50</source>
        <translation>D50</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="374"/>
        <source>ISO studio tungsten</source>
        <translation>Tungsténio ISO studio</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="377"/>
        <source>Other light source</source>
        <translation>Outra fonte de luz</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="380"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source</extracomment>
        <translation>Fonte de luz inválida</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="389"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired</extracomment>
        <translation>sim</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn&apos;t fired</extracomment>
        <translation>não</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="393"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash</extracomment>
        <translation>Sem função de flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="395"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luz de retorno strobe não detetada</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luz de retorno strobe detetada</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="399"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>modo compulsivo de flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="401"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>modo automático</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>redução de olhos vermelhos</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="405"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>detetada luz de retorno</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="407"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luz de retorno não detetada</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="454"/>
        <source>Invalid flash</source>
        <translation>Flash inválido</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>Standard</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="467"/>
        <source>Landscape</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>Portrait</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="473"/>
        <source>Night Scene</source>
        <translation>Cena noturna</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene</extracomment>
        <translation>Tipo de cena inválido</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="25"/>
        <source>Open File</source>
        <translation>Abrir ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="26"/>
        <source>Settings</source>
        <translation>Definições</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="27"/>
        <source>Set as Wallpaper</source>
        <translation>Definir como papel de parede</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="28"/>
        <source>Start Slideshow</source>
        <translation>Iniciar apresentação</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="29"/>
        <source>Filter Images in Folder</source>
        <translation>Filtrar imagens na pasta</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="30"/>
        <source>Show/Hide Metadata</source>
        <translation>Mostrar/ocultar metadados</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="31"/>
        <source>About PhotoQt</source>
        <translation>Sobre o PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="32"/>
        <source>Hide (System Tray)</source>
        <translation>Ocultar (bandeja do sistema)</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="33"/>
        <source>Quit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="120"/>
        <source>Quickstart</source>
        <translation>Arranque rápido</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="47"/>
        <source>Open image file</source>
        <translation>Abrir ficheiro de imagem</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="121"/>
        <location filename="../cplusplus/mainwindow.cpp" line="122"/>
        <location filename="../cplusplus/mainwindow.cpp" line="124"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="126"/>
        <source>All Files</source>
        <translation>Todos os ficheiros</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="537"/>
        <source>Image Viewer</source>
        <translation>Visualizador de imagens</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="542"/>
        <source>Hide/Show PhotoQt</source>
        <translation>Mostrar/Ocultar PhotoQt</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="44"/>
        <source>No File Loaded</source>
        <translation>Nenhum ficheiro carregado</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="62"/>
        <source>File Format Not Supported</source>
        <translation>Formato de ficheiro não suportado</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="80"/>
        <source>Invalid File</source>
        <translation>Ficheiro inválido</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="121"/>
        <source>Keep Open</source>
        <translation>Manter aberta</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="191"/>
        <source>Filesize</source>
        <translation>Tamanho do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="193"/>
        <location filename="../qml/slidein/MetaData.qml" line="196"/>
        <source>Dimensions</source>
        <translation>Dimensões</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="201"/>
        <source>Make</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="202"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="203"/>
        <source>Software</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="205"/>
        <source>Time Photo was Taken</source>
        <translation>Data da foto</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="206"/>
        <source>Exposure Time</source>
        <translation>Tempo de exposição</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="207"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="208"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="209"/>
        <source>Scene Type</source>
        <translation>Tipo de cena</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="210"/>
        <source>Focal Length</source>
        <translation>Distância focal</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="211"/>
        <source>F Number</source>
        <translation>Número F</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="212"/>
        <source>Light Source</source>
        <translation>Fonte de luz</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="214"/>
        <source>Keywords</source>
        <translation>Palavras-chave</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="215"/>
        <source>Location</source>
        <translation>Localização</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="216"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="218"/>
        <source>GPS Position</source>
        <translation>Posição GPS</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="116"/>
        <source>Hide Counter</source>
        <translation>Ocultar contador</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="183"/>
        <source>Hide Filepath, leave Filename</source>
        <translation>Ocultar caminho e manter o nome do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="191"/>
        <source>Hide both, Filename and Filepath</source>
        <translation>Ocultar nome e caminho do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="236"/>
        <source>Filter:</source>
        <extracomment>As in: FILTER images</extracomment>
        <translation>Filtro:</translation>
    </message>
</context>
<context>
    <name>QuickSettings</name>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="46"/>
        <source>Quick Settings</source>
        <translation>Definições rápidas</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="55"/>
        <source>Change settings with one click. They are saved and applied immediately. If you&apos;re unsure what a setting does, check the full settings for descriptions.</source>
        <translation>Mude as definições com um clique. As alterações são gravadas e aplicadas imediatamente. Se não souber o efeito de uma função, consulte as descrições nas definições completas.</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="77"/>
        <source>Sort by</source>
        <translation>Organizar por</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Natural Name</source>
        <translation>Nome natural</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>File Size</source>
        <translation>Tamanho do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="161"/>
        <source>Loop through folder</source>
        <translation>Ciclo na pasta</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="182"/>
        <source>Window mode</source>
        <translation>Modo de janela</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="192"/>
        <source>Show window decoration</source>
        <translation>Mostrar decoração de janela</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="214"/>
        <source>Close on click on background</source>
        <translation>Fechar ao clicar no fundo</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="235"/>
        <source>Keep thumbnails visible</source>
        <translation>Manter miniaturas visíveis</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Normal thumbnails</source>
        <translation>Miniaturas normais</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Dynamic thumbnails</source>
        <translation>Miniaturas dinâmicas</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="280"/>
        <source>Enable &apos;Quick Settings&apos;</source>
        <translation>Ativar definições rápidas</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Smart thumbnails</source>
        <translation>Miniaturas pequenas</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>No tray icon</source>
        <translation>Sem ícone na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Hide to tray icon</source>
        <translation>Ocultar para a bandeja</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation>Mostrar ícone na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="305"/>
        <source>Show full settings</source>
        <translation>Mostrar definições completas</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="68"/>
        <source>Rename File</source>
        <translation>Mudar nome</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="148"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="67"/>
        <source>Scale Image</source>
        <translation>Ajustar imagem</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="83"/>
        <source>Current Size:</source>
        <translation>Tamanho atual:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="112"/>
        <source>Error! Something went wrong, unable to save new dimension...</source>
        <translation>Erro! Algo errado aconteceu e não foi possível guardar a nova dimensão.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="136"/>
        <source>New width:</source>
        <translation>Nova largura:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="143"/>
        <source>New height:</source>
        <translation>Nova altura:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="248"/>
        <source>Quality</source>
        <translation>Qualidade</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="298"/>
        <source>Scale into new file</source>
        <translation>Ajustar em novo ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="315"/>
        <source>Don&apos;t scale</source>
        <translation>Não ajustar</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="217"/>
        <source>Aspect Ratio</source>
        <translation>Rácio de aspeto</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="284"/>
        <source>Scale in place</source>
        <translation>Ajustar no ficheiro atual</translation>
    </message>
</context>
<context>
    <name>SettingsItem</name>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="59"/>
        <source>Look and Feel</source>
        <translation>Aparência</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="72"/>
        <location filename="../qml/settings/SettingsItem.qml" line="124"/>
        <source>Basic</source>
        <translation>Básicas</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="93"/>
        <location filename="../qml/settings/SettingsItem.qml" line="143"/>
        <source>Advanced</source>
        <translation>Avançadas</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="115"/>
        <source>Thumbnails</source>
        <translation>Miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="174"/>
        <source>Metadata</source>
        <translation>Metadados</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="194"/>
        <source>Other Settings</source>
        <translation>Outras definições</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="219"/>
        <source>Filetypes</source>
        <translation>Tipos de ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="240"/>
        <source>Shortcuts</source>
        <translation>Atalhos</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="313"/>
        <source>Restore Default Settings</source>
        <translation>Restaurar predefinições</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="326"/>
        <source>Exit and Discard Changes</source>
        <translation>Sair e rejeitar alterações</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="344"/>
        <source>Save Changes and Exit</source>
        <translation>Gravar alterações e sair</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="358"/>
        <source>Clean Database</source>
        <translation>Limpar base de dados</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>Do you really want to clean up the database?</source>
        <translation>Tem a certeza de que quer limpar a base de dados?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation>Esta operação remove todas as miniaturas obsoletas e pode acelerar o PhotoQt.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This process might take a little while.</source>
        <translation>Este processo pode ser demorado.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="360"/>
        <source>Yes, clean is good</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="361"/>
        <source>No, don&apos;t have time for that</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="368"/>
        <source>Erase Database</source>
        <translation>Apagar base de dados</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>Do you really want to ERASE the entire database?</source>
        <translation>Tem a certeza de que quer apagar a base de dados?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This removes every single item in the database! This step should never really be necessarily. After that, every thumbnail has to be newly re-created.</source>
        <translation>Esta operação remove todos os elementos da base de dados e não deve ser necessária! Após esta operação, todas as miniaturas serão novamente criadas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This step cannot be reversed!</source>
        <translation>Esta operação não pode ser revertida!</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="370"/>
        <source>Yes, get rid of it all</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="371"/>
        <source>Nooo, I want to keep it</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="378"/>
        <source>Set Default Shortcuts</source>
        <translation>Repor atalhos pré-definidos</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="379"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation>Tem a certeza de que deseja repor os atalhos pré-definidos?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="380"/>
        <source>Yes, please</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="381"/>
        <source>Nah, don&apos;t</source>
        <translation>Não</translation>
    </message>
</context>
<context>
    <name>Slideshow</name>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="86"/>
        <source>Start a Slideshow</source>
        <translation>Iniciar apresentação</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="97"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that is played in the background.</source>
        <translation>Pode ajustar diversas definições para as apresentações. Pode ajustar o tempo de exibição das imagens, a duração da transição entre imagens e o ficheiro áudio a reproduzir em segundo plano.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="104"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>Assim que definir as opções, pode iniciar a apresentação através do inicio rápido, isto é, ignorando a janela de definições.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="114"/>
        <source>Time in between</source>
        <translation>Intervalo entre imagens</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="121"/>
        <source>Adjust the time between the images. The time specified here is the amount of time the image will be completely visible, i.e. the transitioning (if set) is not part of this time.</source>
        <translation>Ajuste o tempo de exibição das imagens. O valor que especificar aqui representa o tempo de exibição de cada imagem. O tempo de transição (se definido) não está incluído aqui.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="161"/>
        <source>Smooth Transition</source>
        <translation>Transição suave</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="168"/>
        <source>Here you can set, if you want the images to fade into each other, and how fast they are to do that.</source>
        <translation>Aqui pode escolher se as imagens devem ser desvanecidas e a velocidade que isso ocorre.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="182"/>
        <source>No Transition</source>
        <translation>Sem transição</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="198"/>
        <source>Long Transition</source>
        <translation>Transição longa</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="210"/>
        <source>Shuffle and Loop</source>
        <translation>Baralhar e percorrer</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="217"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note, that no image will be shown twice before every image has been shown once.</source>
        <translation>Se quiser que o PhotoQt percorra todas as imagens (assim que mostrar a última imagem, voltar à primeira) ou se quiser carregar as imagens aleatoriamente, pode ativar as caixas abaixo. Tenha em conta que nenhuma imagem será mostrada duas vezes sem mostrar uma vez todas as imagens. </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="223"/>
        <source>Loop over images</source>
        <translation>Percorrer imagens</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="229"/>
        <source>Shuffle images</source>
        <translation>Baralhar imagens</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="241"/>
        <source>Hide Quickinfo</source>
        <translation>Ocultar informação rápida</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="249"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>Dependendo da configuração, o PhotoQt pode mostrar algumas informações na margem superior. Por exemplo, o diretório ou o nome do ficheiro. Aqui pode desativar estas informações.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="254"/>
        <source>Hide Quickinfos</source>
        <translation>Ocultar informações rápidas</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="266"/>
        <source>Background Music</source>
        <translation>Música de fundo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="273"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>Algumas pessoas gostam de ouvir música durante uma apresentação. Aqui pode selecionar o ficheiro áudio que pretende reproduzir em segundo plano.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="280"/>
        <source>Enable Music</source>
        <translation>Ativar música</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="310"/>
        <source>Click here to select music file...</source>
        <translation>Clique aqui para selecionar o ficheiro áudio.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="344"/>
        <source>Okay, lets start</source>
        <translation>Vamos lá então</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="348"/>
        <source>Wait, maybe later</source>
        <translation>Talvez mais tarde</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="352"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <translation>Guardar alterações mas não iniciar ainda</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Select music file...</source>
        <translation>Selecionar ficheiro áudio...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Music Files</source>
        <translation>Ficheiros áudio</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>All Files</source>
        <translation>Todos os ficheiros</translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Play Slideshow</source>
        <translation>Reproduzir apresentação</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Pause Slideshow</source>
        <translation>Parar apresentação</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="49"/>
        <source>Music Volume:</source>
        <translation>Volume áudio:</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="77"/>
        <source>Exit Slideshow</source>
        <translation>Sair da apresentação</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully installed</source>
        <translation>O PhotoQt foi instalado com sucesso</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully updated</source>
        <translation>O PhotoQt foi atualizado com sucesso</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>Welcome to PhotoQt. PhotoQt is an image viewer, aimed at being fast and reliable, highly customisable and good looking.</source>
        <translation>Bem-vindo ao PhotoQt. O PhotoQt é um visualizador de imagens criado com o intuito de fiável, elegante e ao mesmo tempo fácil de utilizar.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>This app started out more than three and a half years ago, and it has developed quite a bit since then. It has become very efficient, reliable, and highly flexible (check out the settings). I&apos;m convinced it can hold up to the more &apos;traditional&apos; image viewers out there in every way.</source>
        <translation>O PhotoQt está a ser desenvolvido há cerca de três anos e meio e já melhorou muito desde a primeira versão. Tornou-se mais eficiente, estável e altamente personalizado (aceda às definições).  Estou convencido que já está a par com as melhores aplicações existentes.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Welcome back to PhotoQt. It hasn&apos;t been that long since the last release of PhotoQt. Yet, it changed pretty much entirely, as it now is based on QtQuick rather than QWidgets. A large quantity of the code had to be re-written, while some chunks could be re-used. Thus, it is now more reliable than ever before and overall simply feels well rounded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>Many File Formats</source>
        <translation>Diversos formatos de ficheiros</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>PhotoQt can make use of GraphicsMagick, an image library, to display many different image formats. Currently, there are up to 72 different file formats supported (exact number depends on your system)! You can find a list of it in the settings (Tab &apos;Other&apos;). There you can en-/disable different ones and also add custom file endings.</source>
        <translation>O PhotoQt pode utilizar a biblioteca GraphicsMagick para mostrar diferentes formatos de imagem. Atualmente, são suportados 72 formatos de imagem (o número exato depende do seu sistema operativo). Pode encontrar a lista de formatos nas definições (separador Outras) e ativar ou desativar os formatos pretendidos, bem como adicionar formatos personalizados.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>Make PhotoQt your own</source>
        <translation>Personalizar o PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>PhotoQt has an extensive settings area. By default you can call it with the shortcut &apos;e&apos; or through the dropdown menu at the top edge towards the top right corner. You can adjust almost everything in PhotoQt, and it&apos;s certainly worth having a look there. Each setting usually comes with a little explanation text. Some of the most often used settings can also be conveniently adjusted in a slide-in widget, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Most images store some additional information within the file&apos;s metadata. PhotoQt can read and display a selection of this data. You can find this information in the slide-in window hidden behind the left edge of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>PhotoQt also brings a slideshow feature. When you start a slideshow, it starts at the currently displayed image. There are a couple of settings that can be set, like transition, speed, loop, and shuffle. Plus, you can set a music file that is played in the background. When the slideshow takes longer than the music file, then PhotoQt starts the music file all over from the beginning. At anytime during the slideshow, you can move the mouse cursor to the top edge of the screen to get a little bar, where you can pause/exit the slideshow and adjust the music volume.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>Thumbnails</source>
        <translation>Miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Here below you find a short overview of a selection of a few things PhotoQt has to offer, but feel free to skip it and just get started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>What would be an image viewer without thumbnails support? It would only be half as good. Whenever you load an image, PhotoQt loads the other images in the directory in the background (by default, it tries to be smart about it and only loads the ones that are needed). It lines them up in a row at the bottom edge (move your mouse there to see them). There are many settings just for the thumbnails, like, e.g., size, liftup, en-/disabled, type, filename, permanently shown/hidden, etc. PhotoQt&apos;s quite flexible with that.</source>
        <translation>O que seria de um visualizador de imagens sem suporte a miniaturas? Nada de especial. Sempre que carregar uma imagem, o PhotoQt carrega as outras imagens do diretório em segundo plano (por definição, tenta ser inteligente a apenas carrega as necessárias). De seguida, alinha-as na margem inferior do ecrã (mova o rato até à margem para as ver). Existem diversas opções para as miniaturas. Como exemplo temos o tamanho, a ampliação, o tipo, nome de ficheiro, mostrar/ocultar permanentemente e muito mais.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>Shortcuts</source>
        <translation>Atalhos</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>One of the many strengths of PhotoQt is the ability to easily set a shortcut for almost anything. Even mouse shortcuts are possible! You can choose from a huge number of internal functions, or you can run any custom script or command.</source>
        <translation>Uma das características do PhotoQt é a capacidade de definir um atalho para quase todas as opções. Também pode utilizar o rato como atalho. Pode escolher diversas opções e, eventualmente, um comando ou script personalizado.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Image Information (Exif/IPTC)</source>
        <translation>Informações de imagem (Exif/IPTC)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>Slideshow</source>
        <translation>Apresentação</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>Localisation</source>
        <translation>Tradução</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>PhotoQt comes with a number of translations. Many have taken some of their time to create/update one of them (Thank you!). Not all of them are complete... do you want to help?</source>
        <translation>O PhotoQt está disponível em diversos idiomas. Os colaboradores doaram parte do seu tempo para nos ajudar (obrigado). Mas nem todos estão completos... Porque não nos ajuda?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="355"/>
        <source>There are many many more features. Best is, you just give it a go. Don&apos;t forget to check out the settings to make PhotoQt YOUR image viewer. Enjoy :-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="387"/>
        <source>Okay, I got enough now. Lets start!</source>
        <translation>Acho que por agora já chega!</translation>
    </message>
</context>
<context>
    <name>TabDetails</name>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="55"/>
        <source>Image Metadata</source>
        <translation>Metadados da imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="74"/>
        <source>PhotoQt can display different information of and about each image. The widget for this information is on the left outside the screen and slides in when mouse gets close to it and/or when the set shortcut (default Ctrl+E) is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <translation>O PhotoQt pode mostrar diversas informações sobre as imagens. O widget para estas informações situa-se na margem esquerda do ecrã e será exibido ao aproximar o ponteiro do rato ou através do atalho (Ctrl+E). Pode desativar a ativação do widget com o ponteiro do rato selecionando a caixa abaixo.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Trigger Widget on Mouse Hovering</source>
        <translation>Ativar widget ao passar com o rato</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut. This can come in handy, if you get annoyed by accidentally opening the widget occasionally.</source>
        <translation>As informações do widget podem ser mostradas de duas formas: mover o cursor do rato para a margem esquerda do ecrã para o mostrar temporariamente (enquanto o rato lá estiver), ou clicando na caixa (opção apenas memorizada por sessão). Alternativamente, também pode ser mostrado através de uma tecla de atalho. A opção de ativar o widget com o rato pode ser desativada para que o widget apenas seja mostrado através da tecla de atalho. Pode ser bastante útil caso se sinta incomodado pelo aparecimento acidental.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="118"/>
        <source>Turn mouse triggering OFF</source>
        <translation>Desativar ativação com o rato</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>Which items are shown?</source>
        <translation>Quais são os itens mostrados?</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>PhotoQt can display a number of information about the image (often called &apos;Exif data&apos;&apos;). However, you might not be interested in all of them, hence you can choose to disable some of them here.</source>
        <translation>O PhotoQt pode mostrar diversas informações sobre a sua imagem (dados Exif). No entanto, se não estiver interessado nestes dados, pode desativar aqui os itens que não lhe interessam.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="149"/>
        <source>Enable ALL</source>
        <translation>Ativar tudo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="155"/>
        <source>Disable ALL</source>
        <translation>Desativar tudo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="179"/>
        <source>Filesize</source>
        <translation>Tamanho do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="180"/>
        <source>Dimensions</source>
        <translation>Dimensões</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="181"/>
        <source>Make</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="182"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="183"/>
        <source>Software</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="184"/>
        <source>Time Photo was Taken</source>
        <translation>Data da foto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="185"/>
        <source>Exposure Time</source>
        <translation>Tempo de exposição</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="186"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="187"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="188"/>
        <source>Scene Type</source>
        <translation>Tipo de cena</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="189"/>
        <source>Focal Length</source>
        <translation>Distância focal</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="190"/>
        <source>F-Number</source>
        <translation>Número F</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="191"/>
        <source>Light Source</source>
        <translation>Fonte de luz</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="192"/>
        <source>Keywords</source>
        <translation>Palavras-chave</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="193"/>
        <source>Location</source>
        <translation>Localização</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="194"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="195"/>
        <source>GPS Position</source>
        <translation>Posição GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Adjusting Font Size</source>
        <translation>Ajustar tipo de letra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Computers can have very different resolutions. On some of them, it might be nice to increase the font size of the labels to have them easier readable. Often, a size of 8 or 9 should be working quite well...</source>
        <translation>Os computadores podem ter várias resoluções. Em alguns, recomenda-se aumentar o tamanho do tipo de letra para que seja mais fácil de ler. Normalmente, o tamanho 8 ou 9 deve ser o suficiente...</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Rotating/Flipping Image according to Exif Data</source>
        <translation>Rodar/inverter imagem conforme os dados Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly. When asking PhotoQt to always rotate images automatically without asking, it already does so at image load (including thumbnails).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="294"/>
        <source>Never rotate/flip images</source>
        <translation>Não rodar/inverter imagens</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="300"/>
        <source>Always rotate/flip images</source>
        <translation>Rodar/inverter imagens</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="305"/>
        <source>Always ask</source>
        <translation>Perguntar sempre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>Online map for GPS</source>
        <translation>Mapa para GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>If you&apos;re image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>Se a sua imagem incluir uma localização GPS e clicar no texto da localização, o seu navegador web abrirá esta localização num mapa web. Aqui pode escolher o serviço a utilizar para o efeito. Tenha em atenção que aceitamos sugestões para serviços de mapas.</translation>
    </message>
</context>
<context>
    <name>TabFiletypes</name>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="55"/>
        <source>Filetypes</source>
        <translation>Tipos de ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>File Types - Qt</source>
        <translation>Tipos de ficheiro - Qt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.&lt;br&gt;If a file ending for one of the formats is missing, you can add it below, formatted like &apos;*.ending&apos; (without single quotation marks), multiple entries seperated by commas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="102"/>
        <source>Extra File Types:</source>
        <translation>Outros tipos de ficheiro:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="117"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="166"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="214"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="337"/>
        <source>Mark None</source>
        <translation>Desmarcar tudo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="123"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="172"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="220"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="343"/>
        <source>Mark All</source>
        <translation>Marcar tudo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>File Types - GraphicsMagick</source>
        <translation>Tipos de ficheiro - GraphicsMagick</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. If you prefer not to have one or the other enabled in PhotoQt, you can simply disable individual formats below.&lt;br&gt;There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>File Types - GraphicsMagick (requires Ghostscript)</source>
        <translation>Tipos de ficheiro - GraphicsMagick (requer Ghostscript)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and work. However, they require Ghostscript to be installed on the system.</source>
        <translation>Os tipos de ficheiros seguintes são suportados pelo GraphicsMagick, foram testados e funcionam. Contudo, necessita da aplicação Ghostscript instalada no seu sistema.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>File Types - Other tools required</source>
        <translation>Tipos de ficheiro - Outras ferramentas necessárias</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation>Os tipos de ficheiro seguintes são suportados através de aplicações de terceiros. Pode ser necessário instalar essas aplicações para os poder utilizar.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>If an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation>Se algum formato também for disponibilizado pelo GraphicksMagick/Qt, o PhotoQt escolhe, primeiro, a ferramenta externa (se ativa).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <source>Gimp&apos;s XCF file format.</source>
        <extracomment>&apos;Makes use of&apos; is in connection with an external tool (i.e., it &apos;makes use of&apos; tool abc)</extracomment>
        <translation>Ficheiro XFC do Gimp.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Makes use of</source>
        <translation>Utiliza</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Adobe Photoshop PSD and PSB.</source>
        <translation>Adobe Photoshop PSD e PSB.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>File Types - GraphicsMagick (Untested)</source>
        <translation>Tipos de ficheiro - GraphicsMagick (não testados)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>The following file types are generally supported by GraphicsMagick, but I wasn&apos;t able to test them in PhotoQt (due to lack of test images). They might very well be working, but I simply can&apos;t say. If you decide to enable some of the, the worst that could happen ist, that you see an error image instead of the actual image.</source>
        <translation>Os tipos de ficheiro seguintes são, normalmente, suportados pelo GraphicsMagick, mas eu não os consegui testar no PhotoQt (não tenho ficheiros para os testar). Possivelmente poderão ser visualizados mas não o posso garantir. Se decidir ativar algum dos formatos, o pior que pode acontecer é ver uma imagem de erro em vez da imagem.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>If you happen to have an image in one of those formats and don&apos;t mind sending it to me, that&apos;d be really cool...</source>
        <translation>Se tiver algum ficheiro com estes formatos e não se importar de os enviar, seria muito bom...</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelAdvanced</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="54"/>
        <source>Advanced Settings</source>
        <translation>Definições avançadas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>Background of PhotoQt</source>
        <translation>Fundo do PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above.&lt;br&gt;Note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="91"/>
        <source>Use (half-)transparent background</source>
        <translation>Utilizar fundo semi-transparente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="97"/>
        <source>Use faked transparency</source>
        <translation>Utilizar transparência fictícia</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="102"/>
        <source>Use custom background image</source>
        <translation>Utilizar imagem de fundo personalizada</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="107"/>
        <source>Use one-coloured, non-transparent background</source>
        <translation>Utilizar uma cor com fundo opaco</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="164"/>
        <source>No image selected</source>
        <translation>Nenhuma imagem selecionada</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="188"/>
        <source>Scale to fit</source>
        <translation>Ajustar para caber</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="194"/>
        <source>Scale and Crop to fit</source>
        <translation>Ajustar e recortar para caber</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="199"/>
        <source>Stretch to fit</source>
        <translation>Esticar para caber</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="204"/>
        <source>Center image</source>
        <translation>Centrar imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="209"/>
        <source>Tile image</source>
        <translation>Imagem em mosaico</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Background/Overlay Color</source>
        <translation>Cor de fundo/sobreposição</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>Aqui pode ajustar a cor de fundo do PhotoQt (área não coberta pela imagem). Se utilizar um compositor ou uma imagem de fundo, pode especificar o seu valor alfa, isto é, a transparência da camada de sobreposição. Se não utilizar um compositor nem uma imagem de fundo, esta cor será o fundo não transparente do PhotoQt.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="259"/>
        <source>Red:</source>
        <translation>Vermelho:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="281"/>
        <source>Green:</source>
        <translation>Verde:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="303"/>
        <source>Blue:</source>
        <translation>Azul:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="325"/>
        <source>Alpha:</source>
        <translation>Alfa:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="387"/>
        <source>Preview colour</source>
        <translation>Pré-visualizar</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Border Around Image</source>
        <translation>Contorno em redor da imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels (looks better). Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>Sempre que carregar uma imagem, esta não é mostrada completamente no modo de ecrã completo. Será mostrada uma pequena margem em redor da imagem com um conjunto de pixeis (melhor aspeto). Aqui pode ajustar a largura dessa margem (o valor 0 desativa a margem).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Close on Click in empty area</source>
        <translation>Fechar ao clicar numa área vazia</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. It can be a nice feature, PhotoQt will feel even more like a &quot;floating layer&quot;. However, you might at times close PhotoQt accidentally.</source>
        <translation>Esta opção faz com que o PhotoQt se comporte como os visualizadores de imagem JavaScript, encontrados em muitos sítios web. Se clicar numa área exterior à imagem, a aplicação será fechada. Pode ser uma funcionalidade agradável para algumas pessoas, mas tenha em conta que a aplicação pode ser fechada sem querer.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Note: If you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>Nota: se já utilizar o clique do rato como atalho, esta opção não terá qualquer efeito!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="475"/>
        <source>Close on click in empty area</source>
        <translation>Fechar ao clicar numa área vazia</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>Looping Through Folder</source>
        <translation>Percorrer a pasta</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>When you load the last image in a directory and select &apos;Next&apos;, PhotoQt automatically jumps to the first image (and vice versa: if you select &apos;Previous&apos; while having the first image loaded, PhotoQt jumps to the last image). Disabling this option makes PhotoQt stop at the first/last image (i.e. selecting &apos;Next&apos;/&apos;Previous&apos; will have no effect in these two special cases).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="494"/>
        <source>Loop through folder</source>
        <translation>Ciclo na pasta</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Smooth Transition</source>
        <translation>Transição suave</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced by the new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="526"/>
        <source>No Transition</source>
        <translation>Sem transição</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="546"/>
        <source>Long Transition</source>
        <translation>Transição longa</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Menu Sensitivity</source>
        <translation>Sensibilidade do menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Here you can adjust the sensitivity of the drop-down menu. The menu opens when your mouse cursor gets close to the right side of the upper edge. Here you can adjust how close you need to get for it to open.</source>
        <translation>Aqui pode ajustar a sensibilidade do menu emergente. O menu aparece ao colocar o rato no lado direito da margem superior. Pode ajustar a distância dessa margem para que o menu apareça.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="583"/>
        <source>Low Sensitivity</source>
        <translation>Baixa</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="603"/>
        <source>High Sensitivity</source>
        <translation>Alta</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation>Sensibilidade da roda do rato</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Here you can adjust the sensitivity of the mouse wheel. For example, if you have set the mouse wheel up/down for switching back and forth between images, then a lower sensitivity means that you will have to scroll further for triggering a shortcut. Per default it is set to the highest sensitivity, i.e. every single wheel movement is evaluated.</source>
        <translation>Aqui pode ajustar a sensibilidade da roda do rato. Por exemplo, se tiver definido a roda do rato para avançar/recuar imagens, uma sensibilidade menor significa que terá de rodar a roda do rato um pouco mais para efetuar a ação. Por definição, esta opção utiliza a sensibilidade mais alta, isto é, cada movimento do rato é avaliado.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="640"/>
        <source>Very sensitive</source>
        <translation>Muito sensível</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="660"/>
        <source>Not at all sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>Remember per session</source>
        <translation>Memorizar por sessão</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>If you would like PhotoQt to remember the rotation/flipping and/or zoom level per session (not permanent), then you can enable it here. If not set, then every time a new image is displayed, it is displayed neither zoomed nor rotated nor flipped (one could say, it is displayed &apos;normal&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="697"/>
        <source>Remember Rotation/Flip</source>
        <translation>Memorizar rotação e inversão</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="702"/>
        <source>Remember Zoom Level</source>
        <translation>Memorizar ampliação</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation and Window Geometry</source>
        <translation>Geometria e animação de janelas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="748"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation of fade-in widgets (like, e.g., Settings or About Widget)</source>
        <translation>Animação dos widgets (por exemplo: o widget Definições ou o widget Sobre)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation>Gravar e restaurar geometria da janela: ao sair do PhotoQt, a aplicação guarda o tamanho e a posição da janela e, posteriormente, ao iniciar restaura estas definições.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="738"/>
        <source>Animate all fade-in elements</source>
        <translation>Animar todos os elementos</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="743"/>
        <source>Save and restore window geometry</source>
        <translation>Gravar e restaurar geometria da janela</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelBasic</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Definições básicas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Sort Images</source>
        <translation>Organizar imagens</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Here you can adjust, how the images in a folder are supposed to be sorted. You can sort them by Filename, Natural Name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), File Size, and Date. Also, you can reverse the sorting order from ascending to descending if wanted.</source>
        <translation>Aqui pode ajustar o método de organização das imagens na pasta. Pode organizar as imagens por nome, nome natural (o ficheiro imagem10.jpg surge após o ficheiro imagem9.jpg e não após imagem1.jpg), tamanho e data. Pode também inverter a organização de ascendente para descendente.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Hint: You can also change this setting very quickly from the &apos;Quick Settings&apos; window, hidden behind the right screen edge.</source>
        <translation>Dica: pode alterar rapidamente esta definição através das &quot;Definições rápidas&quot;, oculta na margem direita do ecrã.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="90"/>
        <source>Sort by:</source>
        <translation>Organizar por:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Natural Name</source>
        <translation>Nome natural</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Filesize</source>
        <translation>Tamanho do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="105"/>
        <source>Ascending</source>
        <translation>Ascendente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="113"/>
        <source>Descending</source>
        <translation>Descendente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>Window Mode</source>
        <translation>Modo de janela</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>PhotoQt is designed with the space of a fullscreen app in mind. That&apos;s why it by default runs as fullscreen. However, some might prefer to have it as a normal window, e.g. so that they can see the panel.</source>
        <translation>O PhotoQt foi criado com o intuito de ser utilizado no modo de ecrã completo. Contudo, algumas pessoas podem preferir que a aplicação seja aberta no modo de janela, para que seja visto o painel.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="151"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>Executar PhotoQt no modo de janela</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="159"/>
        <source>Show Window Decoration</source>
        <translation>Mostrar decoração da janela</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>Hide to Tray Icon</source>
        <translation>Ocultar na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>When started PhotoQt creates a tray icon in the system tray. If desired, you can set PhotoQt to minimise to the tray instead of quitting. This causes PhotoQt to be almost instantaneously available when an image is opened.&lt;br&gt;It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>No tray icon</source>
        <translation>Sem ícone na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Hide to tray icon</source>
        <translation>Ocultar para a bandeja</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation>Mostrar ícone na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>Closing &apos;X&apos; (top right)</source>
        <translation>&apos;X&apos; de fecho (direita superior)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>There are two looks for the closing &apos;x&apos; at the top right: a normal &apos;x&apos;, or a slightly more fancy &apos;x&apos;. Here you can switch back and forth between both of them, and also change their size. If you prefer not to have a closing &apos;x&apos; at all, see below for an option to hide it.</source>
        <translation>Existem duas formas para o &apos;X&apos; mostrado na margem superior direita. Pode utilizar um &apos;X&apos; normal ou um &apos;X&apos; melhorado. Aqui, pode alternar o &apos;X&apos; mostrado e também o seu tamanho.  Se quiser que este ícone não seja mostrado, veja abaixo a opção para o ocultar.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="216"/>
        <source>Normal Look</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="223"/>
        <source>Fancy Look</source>
        <translation>Melhorado</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="249"/>
        <source>Small Size</source>
        <translation>Menor tamanho</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="264"/>
        <source>Large Size</source>
        <translation>Maior tamanho</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>Fit Image in Window</source>
        <translation>Ajustar imagem à janela</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can zoom those images to make them fir into the window. However, keep in mind, that such images will look pixelated to a certain degree (depending on each image).</source>
        <translation>Se as dimensões da imagem forem inferiores às da janela, o PhotoQt pode ajustar as imagens à janela. Contudo, tenha em atenção que essas imagens poderão estar mais ou menos distorcidas (estando dependente de cada uma das imagens).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="287"/>
        <source>Fit Images in Window</source>
        <translation>Ajustar imagens à janela</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Hide Quickinfo (Text Labels)</source>
        <translation>Ocultar informação rápida (textos)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Here you can hide the text labels shown in the main area: The Counter in the top left corner, the file path/name following the counter, and the &quot;X&quot; displayed in the top right corner. The labels can also be hidden by simply right-clicking on them and selecting &quot;Hide&quot;.</source>
        <translation>Aqui pode ocultar o texto mostrado na área principal. O contador e nome/caminho do ficheiro no canto superior esquerdo e o &quot;X&quot; mostrado no canto superior direito. Estes textos também podem ser ocultados se clicar com o botão direito do rato no item e escolher &quot;Ocultar&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="321"/>
        <source>Hide Counter</source>
        <translation>Ocultar contador</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="326"/>
        <source>Hide Filepath (Shows only file name)</source>
        <translation>Ocultar caminho (mostrar nome do ficheiro)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="331"/>
        <source>Hide Filename (Including file path)</source>
        <translation>Ocultar nome do ficheiro (incluir caminho)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="336"/>
        <source>Hide &quot;X&quot; (Closing)</source>
        <translation>Ocultar o botão &quot;X&quot;</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="55"/>
        <source>Other Settings</source>
        <translation>Outras definições</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>Choose Language</source>
        <translation>Escolher idioma</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>O PhotoQt está traduzido em diversos idiomas. Muito obrigado a todos os tradutores do PhotoQt!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>Quick Settings</source>
        <translation>Definições rápidas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>The &apos;Quick Settings&apos; is a widget hidden on the right side of the screen. When you move the cursor there, it shows up, and you can adjust a few simple settings on the spot without having to go through this settings dialog. Of course, only a small subset of settings is available (the ones needed most often). Here you can disable the dialog so that it doesn&apos;t show on mouse movement anymore.</source>
        <translation>As definições rápidas são um widget que está oculto na margem direita do ecrã. Ao mover o cursor para essa margem, o widget é mostrado e o utilizador pode ajustar algumas definições sem ter que aceder ao diálogo de definições. No entanto, apenas estão disponíveis algumas definições (aquelas que são mais utilizadas). Aqui pode desativar o aparecimento do widget.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="129"/>
        <source>Show &apos;Quick Settings&apos; on mouse hovering</source>
        <translation>Mostrar definições rápidas as passar com o rato</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Adjust Context Menu</source>
        <translation>Ajustar menu de contexto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Here you can adjust the context menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>Aqui pode configurar o menu de contexto. Pode arrastar e largar, editar, adicionar ou remover uma entrada.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="183"/>
        <source>Executable</source>
        <translation>Executável</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="199"/>
        <source>Menu Text</source>
        <translation>Texto do menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="223"/>
        <source>Add new context menu entry</source>
        <translation>Adicionar entrada no menu de contexto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="231"/>
        <source>(Re-)set automatically</source>
        <translation>Redefinir automaticamente</translation>
    </message>
</context>
<context>
    <name>TabOtherContext</name>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="116"/>
        <source>Click here to drag</source>
        <translation>Clique aqui para arrastar</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="186"/>
        <source>quit</source>
        <translation>sair</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="54"/>
        <source>Shortcuts</source>
        <translation>Atalhos</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="73"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available function you can either double click on the tile or click the &quot;+&quot; button. This automatically opens another widget where you can set a key combination.</source>
        <translation>Aqui pode adicionar, remover e ajustar os atalhos ou mudar uma combinação de teclas. Os atalhos estão agrupados em 4 categorias para comandos internos e mais uma para comandos externos. As caixas do lado direito possuem todos os comandos possíveis. Para adicionar um atalho a uma das funções existentes, pode clicar duas vezes na função ou no botão +. Será aberto um novo widget, no qual poderá definir a combinação de teclas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="89"/>
        <source>Set default shortcuts</source>
        <translation>Aplicar atalhos pré-definidos</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="96"/>
        <source>Navigation</source>
        <translation>Navegação</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Open New File</source>
        <translation>Abrir novo ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Filter Images in Folder</source>
        <translation>Filtrar imagens na pasta</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Next Image</source>
        <translation>Imagem seguinte</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Previous Image</source>
        <translation>Imagem anterior</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to first Image</source>
        <translation>Ir para a primeira imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to last Image</source>
        <translation>Ir para a última imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Hide to System Tray</source>
        <translation>Ocultar na bandeja</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Quit PhotoQt</source>
        <translation>Sair do PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="104"/>
        <source>Image</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom In</source>
        <translation>Ampliar</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom Out</source>
        <translation>Reduzir</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom to Actual Size</source>
        <translation>Ampliar ao tamanho real</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Zoom</source>
        <translation>Reiniciar ampliação</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Right</source>
        <translation>Rodar à direita</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Left</source>
        <translation>Rodar à esquerda</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Rotation</source>
        <translation>Reiniciar rotação</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Horizontally</source>
        <translation>Inversão horizontal</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Vertically</source>
        <translation>Inversão vertical</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Scale Image</source>
        <translation>Ajustar imagem</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="112"/>
        <source>File</source>
        <translation>Ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Rename File</source>
        <translation>Mudar nome</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Delete File</source>
        <translation>Eliminar ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Copy File to a New Location</source>
        <translation>Copiar ficheiro para outra localização</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Move File to a New Location</source>
        <translation>Mover ficheiro para outra localização</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="120"/>
        <source>Other</source>
        <translation>Outro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Interrupt Thumbnail Creation</source>
        <translation>Parar criação de miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Reload Thumbnails</source>
        <translation>Recarregar miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Hide/Show Exif Info</source>
        <translation>Mostrar/ocultar informações Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Context Menu</source>
        <translation>Mostrar menu de contexto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Settings</source>
        <translation>Mostrar definições</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow</source>
        <translation>Iniciar apresentação</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Iniciar apresentação (arranque rápido)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>About PhotoQt</source>
        <translation>Sobre o PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Set as Wallpaper</source>
        <translation>Definir como papel de parede</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="128"/>
        <source>Extern</source>
        <translation>Externo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="132"/>
        <source>EXTERN</source>
        <extracomment>Is the shortcut tile text for EXTERNal shortcuts</extracomment>
        <translation>Externos</translation>
    </message>
</context>
<context>
    <name>TabShortcutsCategories</name>
    <message>
        <location filename="../qml/settings/TabShortcutsCategories.qml" line="32"/>
        <source>Category:</source>
        <translation>Categoria:</translation>
    </message>
</context>
<context>
    <name>TabShortcutsTilesAvail</name>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="98"/>
        <source>key</source>
        <extracomment>tile text for KEY shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>tecla</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="154"/>
        <source>mouse</source>
        <extracomment>tile text for MOUSE shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>rato</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsAdvanced</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="52"/>
        <source>Advanced Settings</source>
        <translation>Definições avançadas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Change Thumbnail Position</source>
        <translation>Mudar posição das miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Per default the bar with the thumbnails is shown at the lower edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge, so that&apos;s what can be changed here.</source>
        <translation>Por definição, a barra de miniaturas é mostrada na margem inferior. Contudo, se preferir que a barra de miniaturas apareça na margem superior, pode alterar aqui essa opção.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="86"/>
        <source>Show at lower edge</source>
        <translation>Mostrar na margem inferior</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="93"/>
        <source>Show at upper edge</source>
        <translation>Mostrar na margem superior</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>Filename? Dimension? Or both?</source>
        <translation>Nome de ficheiro? Dimensão? Ou ambos?</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="132"/>
        <source>Write Filename</source>
        <translation>Utilizar nome de ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>Use file-name-only Thumbnails</source>
        <translation>Utilizar miniaturas só com nome do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a file-name-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>Se não quiser que o PhotoQt carregue a miniatura da imagem atual em segundo plano, mas ainda assim quiser algo para uma melhor navegação, aqui pode criar uma miniatura só com o nome do ficheiro. O PhotoQt não carregará quaisquer miniaturas mas coloca o nome do ficheiro na caixa. Também pode ajustar o tipo de letra do texto.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>When thumbnails are displayed at the top/bottom, PhotoQt usually writes the filename on them (if not disabled). You can also use the slider below to adjust the font size.</source>
        <translation>Quando as miniaturas estão no topo/base do ecrã, o PhotoQt mostra o nome do ficheiro (se a opção não estiver desativada). Pode utilizar o controlo abaixo para ajustar o tamanho do tipo de letra.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="213"/>
        <source>Use filename-only thumbnail</source>
        <translation>Miniatura só com nome do ficheiro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>Disable Thumbnails</source>
        <translation>Desativar miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This option can also be toggled remotely via command line (run &apos;photoqt --help&apos; for more information on that). This might increase the speed of PhotoQt a good bit, however, navigating through a folder might be a little harder without thumbnails.</source>
        <translation>Se não quiser ou não precisar de miniaturas, pode desativar aqui essa opção. A opção também pode ser alternada através da linha de comandos (execute &apos;photoqt --help&apos; para obter mais informações). Esta opção pode acelerar um pouco a resposta do PhotoQt mas a navegação no diretório de imagens pode ser um pouco mais complicada sem miniaturas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="287"/>
        <source>Disable Thumbnails altogether</source>
        <translation>Desativar miniaturas globalmente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnail Cache</source>
        <translation>Cache de miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnails can be cached in two different ways:&lt;br&gt;1) File Caching (following the freedesktop.org standard) or&lt;br&gt;2) Database Caching (better performance and management, default option).</source>
        <translation>As miniaturas pode ser colocadas em cache de duas forma:&lt;br&gt;1) Cache de ficheiros (seguindo a norma freedesktop.org) ou&lt;br&gt;2) Cache de base de dados (melhor desempenho e gestão, sendo esta a opção predefinida).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <translation>Ambos os métodos possuem vantagens e desvantagens:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file. However, it&apos;s not possible to check for obsolete thumbnails (thus this may lead to many unneeded thumbnail files).</source>
        <translation>A cache de ficheiros é efetuada seguindo a norma freedesktop.org e, deste modo, a mesma miniatura pode ser partilhada pelas diversas aplicações. Contudo, não é possível procurar miniaturas obsoletas (o que pode originar a exibição de miniaturas desnecessárias).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it brings a slightly better performance, and it allows a better handling of existing thumbnails (e.g. deleting obsolete thumbnails).</source>
        <translation>A cache de base de dados não possui a vantagem de partilha de miniaturas com outras aplicações (e todas as miniaturas terão que ser criadas pelo PhotoQt), mas melhora o desempenho e permite uma melhor gestão das miniaturas existentes (por exmplo, eliminação de miniaturas obsoletas).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>PhotoQt works with either option, though the second way is set as default.</source>
        <translation>O PhotoQt pode utilizar ambas as opções mas a segunda é a pré-definida.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, that does affect the performance and usability of PhotoQt, since thumbnails have to be newly re-created every time they are needed.</source>
        <translation>Embora os utilizadores devam utilizar uma das opções referidas, pode desativar a utilização de cache. Contudo, a desativação afeta o desempenho e utilidade do PhotoQt, uma vez que as miniaturas terão que ser recriadas sempre que sejam necesárias.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="327"/>
        <source>Enable Thumbnail Cache</source>
        <translation>Ativar cache de miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="348"/>
        <source>File Caching</source>
        <translation>Cache de ficheiros</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="354"/>
        <source>Database Caching</source>
        <translation>Cache de bases de dados</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="379"/>
        <source>Current database filesize:</source>
        <translation>Tamanho da base de dados:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="405"/>
        <source>Entries in database:</source>
        <translation>Entradas na base de dados:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="426"/>
        <source>CLEAN UP database</source>
        <translation>Limpar base de dados</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="438"/>
        <source>ERASE database</source>
        <translation>Apagar base de dados</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsBasic</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Definições básicas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Thumbnail Size</source>
        <translation>Tamanho da miniatura</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but with different screen resolutions it might be nice to have them larger/smaller.</source>
        <translation>Aqui pode ajustar o tamanho das miniaturas. Pode definir um valor entre 20 e 256 pixeis. O valor pré-definido é 80 pixeis, mas dependendo da resolução do seu ecrã, pode optar por escolher outro tamanho.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>Spacing Between Thumbnail Images</source>
        <translation>Espaço entre as miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them, however exactly that can be changed here.</source>
        <translation>As miniaturas são mostradas numa linha na margem superior ou inferior (depende da configuração) e alinhadas lado a lado. Por definição, não existe qualquer espaçamento entre as miniaturas mas pode alterar aqui a opção.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>Lift-up of Thumbnail Images on Hovering</source>
        <translation>Ampliar miniaturas ao passar com o rato</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels (default 10). Here you can increase/decrease this value according to your personal preference.</source>
        <translation>Ao passar o ponteiro do rato sobre uma miniatura, esta é ampliada (pré-definição: 10 pixeis). Aqui pode aumentar/diminuir este valor.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Keep Thumbnails Visible</source>
        <translation>Manter miniaturas visíveis</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Per default the Thumbnails slide out over the edge of the screen. Here you can force them to stay visible. The big image is shrunk to fit into the empty space. Note, that the thumbnails will be hidden (and only shown on mouse hovering) once you zoomed the image in/out. Resetting the zoom restores the original visibility of the thumbnails.</source>
        <translation>Por definição, as miniaturas deslizam para fora do ecrã. Aqui pode forçar a sua visibilidade. A imagem maior é ajustada para caber num espaço visível. Tenha em atenção que as miniaturas serão ocultadas (e mostradas ao passar com o rato) assim que ampliar ou reduzir a imagem. A reposição da ampliação restaura a visibilidade original das miniaturas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="259"/>
        <source>Keep Thumnails Visible</source>
        <translation>Manter miniaturas visíveis</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic Thumbnail Creation</source>
        <translation>Criação dinâmica de miniaturas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic thumbnail creation means, that PhotoQt only sets up those thumbnail images that are actually needed, i.e. it stops once it reaches the end of the visible area and sits idle until you scroll left/right.</source>
        <translation>A criação dinâmica de miniaturas significa que o PhotoQt apenas configura as miniaturas se forem necessárias, isto é, para assim que atingir o final da área visível e fica inativa até que desloque para a esquerda ou direita.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Smart thumbnails are similar in nature. However, they make use of the fast, that once a thumbnail has been created, it can be loaded very quickly and efficiently. It also first loads all of the currently visible thumbnails, but it doesn&apos;t stop there: Any thumbnails (even if invisible at the moment) that once have been created are loaded. This is a nice compromise between efficiency and usability.</source>
        <translation>As miniaturas inteligentes são similares às dinâmicas. Contudo, a sua exibição é mais rápida porque assim que forem criadas, podem ser carregadas rapidamente e eficazmente. Carregam todas as miniaturas visíveis, mas não se fica por aqui. Neste caso, todas as miniaturas (mesmo que estejam invisíveis) que tiverem sido criadas serão carregadas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Enabling either the smart or dynamic option is recommended, as it increases the performance of PhotoQt significantly, while preserving the usability.</source>
        <translation>Deve ativar uma das duas opções, uma vez que melhora o desempenho do PhotoQt e ao mesmo tempo melhora a usabilidade.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="295"/>
        <source>Normal Thumbnails</source>
        <translation>Miniaturas normais</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="301"/>
        <source>Dynamic Thumbnails</source>
        <translation>Miniaturas dinâmicas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="307"/>
        <source>Smart Thumbnail</source>
        <translation>Miniaturas inteligentes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>Always center on Active Thumbnail</source>
        <translation>Centrar sempre a miniatura ativa</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>If this option is set, then the active thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation>Se ativar esta opção, a miniatura ativa (isto é: a miniatura da imagem em exibição) será mantida no centro da barra de miniaturas (se possível). Se não ativar a opção, a miniatura ativa estará visível, mas não necessariamente no centro da barra de miniaturas.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="333"/>
        <source>Center on Active Thumbnails</source>
        <translation>Centrar na miniatura ativa</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="799"/>
        <source>Okay, do it!</source>
        <translation>Aplicar!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="804"/>
        <source>Nooo, don&apos;t!</source>
        <translation>Cancelar!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="106"/>
        <source>Window Manager</source>
        <translation>Gestor de janelas</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="260"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="401"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <translation>Existem diversas opções de imagens que podem ser definidas para o papel de parede.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="86"/>
        <source>Set as Wallpaper:</source>
        <translation>Definir como papel de parede:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="114"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation>O PhotoQt tenta detetar o gestor de janelas definido nas variáveis do seu ambiente de trabalho. Se a deteção automática não funcionar, pode definir o gestor de janelas manualmente.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="192"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <translation>O KDE4 ainda não disponibiliza a possibilidade de alterar o papel de parede sem ser através das definições do sistema. Infelizmente, não há nada que eu possa fazer.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="219"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <translation>O Plasma 5 ainda não disponibiliza a possibilidade de alterar o papel de parede sem ser através das definições do sistema. Esperemos que esta função seja disponibilizada mas, por enquanto, nada podemos fazer.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="250"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <translation>Aviso: parece que &apos;gsettings&apos; não está disponível! Tem a certeza que o Gnome/Unity está instalado?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="337"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <translation>Aviso: parece que &apos;xfconf-query&apos; não está disponível! Tem a certeza que o XFCE4 está instalado?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="350"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="505"/>
        <source>The wallpaper can be set to either of the available monitors (or any combination).</source>
        <translation>O papel de parede pode ser definido em todos os monitores disponíveis (ou combinação).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="366"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="521"/>
        <source>Screen #</source>
        <translation>Ecrã #</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="483"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console &gt; Add-ons &gt; Modules &gt; System.</source>
        <translation>Aviso: parece que o módulo &apos;msgbus&apos; (DBUS) não está ativado. Pode ativar o módulo em Definições -&gt; Extras -&gt; Módulos -&gt; Sistema.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="494"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <translation>Aviso: parece que &apos;enlightenment_remote&apos; não está disponível! Tem a certeza que o Enlightenment está instalado?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="556"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation>Pode definir o papel de parede para qualquer subseleção de áreas de trabalho</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="576"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="578"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="579"/>
        <source>Workspace #</source>
        <translation>Área de trabalho #</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="636"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <translation>Aviso: parece que &apos;feh&apos; não está instalado!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="647"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <translation>Aviso: parece que &apos;nitrogen&apos; não está instalado!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="658"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <translation>Aviso: parece que tanto &apos;feh&apos; como &apos;nitrogen&apos; não estão instalados!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="669"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.&lt;br&gt;This is intended particularly for window managers that don&apos;t natively support wallpapers (e.g., like Openbox).</source>
        <translation>O PhotoQt utiliza o &apos;feh&apos; ou &apos;nitrogen&apos; para mudar a imagem do ambiente de trabalho.&lt;br&gt;Esta opção é particularmente útil para gestores de janelas sem suporte a esta opção (ex.: Openbox).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="685"/>
        <source>Use &apos;feh&apos;</source>
        <extracomment>feh is an application, do not translate</extracomment>
        <translation>Utilizar &apos;feh&apos;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="692"/>
        <source>Use &apos;nitrogen&apos;</source>
        <extracomment>nitrogen is an application, do not translate</extracomment>
        <translation>Utilizar &apos;nitrogen&apos;</translation>
    </message>
</context>
</TS>
