<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/fadein/About.qml" line="113"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>PhotoQt est un logiciel simple de visualisation d&apos;images, avec une interface agréable et configurable, facile à utiliser et rapide.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="115"/>
        <source>With PhotoQt I try to be different than other image viewers (after all, there are plenty of good image viewers already out there). Its interface is kept very simple, yet there is an abundance of settings to customize the look and feel to make PhotoQt YOUR image viewer.</source>
        <translation>PhotoQt se veut être un logiciel de visualisation d&apos;images différent (après tout, il en existent déjà pas mal et de qualité). Son interface reste très simple, tout en proposant une abondance de paramètres pour en personnaliser la présentation et faire de PhotoQt VOTRE visionneuse d&apos;images.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="117"/>
        <source>I&apos;m not a trained programmer. I&apos;m a simple Maths student that loves doing stuff like this. Most of my programming knowledge I taught myself over the past 10-ish years, and it has been developing a lot since I started PhotoQt. During my studies in university I learned a lot about the basics of programming that I was missing. And simply working on PhotoQt gave me a lot of invaluable experience. So the code of PhotoQt might in places not quite be done in the best of ways, but I think it&apos;s getting better and better with each release.</source>
        <translation>Je ne suis pas un programmeur formé. Je suis un simple étudiant en Mathématiques qui aime faire des choses de ce type. La plus grande partie de mes connaissances en programmation fut acquise dans les 10 dernières années, et elles se sont sérieusement améliorées pendant le développement de PhotoQt. Lors de mes études à l&apos;université, j&apos;ai appris les bases de la programmation qui me manquaient. Et c&apos;est en développant PhotoQt que j&apos;ai acquis une expérience inestimable. Le code de PhotoQt peut donc ne pas être de la meilleure facture, mais je l&apos;améliore de version en version.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="121"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>N&apos;oubliez pas de visiter le site internet: </translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="123"/>
        <source>If you find a bug or if you have a question or suggestion, tell me. I&apos;m open to any feedback I get :)</source>
        <translation>Si vous trouvez un bug ou si vous avez une question ou une suggestion, dites-le moi. Je suis ouvert à toutes vos remarques :)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="174"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don&apos;t forget to add the %1 in your translation!!</extracomment>
        <translation>Vous voulez rejoindre l&apos;équipe et faire quelque chose, comme par exemple traduire PhotoQt dans une autre langue? Envoyez-moi un courriel (%1), et pour les traductions, vérifiez la page du projet sur Transifex:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="119"/>
        <source>I heard a number of times people saying, that PhotoQt is a &apos;copy&apos; of Picasa&apos;s image viewer. Well, it&apos;s not. In fact, I myself have never used Picasa. I have seen it in use though by others, and I can&apos;t deny that it influenced the basic design idea a little. But I&apos;m not trying to do something &apos;like Picasa&apos;. I try to do my own thing, and to do it as good as I can.</source>
        <translation>J&apos;ai entendu pas mal de fois des gens me dire que PhotoQt est une &apos;copie&apos; de la visionneuse d&apos;images de Picasa. Et bien, ce n&apos;est pas le cas. En fait, je n&apos;ai jamais utilisé Picasa. Je l&apos;ai vu en usage chez d&apos;autres, et je ne peux nier son influence dans l&apos;idée de base de sa conception. Mais je ne tente pas de faire quelque chose &apos;comme Picasa&apos;. Je tente de créer mon propre projet, et de le faire du mieux que je peux.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="158"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation>Merci à tous ceux qui ont contribué à PhotoQt et/ou à sa traduction! Vous êtes super!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>website:</source>
        <translation>Site internet:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>Licensed under GPLv2 or later, without any guarantee</source>
        <translation>Sous licence GPLv2 ou suivantes, sans aucun garantie.</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="52"/>
        <source>Move:</source>
        <extracomment>as in: &quot;Move file...&quot;</extracomment>
        <translation>Déplacer:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="62"/>
        <source>Previous</source>
        <extracomment>Go to previous file</extracomment>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="68"/>
        <source>Next</source>
        <extracomment>Go to next file</extracomment>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="99"/>
        <source>Rotate:</source>
        <extracomment>As in: Rotate file</extracomment>
        <translation>Pivoter:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="104"/>
        <source>Left</source>
        <extracomment>As in: rotate LEFT</extracomment>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="111"/>
        <source>Right</source>
        <extracomment>As in: Rotate RIGHT</extracomment>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="140"/>
        <source>Flip:</source>
        <extracomment>As in: Flip file</extracomment>
        <translation>Inverser:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="145"/>
        <source>Horizontal</source>
        <extracomment>As in: Flip file HORIZONTALLY</extracomment>
        <translation>Horizontalement</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="152"/>
        <source>Vertical</source>
        <extracomment>As in: Flip file VERTICALLY</extracomment>
        <translation>Verticalement</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="180"/>
        <source>Zoom:</source>
        <extracomment>Zoom file</extracomment>
        <translation>Zoom:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="185"/>
        <source>In</source>
        <extracomment>As in: Zoom IN</extracomment>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="191"/>
        <source>Out</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="197"/>
        <source>Actual</source>
        <extracomment>As in: Zoom to ACTUAL size</extracomment>
        <translation>Actuel</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="203"/>
        <source>Reset</source>
        <extracomment>As in: Reset zoom</extracomment>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="224"/>
        <source>Scale Image</source>
        <translation>Mise à l&apos;échelle de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="234"/>
        <source>Open in default File Manager</source>
        <translation>Ouvrir dans le gestionnaire de fichiers par défaut</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="266"/>
        <source>Rename File</source>
        <translation>Renommer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="276"/>
        <source>Delete File</source>
        <translation>Supprimer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="301"/>
        <source>Copy File</source>
        <translation>Copier le fichier</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="312"/>
        <source>Move File</source>
        <translation>Déplacer le fichier</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="11"/>
        <source>Confirm me?</source>
        <translation>Me confirmer?</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="12"/>
        <source>Do you really want to do this?</source>
        <translation>Voulez-vous vraiment faire cela?</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="13"/>
        <source>Yes, do it</source>
        <translation>Oui, le faire</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="14"/>
        <source>No, don&apos;t</source>
        <translation>Non, ne pas le faire</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="110"/>
        <source>Don&apos;t ask again</source>
        <translation>Ne plus me le redemander</translation>
    </message>
</context>
<context>
    <name>CustomDetectShortcut</name>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="72"/>
        <source>Detect key combination</source>
        <translation>Détecter mes combinaisons</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="90"/>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="127"/>
        <source>Press keys</source>
        <translation>Appuyer les touches</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="107"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>CustomExternalCommand</name>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="71"/>
        <source>External Command</source>
        <translation>Commande externe</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (with path)</source>
        <translation>fichier courant (avec chemin)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (without path)</source>
        <translation>fichier courant (sans chemin)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current directory</source>
        <translation>répertoire courant</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="122"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="135"/>
        <source>Save it</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="159"/>
        <source>Select Executeable</source>
        <translation>Sélectionner l&apos;exécutable</translation>
    </message>
</context>
<context>
    <name>CustomMouseShortcut</name>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="69"/>
        <source>Set Mouse Shortcut</source>
        <translation>Définir le raccourci souris</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="114"/>
        <source>Don&apos;t set</source>
        <translation>Ne pas définir</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="127"/>
        <source>Set Shortcut</source>
        <translation>Définir le raccourci</translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="68"/>
        <source>Delete File</source>
        <translation>Supprimer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="98"/>
        <source>Do you really want to delete this file?</source>
        <translation>Voulez-vous vraiment supprimer ce fichier?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Move to Trash</source>
        <translation>Déplacer vers la corbeille</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="137"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="150"/>
        <source>Delete permanently</source>
        <translation>Supprimer définitivement</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation>Entrée = Mettre à la corbeille, Maj+Entrée = Supprimer définitivement, Échappe = Abandon</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation>Entrée = Supprimer définitivement, Échappe = Abandon</translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <location filename="../qml/mainview/Display.qml" line="481"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="507"/>
        <source>Open a file to begin</source>
        <translation>Ouvrir un fichier pour commencer</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="558"/>
        <source>No results found...</source>
        <translation>Aucun résultat trouvé...</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="565"/>
        <source>Rotate Image?</source>
        <translation>Pivoter l&apos;image?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>The Exif data of this image says, that this image is supposed to be rotated.</source>
        <translation>Les données Exif de cette image indiquent que cette image devrait être pivotée.</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>Do you want to apply the rotation?</source>
        <translation>Souhaitez-vous appliquer la rotation?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="567"/>
        <source>Yes, do it</source>
        <translation>Oui, le faire</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="568"/>
        <source>No, don&apos;t</source>
        <translation>Non, ne pas le faire</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="68"/>
        <source>Filter images in current directory</source>
        <translation>Filtrer les images dans le répertoire courant</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="83"/>
        <source>Enter here the term you want to search for. Seperate multiple terms by a space.</source>
        <translation>Taper ici le terme recherché. Séparer les termes multiples par un espace.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="90"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>Si vous désirez limiter un terme à l&apos;extension de fichier, préfixez le terme d&apos;un point &quot;.&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="137"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="145"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="160"/>
        <source>Remove Filter</source>
        <translation>Supprimer le filtre</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="10"/>
        <source>Edit with</source>
        <translation>Éditer avec</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="13"/>
        <source>Open in</source>
        <translation>Ouvrir avec</translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="317"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="320"/>
        <source>Daylight</source>
        <translation>Lumière du jour</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="323"/>
        <source>Fluorescent</source>
        <translation>Fluorescent</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="326"/>
        <source>Tungsten (incandescent light)</source>
        <translation>Tungstène (lumière incandescente)</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="329"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="332"/>
        <source>Fine weather</source>
        <translation>Temps clair</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="335"/>
        <source>Cloudy Weather</source>
        <translation>Temps nuageux</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="338"/>
        <source>Shade</source>
        <translation>Ombre</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="341"/>
        <source>Daylight fluorescent</source>
        <translation>Fluorescent lumière du jour</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="344"/>
        <source>Day white fluorescent</source>
        <translation>Fluorescent blanc du jour</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="347"/>
        <source>Cool white fluorescent</source>
        <translation>Fluorescent blanc froid</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="350"/>
        <source>White fluorescent</source>
        <translation>Fluorescent blanc</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="353"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="356"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="359"/>
        <source>Standard light</source>
        <translation>Lumière standard</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="371"/>
        <source>D50</source>
        <translation>D50</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="374"/>
        <source>ISO studio tungsten</source>
        <translation>Tungstène studio ISO</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="377"/>
        <source>Other light source</source>
        <translation>Autre source de lumière</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="380"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source</extracomment>
        <translation>Source de lumière non valide</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="389"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired</extracomment>
        <translation>oui</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn&apos;t fired</extracomment>
        <translation>non</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="393"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash</extracomment>
        <translation>Pas de fonction flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="395"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>Retour flash non détecté</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>Retour flash détecté</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="399"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>mode flash obligatoire </translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="401"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>mode automatique</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>mode réduction des yeux rouges</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="405"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>Retour flash détecté</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="407"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>Retour flash non détecté</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="454"/>
        <source>Invalid flash</source>
        <translation> Flash non valide</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="467"/>
        <source>Landscape</source>
        <translation>Paysage</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="473"/>
        <source>Night Scene</source>
        <translation>Scène de nuit</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene</extracomment>
        <translation>Type de scène non valide</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="25"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="26"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="27"/>
        <source>Set as Wallpaper</source>
        <translation>Utiliser comme fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="28"/>
        <source>Start Slideshow</source>
        <translation>Démarrer le diaporama</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="29"/>
        <source>Filter Images in Folder</source>
        <translation>Filtrer les images dans le dossier</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="30"/>
        <source>Show/Hide Metadata</source>
        <translation>Afficher/Cacher les métadonnées</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="31"/>
        <source>About PhotoQt</source>
        <translation>À propos de PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="32"/>
        <source>Hide (System Tray)</source>
        <translation>Masquer (zone de notification)</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="33"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="120"/>
        <source>Quickstart</source>
        <translation>Démarrage rapide</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="47"/>
        <source>Open image file</source>
        <translation>Ouvrir un fichier image</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="121"/>
        <location filename="../cplusplus/mainwindow.cpp" line="122"/>
        <location filename="../cplusplus/mainwindow.cpp" line="124"/>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="126"/>
        <source>All Files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="537"/>
        <source>Image Viewer</source>
        <translation>Visionneuse d&apos;images</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="542"/>
        <source>Hide/Show PhotoQt</source>
        <translation>Afficher/Masquer PhotoQt</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="44"/>
        <source>No File Loaded</source>
        <translation>Aucun fichier chargé</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="62"/>
        <source>File Format Not Supported</source>
        <translation>Format de fichier non supporté</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="80"/>
        <source>Invalid File</source>
        <translation>Fichier non valide</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="121"/>
        <source>Keep Open</source>
        <translation>Garder ouvert</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="191"/>
        <source>Filesize</source>
        <translation>Taille de fichier</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="193"/>
        <location filename="../qml/slidein/MetaData.qml" line="196"/>
        <source>Dimensions</source>
        <translation>Dimensions</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="201"/>
        <source>Make</source>
        <translation>Fabriquant</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="202"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="203"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="205"/>
        <source>Time Photo was Taken</source>
        <translation>Heure de prise de vue</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="206"/>
        <source>Exposure Time</source>
        <translation>Temps d&apos;exposition</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="207"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="208"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="209"/>
        <source>Scene Type</source>
        <translation>Type de scène</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="210"/>
        <source>Focal Length</source>
        <translation>Longueur de focale</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="211"/>
        <source>F Number</source>
        <translation>Nombre F</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="212"/>
        <source>Light Source</source>
        <translation>Source de lumière</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="214"/>
        <source>Keywords</source>
        <translation>Mots-clés</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="215"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="216"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="218"/>
        <source>GPS Position</source>
        <translation>Position GPS</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="116"/>
        <source>Hide Counter</source>
        <translation>Masquer le Compteur</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="183"/>
        <source>Hide Filepath, leave Filename</source>
        <translation>Masquer le chemin, laisser le nom de fichier</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="191"/>
        <source>Hide both, Filename and Filepath</source>
        <translation>Cacher les deux, nom et chemin de fichier</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="236"/>
        <source>Filter:</source>
        <extracomment>As in: FILTER images</extracomment>
        <translation>Filter:</translation>
    </message>
</context>
<context>
    <name>QuickSettings</name>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="46"/>
        <source>Quick Settings</source>
        <translation>Paramétrage rapide</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="55"/>
        <source>Change settings with one click. They are saved and applied immediately. If you&apos;re unsure what a setting does, check the full settings for descriptions.</source>
        <translation>Changer les paramètres d&apos;un clic. Ceux-ci sont sauvegardés et appliqués immédiatement. En cas de doute sur l&apos;action des réglages, vérifiez les descriptions complètes.</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="77"/>
        <source>Sort by</source>
        <translation>Trier par</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Natural Name</source>
        <translation>Nom naturel</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>File Size</source>
        <translation>Taille de fichier</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="161"/>
        <source>Loop through folder</source>
        <translation>Parcourir le dossier en boucle</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="182"/>
        <source>Window mode</source>
        <translation>Mode fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="192"/>
        <source>Show window decoration</source>
        <translation>Afficher les décoration de fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="214"/>
        <source>Close on click on background</source>
        <translation>Fermer en cliquand sur le fond</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="235"/>
        <source>Keep thumbnails visible</source>
        <translation>Toujours afficher les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Normal thumbnails</source>
        <translation>Vignettes normales</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Dynamic thumbnails</source>
        <translation>Vignettes dynamiques</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="280"/>
        <source>Enable &apos;Quick Settings&apos;</source>
        <translation>Activer les &apos;réglages rapides&apos;</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Smart thumbnails</source>
        <translation>Vignettes intelligentes</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>No tray icon</source>
        <translation>Pas d&apos;icône systray</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Hide to tray icon</source>
        <translation>Cacher dans le systray</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation>Afficher dans le systray, mais sans l&apos;y cacher</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="305"/>
        <source>Show full settings</source>
        <translation>Afficher toutes les préférences</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="68"/>
        <source>Rename File</source>
        <translation>Renommer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="148"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="67"/>
        <source>Scale Image</source>
        <translation>Mise à l&apos;échelle de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="83"/>
        <source>Current Size:</source>
        <translation>Taille actuelle:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="112"/>
        <source>Error! Something went wrong, unable to save new dimension...</source>
        <translation>Erreur! Quelque chose s&apos;est mal passé, impossible de sauvegarder la nouvelle dimension...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="136"/>
        <source>New width:</source>
        <translation>Nouvelle largeur:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="143"/>
        <source>New height:</source>
        <translation>Nouvelle longueur:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="248"/>
        <source>Quality</source>
        <translation>Qualité</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="298"/>
        <source>Scale into new file</source>
        <translation>Mettre à l&apos;échelle dans un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="315"/>
        <source>Don&apos;t scale</source>
        <translation>Ne pas mettre à l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="217"/>
        <source>Aspect Ratio</source>
        <translation>Rapport hauteur/largeur de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="284"/>
        <source>Scale in place</source>
        <translation>Mise à l&apos;échelle écrasant le fichier</translation>
    </message>
</context>
<context>
    <name>SettingsItem</name>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="59"/>
        <source>Look and Feel</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="72"/>
        <location filename="../qml/settings/SettingsItem.qml" line="124"/>
        <source>Basic</source>
        <translation>Basique</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="93"/>
        <location filename="../qml/settings/SettingsItem.qml" line="143"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="115"/>
        <source>Thumbnails</source>
        <translation>Vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="174"/>
        <source>Metadata</source>
        <translation>Métadonnées</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="194"/>
        <source>Other Settings</source>
        <translation>Autres paramètres</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="219"/>
        <source>Filetypes</source>
        <translation>Types de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="240"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="313"/>
        <source>Restore Default Settings</source>
        <translation>Restaurer les paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="326"/>
        <source>Exit and Discard Changes</source>
        <translation>Annuler les changements et quitter</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="344"/>
        <source>Save Changes and Exit</source>
        <translation>Enregistrer les changements et quitter</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="358"/>
        <source>Clean Database</source>
        <translation>Nettoyer la base de données</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>Do you really want to clean up the database?</source>
        <translation>Voulez-vous vraiment nettoyer la base de données?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation>Ceci supprime toutes les vignettes obsolètes et rend  PhotoQt un peu plus rapide.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This process might take a little while.</source>
        <translation>Ce processus peut prendre du temps.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="360"/>
        <source>Yes, clean is good</source>
        <translation>Oui, nettoyer c&apos;est OK</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="361"/>
        <source>No, don&apos;t have time for that</source>
        <translation>Non, je n&apos;ai pas le temps</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="368"/>
        <source>Erase Database</source>
        <translation>Effacer la base de données</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>Do you really want to ERASE the entire database?</source>
        <translation>Voulez-vous vraiment EFFACER la base de données complète?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This removes every single item in the database! This step should never really be necessarily. After that, every thumbnail has to be newly re-created.</source>
        <translation>Ceci efface tous les éléments de la base de données. Cette action ne devrais jamais être vraiment nécessaire. Après cela, toutes les vignettes devront être recréées. </translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This step cannot be reversed!</source>
        <translation>Ceci est irréversible!</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="370"/>
        <source>Yes, get rid of it all</source>
        <translation>Oui, tout effacer</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="371"/>
        <source>Nooo, I want to keep it</source>
        <translation>Non, je veux la garder intacte</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="378"/>
        <source>Set Default Shortcuts</source>
        <translation>Établir les raccourcis par défaut</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="379"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation>Voulez-vous vraiment réinitialiser les raccourcis par défaut?</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="380"/>
        <source>Yes, please</source>
        <translation>Oui, merci</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="381"/>
        <source>Nah, don&apos;t</source>
        <translation>Non, ne pas le faire</translation>
    </message>
</context>
<context>
    <name>Slideshow</name>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="86"/>
        <source>Start a Slideshow</source>
        <translation>Démarrer un diaporama</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="97"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that is played in the background.</source>
        <translation>Différents paramètres peuvent être ajustés pour un diaporama: le temps entre les images, le type et la durée des transitions et également le fichier musical à jouer en arrière plan.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="104"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>Une fois les paramètres ajustées, vous pouvez démarrer le diaporama avec &quot;Démarrage rapide&quot;, i. e. en sautant cette étape.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="114"/>
        <source>Time in between</source>
        <translation>Intervalle</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="121"/>
        <source>Adjust the time between the images. The time specified here is the amount of time the image will be completely visible, i.e. the transitioning (if set) is not part of this time.</source>
        <translation>Ajuste le temps entre deux images. Le temps spécifié ici est le temps que l&apos;image sera visible complètement, i.e. le temps de transition (si actif) n&apos;est pas pris en compte. </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="161"/>
        <source>Smooth Transition</source>
        <translation>Transition douce</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="168"/>
        <source>Here you can set, if you want the images to fade into each other, and how fast they are to do that.</source>
        <translation>Ici, vous pouvez ajuster l&apos;utilisation et la durée du fondu entre deux images.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="182"/>
        <source>No Transition</source>
        <translation>Pas de transition</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="198"/>
        <source>Long Transition</source>
        <translation>Transition longue</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="210"/>
        <source>Shuffle and Loop</source>
        <translation>Lecture aléatoire et boucle</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="217"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note, that no image will be shown twice before every image has been shown once.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="223"/>
        <source>Loop over images</source>
        <translation>Boucler sur les images</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="229"/>
        <source>Shuffle images</source>
        <translation>Lecture aléatoire des images</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="241"/>
        <source>Hide Quickinfo</source>
        <translation>Masquer Infos</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="249"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>En fonction de votre configuration, PhotoQt affiche en haut de l&apos;écran des informations telles que la position dans le dossier courant ou le chemin/nom de fichier. Vous pouvez temporairement cet affichage pour le diaporama.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="254"/>
        <source>Hide Quickinfos</source>
        <translation>Masquer les bulles d&apos;infos</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="266"/>
        <source>Background Music</source>
        <translation>Musique d&apos;arrière plan</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="273"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>Certains aiment écouter de la musique pendant le diaporama. Vous pouvez sélectionner le fichier musical joué en arrière plan ici.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="280"/>
        <source>Enable Music</source>
        <translation>Activer la musique</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="310"/>
        <source>Click here to select music file...</source>
        <translation>Cliquer ici pour sélectionner le fichier musical...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="344"/>
        <source>Okay, lets start</source>
        <translation>Ok, démarrer </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="348"/>
        <source>Wait, maybe later</source>
        <translation>Attendez. Peut-être plus tard.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="352"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <translation>Enregistrer les changements mais sans démarrer immédiatement</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Select music file...</source>
        <translation>Sélectionner le fichier musical</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Music Files</source>
        <translation>Fichiers musicaux</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>All Files</source>
        <translation>Tous les fichiers</translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Play Slideshow</source>
        <translation>Lire le diaporama</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Pause Slideshow</source>
        <translation>Pause du diaporama</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="49"/>
        <source>Music Volume:</source>
        <translation>Volume de la musique:</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="77"/>
        <source>Exit Slideshow</source>
        <translation>Quitter le diaporama</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully installed</source>
        <translation>PhotoQt a été installé avec succés</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully updated</source>
        <translation>PhotoQt a été mis à jour avec succès</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>Welcome to PhotoQt. PhotoQt is an image viewer, aimed at being fast and reliable, highly customisable and good looking.</source>
        <translation>Bienvenue sur PhotoQt. PhotoQt est une visionneuse d&apos;images dont le but est d&apos;être rapide, hautement configurable et d&apos;apparence agréable. </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>This app started out more than three and a half years ago, and it has developed quite a bit since then. It has become very efficient, reliable, and highly flexible (check out the settings). I&apos;m convinced it can hold up to the more &apos;traditional&apos; image viewers out there in every way.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Welcome back to PhotoQt. It hasn&apos;t been that long since the last release of PhotoQt. Yet, it changed pretty much entirely, as it now is based on QtQuick rather than QWidgets. A large quantity of the code had to be re-written, while some chunks could be re-used. Thus, it is now more reliable than ever before and overall simply feels well rounded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>Many File Formats</source>
        <translation>Nombreux formats de fichiers</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>PhotoQt can make use of GraphicsMagick, an image library, to display many different image formats. Currently, there are up to 72 different file formats supported (exact number depends on your system)! You can find a list of it in the settings (Tab &apos;Other&apos;). There you can en-/disable different ones and also add custom file endings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>Make PhotoQt your own</source>
        <translation>Composer votre propre PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>PhotoQt has an extensive settings area. By default you can call it with the shortcut &apos;e&apos; or through the dropdown menu at the top edge towards the top right corner. You can adjust almost everything in PhotoQt, and it&apos;s certainly worth having a look there. Each setting usually comes with a little explanation text. Some of the most often used settings can also be conveniently adjusted in a slide-in widget, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Most images store some additional information within the file&apos;s metadata. PhotoQt can read and display a selection of this data. You can find this information in the slide-in window hidden behind the left edge of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>PhotoQt also brings a slideshow feature. When you start a slideshow, it starts at the currently displayed image. There are a couple of settings that can be set, like transition, speed, loop, and shuffle. Plus, you can set a music file that is played in the background. When the slideshow takes longer than the music file, then PhotoQt starts the music file all over from the beginning. At anytime during the slideshow, you can move the mouse cursor to the top edge of the screen to get a little bar, where you can pause/exit the slideshow and adjust the music volume.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>Thumbnails</source>
        <translation>Vignettes</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Here below you find a short overview of a selection of a few things PhotoQt has to offer, but feel free to skip it and just get started.</source>
        <translation>Vous trouverez ci-dessous un aperçu rapide des caractéristiques de PhotoQt. Mais n&apos;hésitez pas à sauter cette partie et à démarrer directement.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>What would be an image viewer without thumbnails support? It would only be half as good. Whenever you load an image, PhotoQt loads the other images in the directory in the background (by default, it tries to be smart about it and only loads the ones that are needed). It lines them up in a row at the bottom edge (move your mouse there to see them). There are many settings just for the thumbnails, like, e.g., size, liftup, en-/disabled, type, filename, permanently shown/hidden, etc. PhotoQt&apos;s quite flexible with that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>One of the many strengths of PhotoQt is the ability to easily set a shortcut for almost anything. Even mouse shortcuts are possible! You can choose from a huge number of internal functions, or you can run any custom script or command.</source>
        <translation>L&apos;un des points forts de PhotoQt est la possibilité de définir des raccourcis clavier pour presque tout. Il est même possible de définir des raccourcis pour la souris! Vous pouvez choisir parmi un grand nombre de fonctions internes, ou vous pouvez utiliser un script ou une commande personnalisés.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Image Information (Exif/IPTC)</source>
        <translation>Information de l&apos;image (Exif/IPTC)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>Slideshow</source>
        <translation>Diaporama</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>Localisation</source>
        <translation>Localisation</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>PhotoQt comes with a number of translations. Many have taken some of their time to create/update one of them (Thank you!). Not all of them are complete... do you want to help?</source>
        <translation>Photo est livré avec de nombreuses traductions. De nombreuses personnes ont pris sur leur temps pour les créer/mettre à jour (Merci!). Certaines traductions sont incomplètes... voulez-vous donner un coup de main ?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="355"/>
        <source>There are many many more features. Best is, you just give it a go. Don&apos;t forget to check out the settings to make PhotoQt YOUR image viewer. Enjoy :-)</source>
        <translation>Il y a de nombreuses autres fonctionnalités. Le mieux est simplement d&apos;essayer. N&apos;oubliez pas de jeter un oeil aux réglages pour que PhotoQt soit &quot;le vôtre&quot;. Profitez-en bien!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="387"/>
        <source>Okay, I got enough now. Lets start!</source>
        <translation>OK, ça suffit. Démarrer!</translation>
    </message>
</context>
<context>
    <name>TabDetails</name>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="55"/>
        <source>Image Metadata</source>
        <translation>Métadonnées de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="74"/>
        <source>PhotoQt can display different information of and about each image. The widget for this information is on the left outside the screen and slides in when mouse gets close to it and/or when the set shortcut (default Ctrl+E) is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <translation>PhotoQt peut afficher diverses information sur les images. Le composant pour cette information est sur la gauche de l&apos;écran et s&apos;affiche au survol de la souris ou grâce au raccourci clavier (Ctrl+E par défaut). La coche ci-dessous permet de désactiver l&apos;affichage par la souris. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Trigger Widget on Mouse Hovering</source>
        <translation>Afficher le composant au survol de la souris</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut. This can come in handy, if you get annoyed by accidentally opening the widget occasionally.</source>
        <translation>Par défaut, le composant exif peut être affiché de deux manières: temporairement en déplaçant la souris vers le bord gauche de l&apos;écran (le composant reste affiché tant que le curseur survole cette zone), ou de manière permanente en cochant la case (réglage propre à la session, ne peut pas être sauvegardé d&apos;une session à l&apos;autre!). Le composant peut également être affiché grâce à un raccourci clavier. L&apos;affichage à la souris peut être désactivé afin d&apos;utiliser uniquement le raccourci clavier. Ceci peut être pratique pour éviter l&apos;ouverture intempestive du composant. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="118"/>
        <source>Turn mouse triggering OFF</source>
        <translation>Désactiver l&apos;affichage au survol de la souris</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>Which items are shown?</source>
        <translation>Quels éléments sont affichés?</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>PhotoQt can display a number of information about the image (often called &apos;Exif data&apos;&apos;). However, you might not be interested in all of them, hence you can choose to disable some of them here.</source>
        <translation>PhotoQt peut afficher de nombreuses informations à propos de l&apos;image (souvent nommées &quot;données Exif&quot;). Vous pouvez  cependant choisir d&apos;en désactiver certaines ici.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="149"/>
        <source>Enable ALL</source>
        <translation>Activer TOUT</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="155"/>
        <source>Disable ALL</source>
        <translation>Désactiver TOUT</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="179"/>
        <source>Filesize</source>
        <translation>Taille de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="180"/>
        <source>Dimensions</source>
        <translation>Dimensions</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="181"/>
        <source>Make</source>
        <translation>Fabriquant</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="182"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="183"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="184"/>
        <source>Time Photo was Taken</source>
        <translation>Heure de prise de vue</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="185"/>
        <source>Exposure Time</source>
        <translation>Temps d&apos;exposition</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="186"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="187"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="188"/>
        <source>Scene Type</source>
        <translation>Type de scène</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="189"/>
        <source>Focal Length</source>
        <translation>Longueur de Focale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="190"/>
        <source>F-Number</source>
        <translation>Nombre F</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="191"/>
        <source>Light Source</source>
        <translation>Source de lumière</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="192"/>
        <source>Keywords</source>
        <translation>Mots-clés</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="193"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="194"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="195"/>
        <source>GPS Position</source>
        <translation>Position GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Adjusting Font Size</source>
        <translation>Ajuster la taille de police</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Computers can have very different resolutions. On some of them, it might be nice to increase the font size of the labels to have them easier readable. Often, a size of 8 or 9 should be working quite well...</source>
        <translation>Les écrans peuvent avoir différentes résolutions. Sur certains, il peut être intéressant d&apos;augmenter la taille de police pour rendre l&apos;affichage plus lisible. Une taille de 8 ou 9 devrait convenir dans la plupart des cas... </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Rotating/Flipping Image according to Exif Data</source>
        <translation>Pivoter/Inverser l&apos;image d&apos;après les données Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly. When asking PhotoQt to always rotate images automatically without asking, it already does so at image load (including thumbnails).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="294"/>
        <source>Never rotate/flip images</source>
        <translation>Ne jamais pivoter les images</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="300"/>
        <source>Always rotate/flip images</source>
        <translation>Toujours pivoter les images</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="305"/>
        <source>Always ask</source>
        <translation>Toujours demander</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>Online map for GPS</source>
        <translation>Carte en ligne pour le GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>If you&apos;re image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>Si votre image inclut une position GPS, un clic permet d&apos;afficher cette position sur une carte en ligne dans le navigateur par défaut. Ici, vous pouvez définir le service de cartes en ligne à utiliser (autres suggestions bienvenues).</translation>
    </message>
</context>
<context>
    <name>TabFiletypes</name>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="55"/>
        <source>Filetypes</source>
        <translation>Types de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>File Types - Qt</source>
        <translation>Types de fichiers - Qt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.&lt;br&gt;If a file ending for one of the formats is missing, you can add it below, formatted like &apos;*.ending&apos; (without single quotation marks), multiple entries seperated by commas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="102"/>
        <source>Extra File Types:</source>
        <translation>Types de fichiers supplémentaires:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="117"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="166"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="214"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="337"/>
        <source>Mark None</source>
        <translation>Marquer aucun</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="123"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="172"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="220"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="343"/>
        <source>Mark All</source>
        <translation>Tout marquer</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>File Types - GraphicsMagick</source>
        <translation>Types de fichiers - GraphicsMagick</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. If you prefer not to have one or the other enabled in PhotoQt, you can simply disable individual formats below.&lt;br&gt;There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation>PhotoQt fait appel à GraphicsMagick pour supporter de nombreux formats d&apos;image différents. La liste ci-dessous fournit les formats d&apos;image affichés correctement avec les images test. Si vous préférez que certains formats ne soient pas activés dans PhotoQt, vous pouvez simplement les désélectionner ci-dessous. &lt;br&gt; Quelques formats n&apos;ont pas été testés avec PhotoQt (par manque d&apos;image test). Ils sont listés dans la catégorie &quot;Non testé&quot; ci-dessous. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>File Types - GraphicsMagick (requires Ghostscript)</source>
        <translation>Types de fichiers - GraphicsMagick (Nécessite Ghostscript)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and work. However, they require Ghostscript to be installed on the system.</source>
        <translation>Les types de fichiers suivants sont supportés par GraphicsMagick, ils ont été testés et fonctionnent. Cependant, ils nécessitent que Ghostscript soit installé sur le système.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>File Types - Other tools required</source>
        <translation>Types de fichiers - Autres outils requis</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation>Les types de fichiers suivants sont supportés seulement par des outils tiers. Vous devez d&apos;abord les installer pour pouvoir les utiliser.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>If an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <source>Gimp&apos;s XCF file format.</source>
        <extracomment>&apos;Makes use of&apos; is in connection with an external tool (i.e., it &apos;makes use of&apos; tool abc)</extracomment>
        <translation>Format de fichier XCF de GIMP.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Makes use of</source>
        <translation>Fait appel à</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Adobe Photoshop PSD and PSB.</source>
        <translation>Adobe Photoshop PSD et PSB.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>File Types - GraphicsMagick (Untested)</source>
        <translation>Types de fichiers - GraphicsMagick (Non testé)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>The following file types are generally supported by GraphicsMagick, but I wasn&apos;t able to test them in PhotoQt (due to lack of test images). They might very well be working, but I simply can&apos;t say. If you decide to enable some of the, the worst that could happen ist, that you see an error image instead of the actual image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>If you happen to have an image in one of those formats and don&apos;t mind sending it to me, that&apos;d be really cool...</source>
        <translation>Si par hasard vous avez une image dans l&apos;un de ces formats que ca ne vous dérange pas de m&apos;envoyer, ça serait vraiment sympa...</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelAdvanced</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="54"/>
        <source>Advanced Settings</source>
        <translation>Réglages avancés</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>Background of PhotoQt</source>
        <translation>Fond de PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above.&lt;br&gt;Note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="91"/>
        <source>Use (half-)transparent background</source>
        <translation>Utiliser un fond (semi)-transparent</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="97"/>
        <source>Use faked transparency</source>
        <translation>Utiliser la fausse transparenc</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="102"/>
        <source>Use custom background image</source>
        <translation>Utiliser une image de fond personnalisée</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="107"/>
        <source>Use one-coloured, non-transparent background</source>
        <translation>Utiliser un fond uni non transparent</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="164"/>
        <source>No image selected</source>
        <translation>Aucune image sélectionnée</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="188"/>
        <source>Scale to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="194"/>
        <source>Scale and Crop to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="199"/>
        <source>Stretch to fit</source>
        <translation>Étirer pour ajuster à l&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="204"/>
        <source>Center image</source>
        <translation>Centrer l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="209"/>
        <source>Tile image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Background/Overlay Color</source>
        <translation>Couleur d&apos;arrière plan / de superposition </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>Ici, vous pouvez ajuster la couleur de fond de PhotoQt (couleur de la partie non couverte par une image). Lorsqu&apos;un compositeur ou une image de fond est utilisé, il est également possible de spécifier une valeur alpha, i.e. la transparence de la couleur de la couche de superposition. Lorsque le compositeur est désactivé et qu&apos;aucune image n&apos;est sélectionnée, cette couleur sera le fond non-transparent de PhotoQt. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="259"/>
        <source>Red:</source>
        <translation>Rouge:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="281"/>
        <source>Green:</source>
        <translation>Vert:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="303"/>
        <source>Blue:</source>
        <translation>Bleu:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="325"/>
        <source>Alpha:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="387"/>
        <source>Preview colour</source>
        <translation>Prévisualiser la couleur</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Border Around Image</source>
        <translation>Bordure autour de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels (looks better). Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>Par défaut, à l&apos;ouverture, les images ne s&apos;affichent pas en plein écran mais avec une bordure de quelques pixels (pour des raisons esthétiques). Ici, vous pouvez ajuster la largeur de cette bordure (fixer à 0 pour désactiver complètement la bordure). </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Close on Click in empty area</source>
        <translation>Fermer en cliquant dans une zone vide</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. It can be a nice feature, PhotoQt will feel even more like a &quot;floating layer&quot;. However, you might at times close PhotoQt accidentally.</source>
        <translation>Cette option permet à PhotoQt de fonctionner comme un peu comme les visionneuses d&apos;images en javascript que l&apos;on trouve sur internet. Un clic en dehors de l&apos;image sur le fond gris fermera l&apos;application. C&apos;est une fonctionnalité intéressante: PhotoQt donnera encore plus l&apos;impression de &quot;flotter&quot;. Vous risquez cependant de fermer PhotoQt accidentellement.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Note: If you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>Note : Si vous utilisez un clic de souris comme raccourci, cette option n&apos;aura aucun d&apos;effet !</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="475"/>
        <source>Close on click in empty area</source>
        <translation>Fermer en cliquant dans une zone vide</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>Looping Through Folder</source>
        <translation>Dossier en boucle</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>When you load the last image in a directory and select &apos;Next&apos;, PhotoQt automatically jumps to the first image (and vice versa: if you select &apos;Previous&apos; while having the first image loaded, PhotoQt jumps to the last image). Disabling this option makes PhotoQt stop at the first/last image (i.e. selecting &apos;Next&apos;/&apos;Previous&apos; will have no effect in these two special cases).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="494"/>
        <source>Loop through folder</source>
        <translation>Parcourir le dossier en boucle</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Smooth Transition</source>
        <translation>Transition douce</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced by the new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="526"/>
        <source>No Transition</source>
        <translation>Pas de transition</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="546"/>
        <source>Long Transition</source>
        <translation>Transition longue</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Menu Sensitivity</source>
        <translation>Sensibilité du menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Here you can adjust the sensitivity of the drop-down menu. The menu opens when your mouse cursor gets close to the right side of the upper edge. Here you can adjust how close you need to get for it to open.</source>
        <translation>Ici, vous pouvez ajuster la sensibilité du menu déroulant. Le menu s&apos;affiche à l&apos;approche du curseur vers le bord de l&apos;écran en haut à droite. Ici, vous pouvez ajuster la distance du curseur permettant l&apos;affichage du menu. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="583"/>
        <source>Low Sensitivity</source>
        <translation>Sensibilité faible</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="603"/>
        <source>High Sensitivity</source>
        <translation>Sensibilité élevée</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation>Sensibilité de la roulette de la souris</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Here you can adjust the sensitivity of the mouse wheel. For example, if you have set the mouse wheel up/down for switching back and forth between images, then a lower sensitivity means that you will have to scroll further for triggering a shortcut. Per default it is set to the highest sensitivity, i.e. every single wheel movement is evaluated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="640"/>
        <source>Very sensitive</source>
        <translation>Très sensible</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="660"/>
        <source>Not at all sensitive</source>
        <translation>Pas du tout sensible </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>Remember per session</source>
        <translation>Mémoriser par session</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>If you would like PhotoQt to remember the rotation/flipping and/or zoom level per session (not permanent), then you can enable it here. If not set, then every time a new image is displayed, it is displayed neither zoomed nor rotated nor flipped (one could say, it is displayed &apos;normal&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="697"/>
        <source>Remember Rotation/Flip</source>
        <translation>Mémoriser Pivoter/Inverser</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="702"/>
        <source>Remember Zoom Level</source>
        <translation>Mémoriser le niveau de zoom</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation and Window Geometry</source>
        <translation>Géométrie d&apos;animation et de fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="748"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation of fade-in widgets (like, e.g., Settings or About Widget)</source>
        <translation>Composants d&apos;animation ou de fondu (comme, p. ex., Paramètres ou À propos du composant)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation>Sauvegarder et restituer la géométrie de fenêtre: Lorsque l&apos;on quitte PhotoQt, la taille et la position de la fenêtre sont stockées afin d&apos;être restituées au prochain démarrage.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="738"/>
        <source>Animate all fade-in elements</source>
        <translation>Animer tous les éléments de fondu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="743"/>
        <source>Save and restore window geometry</source>
        <translation>Sauvegarder et rétablir la géométrie de fenêtre</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelBasic</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Réglages de base</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Sort Images</source>
        <translation>Trier les images</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Here you can adjust, how the images in a folder are supposed to be sorted. You can sort them by Filename, Natural Name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), File Size, and Date. Also, you can reverse the sorting order from ascending to descending if wanted.</source>
        <translation>Ici vous pouvez paramétrer la façon dont les images doivent être triées dans un dossier. Vous pouvez les trier par Nom de fichier, Nom naturel (p.e., fichier10.jpg vient après fichier9.jpg et pas après fichier1.jpg), Taille de fichier et Date. Vous pouvez également inverser l&apos;ordre de tri d&apos;ascendant à descendant si besoin.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Hint: You can also change this setting very quickly from the &apos;Quick Settings&apos; window, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="90"/>
        <source>Sort by:</source>
        <translation>Trier par:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Natural Name</source>
        <translation>Nom naturel</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Filesize</source>
        <translation>Taille de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="105"/>
        <source>Ascending</source>
        <translation>Ordre croissant</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="113"/>
        <source>Descending</source>
        <translation>Ordre décroissant</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>Window Mode</source>
        <translation>Mode fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>PhotoQt is designed with the space of a fullscreen app in mind. That&apos;s why it by default runs as fullscreen. However, some might prefer to have it as a normal window, e.g. so that they can see the panel.</source>
        <translation>PhotoQt est conçu pour être utilisé en plein écran. C&apos;est pourquoi il s&apos;affiche en plein écran par défaut. Cependant, certains peuvent préférer l&apos;utiliser en mode fenêtre, afin de voir le tableau de bord. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="151"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>Utiliser PhotoQt en mode fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="159"/>
        <source>Show Window Decoration</source>
        <translation>Afficher les décoration de fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>Hide to Tray Icon</source>
        <translation>Masquer vers la zone de notification</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>When started PhotoQt creates a tray icon in the system tray. If desired, you can set PhotoQt to minimise to the tray instead of quitting. This causes PhotoQt to be almost instantaneously available when an image is opened.&lt;br&gt;It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>No tray icon</source>
        <translation>Pas d&apos;icône de barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Hide to tray icon</source>
        <translation>Masquer vers la zone de notification</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation type="unfinished">Afficher dans le systray, mais sans l&apos;y cacher</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>Closing &apos;X&apos; (top right)</source>
        <translation>Quitter &apos;X&apos; (en haut à droite) </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>There are two looks for the closing &apos;x&apos; at the top right: a normal &apos;x&apos;, or a slightly more fancy &apos;x&apos;. Here you can switch back and forth between both of them, and also change their size. If you prefer not to have a closing &apos;x&apos; at all, see below for an option to hide it.</source>
        <translation>Il y a deux aspects possibles pour le &apos;x&apos; de fermeture en haut à droite: un &apos;x&apos; normal, ou un &apos;x&apos; légèrement plus élégant. Ici vous pouvez passer de l&apos;un à l&apos;autre et en changer la taille. Si vous préférez ne pas avoir de &apos;x&apos; de fermeture du tout, voir ci-dessous pour une option pour le cacher. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="216"/>
        <source>Normal Look</source>
        <translation>Aspect normal</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="223"/>
        <source>Fancy Look</source>
        <translation>Aspect élégant</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="249"/>
        <source>Small Size</source>
        <translation>Petite taille</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="264"/>
        <source>Large Size</source>
        <translation>Grande taille</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>Fit Image in Window</source>
        <translation>Ajuster l&apos;image à la fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can zoom those images to make them fir into the window. However, keep in mind, that such images will look pixelated to a certain degree (depending on each image).</source>
        <translation>Si la dimension des images est plus petite que la dimension de l&apos;écran, PhotoQt peut zoomer ces images pour les ajuster à la fenêtre. Mais, ce type d&apos;ajustement peut rendre à un certain degré une image pixelisée (dépendant de chaque image).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="287"/>
        <source>Fit Images in Window</source>
        <translation>Ajuster les images à la fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Hide Quickinfo (Text Labels)</source>
        <translation>Masquer les bulles (étiquettes de texte)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Here you can hide the text labels shown in the main area: The Counter in the top left corner, the file path/name following the counter, and the &quot;X&quot; displayed in the top right corner. The labels can also be hidden by simply right-clicking on them and selecting &quot;Hide&quot;.</source>
        <translation>Ici vous pouvez cacher les étiquettes de texte affichées dans la zone principale : le compteur dans le coin en haut à gauche, le nom du fichier à côté du compteur, et le &quot;X&quot; affiché dans le coin en haut à droite. Clic droit &gt; &quot;Masquer&quot; sur les étiquettes permet également de les masquer. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="321"/>
        <source>Hide Counter</source>
        <translation>Masquer le Compteur</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="326"/>
        <source>Hide Filepath (Shows only file name)</source>
        <translation>Masquer le chemin (afficher seulement le nom de fichier)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="331"/>
        <source>Hide Filename (Including file path)</source>
        <translation>Masquer le nom de fichier (y compris le chemin)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="336"/>
        <source>Hide &quot;X&quot; (Closing)</source>
        <translation>Masquer &quot;X&quot; (Fermer)</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="55"/>
        <source>Other Settings</source>
        <translation>Autres paramètres</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>Choose Language</source>
        <translation>Choisissez la langue</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>Un bon nombre de traductions est disponible. Merci à tous ceux qui ont pris le temps de traduire PhotoQt!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>Quick Settings</source>
        <translation>Réglages rapides</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>The &apos;Quick Settings&apos; is a widget hidden on the right side of the screen. When you move the cursor there, it shows up, and you can adjust a few simple settings on the spot without having to go through this settings dialog. Of course, only a small subset of settings is available (the ones needed most often). Here you can disable the dialog so that it doesn&apos;t show on mouse movement anymore.</source>
        <translation>Le composant &quot;Paramétrage rapide&quot; est caché du côté droit de l&apos;écran. Lorsque vous bougez le curseur dans cette zone, il apparaît, et vous pouvez y ajuster quelques réglages à la volée, sans avoir à passer par les boîtes de dialogue des réglages. Bien sûr, seulement quelques réglages sont disponibles (ceux dont on a besoin le plus souvent). Vous pouvez désactiver cette option ici si vous ne désirez pas son apparition par survol de la zone.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="129"/>
        <source>Show &apos;Quick Settings&apos; on mouse hovering</source>
        <translation>Afficher les &apos;Réglages rapides&apos; au survol de la souris</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Adjust Context Menu</source>
        <translation>Ajuster le menu contextuel</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Here you can adjust the context menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>Ici, vous pouvez ajuster le menu contextuel. Vous pouvez glisser/déposer les entrées, les éditer, en ajouter de nouvelles ou supprimer celles qui existent. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="183"/>
        <source>Executable</source>
        <translation>Exécutable</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="199"/>
        <source>Menu Text</source>
        <translation>Texte du Menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="223"/>
        <source>Add new context menu entry</source>
        <translation>Ajouter une nouvelle entrée au menu contextuel</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="231"/>
        <source>(Re-)set automatically</source>
        <translation>(Ré)initialiser automatiquement</translation>
    </message>
</context>
<context>
    <name>TabOtherContext</name>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="116"/>
        <source>Click here to drag</source>
        <translation>Cliquez ici pour glisser/déposer</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="186"/>
        <source>quit</source>
        <translation>quitter</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="54"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="73"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available function you can either double click on the tile or click the &quot;+&quot; button. This automatically opens another widget where you can set a key combination.</source>
        <translation>Ici, vous pouvez ajouter, supprimer ou éditer les raccourcis clavier. Les raccourcis sont groupés en 4 catégories pour les commandes internes et une pour les commandes externes. Les cases de droite représentent toutes les commandes possibles. Pour ajouter un raccourci, vous pouvez double-cliquer sur le titre ou cliquer sur le bouton &quot;+&quot;. Ceci ouvre automatiquement un autre composant où vous pouvez définir la combinaison de touches du raccourci. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="89"/>
        <source>Set default shortcuts</source>
        <translation>Établir les raccourcis par défaut</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="96"/>
        <source>Navigation</source>
        <translation>Navigatio</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Open New File</source>
        <translation>Ouvrir un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Filter Images in Folder</source>
        <translation>Filtrer les images dans le dossier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Next Image</source>
        <translation>Image suivante</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Previous Image</source>
        <translation>Image précédente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to first Image</source>
        <translation>Aller à la première image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to last Image</source>
        <translation>Aller à la dernière image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Hide to System Tray</source>
        <translation>Cacher vers la zone de notification</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Quit PhotoQt</source>
        <translation>Quitter PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="104"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom In</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom Out</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom to Actual Size</source>
        <translation>Zoomer à la taille réelle</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Zoom</source>
        <translation>Réinitialiser le zoom</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Right</source>
        <translation>Pivoter vers la droite</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Left</source>
        <translation>Pivoter vers la gauche</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Rotation</source>
        <translation>Réinitialiser la rotation</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Horizontally</source>
        <translation>Inverser horizontalement</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Vertically</source>
        <translation>Inverser verticalement</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Scale Image</source>
        <translation>Mise à l&apos;échelle de l&apos;image</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="112"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Rename File</source>
        <translation>Renommer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Delete File</source>
        <translation>Supprimer le fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Copy File to a New Location</source>
        <translation>Copier le fichier dans un nouveau répertoire</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Move File to a New Location</source>
        <translation>Déplacer le fichier dans un nouveau répertoire</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="120"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Interrupt Thumbnail Creation</source>
        <translation>Interrompre la création des vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Reload Thumbnails</source>
        <translation>Recharger les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Hide/Show Exif Info</source>
        <translation>Afficher / cacher les informations Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Context Menu</source>
        <translation>Afficher le menu contextuel</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Settings</source>
        <translation>Afficher les préférences</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow</source>
        <translation>Démarrer le diaporama</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Démarrer le diaporama (démarrage rapide)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>About PhotoQt</source>
        <translation>À propos de PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Set as Wallpaper</source>
        <translation>Utiliser comme fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="128"/>
        <source>Extern</source>
        <translation>Externe</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="132"/>
        <source>EXTERN</source>
        <extracomment>Is the shortcut tile text for EXTERNal shortcuts</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabShortcutsCategories</name>
    <message>
        <location filename="../qml/settings/TabShortcutsCategories.qml" line="32"/>
        <source>Category:</source>
        <translation>Catégorie:</translation>
    </message>
</context>
<context>
    <name>TabShortcutsTilesAvail</name>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="98"/>
        <source>key</source>
        <extracomment>tile text for KEY shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>touche</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="154"/>
        <source>mouse</source>
        <extracomment>tile text for MOUSE shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>souris</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsAdvanced</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="52"/>
        <source>Advanced Settings</source>
        <translation>Réglages avancés</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Change Thumbnail Position</source>
        <translation>Changer la position des vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Per default the bar with the thumbnails is shown at the lower edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge, so that&apos;s what can be changed here.</source>
        <translation>Par défaut, le bandeau de vignettes est affiché en bas de l&apos;écran. Certains utilisateurs peuvent préférer afficher ce bandeau en haut et ceci peut être ajusté ici. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="86"/>
        <source>Show at lower edge</source>
        <translation>Afficher en bas</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="93"/>
        <source>Show at upper edge</source>
        <translation>Afficher en haut</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>Filename? Dimension? Or both?</source>
        <translation>Nom de fichier? Taille? Ou les deux? </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="132"/>
        <source>Write Filename</source>
        <translation>Afficher le nom du fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>Use file-name-only Thumbnails</source>
        <translation>Utiliser seulement les noms de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a file-name-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>Si vous ne souhaitez pas que PhotoQt charge les vignettes en tâche de fond, tout en conservant les facilités de navigation, vous pouvez utiliser uniquement les noms de fichiers. PhotoQt ne chargera plus les vignettes mais mettra simplement les noms de fichiers dans les boites. Vous pouvez ajuster la taille de police de ce texte. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>When thumbnails are displayed at the top/bottom, PhotoQt usually writes the filename on them (if not disabled). You can also use the slider below to adjust the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="213"/>
        <source>Use filename-only thumbnail</source>
        <translation>Utiliser seulement le nom de fichier</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>Disable Thumbnails</source>
        <translation>Désactiver les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This option can also be toggled remotely via command line (run &apos;photoqt --help&apos; for more information on that). This might increase the speed of PhotoQt a good bit, however, navigating through a folder might be a little harder without thumbnails.</source>
        <translation>Si vous n&apos;avez pas du tout besoin des vignettes, vous pouvez les désactiver complètement. Cette option peut également être ajustée via la ligne de commandes (lancez &apos;photoqt --help&apos; pour plus d&apos;informations). Ceci peut améliorer significativement les performances de PhotoQt mais naviguer dans un dossier peut être un peu plus difficile sans les vignettes.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="287"/>
        <source>Disable Thumbnails altogether</source>
        <translation>Désactiver complètement les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnail Cache</source>
        <translation>Cache des vignettes </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnails can be cached in two different ways:&lt;br&gt;1) File Caching (following the freedesktop.org standard) or&lt;br&gt;2) Database Caching (better performance and management, default option).</source>
        <translation>Les vignettes peuvent être mises en cache de deux manières:&lt;br&gt;1) mise en cache des fichiers (suivant les standards de freedesktop.org) ou &lt;br&gt;2) mise en cache de la base de données (gestion et performance améliorées, option par défaut).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <translation>Les deux approches ont leurs avantages et inconvénients:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file. However, it&apos;s not possible to check for obsolete thumbnails (thus this may lead to many unneeded thumbnail files).</source>
        <translation>La mise en cache des fichiers suit le standard de freedesktop.org afin que plusieurs applications puissent partager la vignette d&apos;un fichier image donné. Il n&apos;est cependant pas possible de vérifier si des vignettes sont obsolètes (certaines vignettes sont donc inutiles).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it brings a slightly better performance, and it allows a better handling of existing thumbnails (e.g. deleting obsolete thumbnails).</source>
        <translation>La mise en cache de la base de données ne permet par le partage de vignettes entre applications (par conséquent, toutes les vignettes doivent être créées par PhotoQt), mais la gestion des vignettes existantes et les performances sont améliorées (e.g., il est possible de supprimer les vignettes obsolètes).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>PhotoQt works with either option, though the second way is set as default.</source>
        <translation>PhotoQt fonctionne avec l&apos;une ou l&apos;autre des deux options. La seconde est activée par défaut. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, that does affect the performance and usability of PhotoQt, since thumbnails have to be newly re-created every time they are needed.</source>
        <translation>Bien que l&apos;utilisation de l&apos;une ou l&apos;autre option soit encouragée, la mise en cache peut être complètement désactivée. Cependant, ceci affecte la performance et la facilité d&apos;utilisation de PhotoQt car les vignettes sont recréées chaque fois qu&apos;elles sont nécessaires.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="327"/>
        <source>Enable Thumbnail Cache</source>
        <translation>Activer le cache des vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="348"/>
        <source>File Caching</source>
        <translation>Cache des fichiers</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="354"/>
        <source>Database Caching</source>
        <translation>Cache de la base de données</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="379"/>
        <source>Current database filesize:</source>
        <translation>Taille actuelle de la base de données:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="405"/>
        <source>Entries in database:</source>
        <translation>Nombre d&apos;entrées dans la base de données:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="426"/>
        <source>CLEAN UP database</source>
        <translation>NETTOYER la base de données</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="438"/>
        <source>ERASE database</source>
        <translation>EFFACER la base de données</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsBasic</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Réglages de base</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Thumbnail Size</source>
        <translation>Taille des vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but with different screen resolutions it might be nice to have them larger/smaller.</source>
        <translation>Ici, vous pouvez ajuster la taille des vignettes entre 20 et 256 pixels. La valeur par défaut est de 80 pixels mais il peut être utile d&apos;agrandir / réduire les vignettes suivant la résolution de l&apos;écran. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>Spacing Between Thumbnail Images</source>
        <translation>Espace entre les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them, however exactly that can be changed here.</source>
        <translation>Les vignettes sont affichées en ligne en haut ou en bas de l&apos;écran (suivant vos réglages). Elles sont alignées côte à côte. Par défaut, il n&apos;y a pas d&apos;espace entre les vignettes mais ceci peut être ajusté ici. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>Lift-up of Thumbnail Images on Hovering</source>
        <translation>Surélever les vignettes au survol</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels (default 10). Here you can increase/decrease this value according to your personal preference.</source>
        <translation>Au survol de la souris, les vignettes sont surélevées de quelques pixels (par défaut: 10). Ici, vous pouvez personnaliser cette valeur. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Keep Thumbnails Visible</source>
        <translation>Toujours afficher les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Per default the Thumbnails slide out over the edge of the screen. Here you can force them to stay visible. The big image is shrunk to fit into the empty space. Note, that the thumbnails will be hidden (and only shown on mouse hovering) once you zoomed the image in/out. Resetting the zoom restores the original visibility of the thumbnails.</source>
        <translation>Par défaut, les vignettes sont masquées lorsqu&apos;elles arrivent en bordure d&apos;écran. Ici, vous pouvez forcer les vignettes à rester toujours visibles. L&apos;image principale est alors réduite pour s&apos;ajuster à l&apos;espace vide sur l&apos;écran. Les vignettes sont masquées (et affichées de nouveau au survol de la souris) une fois que l&apos;image principale est zoomée. Réinitialiser le zoom rétablit l&apos;affichage initial des vignettes. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="259"/>
        <source>Keep Thumnails Visible</source>
        <translation>Toujours afficher les vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic Thumbnail Creation</source>
        <translation>Création dynamique des vignettes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic thumbnail creation means, that PhotoQt only sets up those thumbnail images that are actually needed, i.e. it stops once it reaches the end of the visible area and sits idle until you scroll left/right.</source>
        <translation>Création dynamique des vignettes signifie que PhotoQt crée uniquement les vignettes nécessaires, i.e., il stoppe lorsqu&apos;il atteint la fin de la zone visible et attend jusqu&apos;à ce que vous fassiez défiler vers la droite ou la gauche. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Smart thumbnails are similar in nature. However, they make use of the fast, that once a thumbnail has been created, it can be loaded very quickly and efficiently. It also first loads all of the currently visible thumbnails, but it doesn&apos;t stop there: Any thumbnails (even if invisible at the moment) that once have been created are loaded. This is a nice compromise between efficiency and usability.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Enabling either the smart or dynamic option is recommended, as it increases the performance of PhotoQt significantly, while preserving the usability.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="295"/>
        <source>Normal Thumbnails</source>
        <translation>Vignettes normales</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="301"/>
        <source>Dynamic Thumbnails</source>
        <translation>Vignettes dynamiques</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="307"/>
        <source>Smart Thumbnail</source>
        <translation>Vignettes intelligentes</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>Always center on Active Thumbnail</source>
        <translation>Toujours centrer sur la vignette active</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>If this option is set, then the active thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="333"/>
        <source>Center on Active Thumbnails</source>
        <translation>Centrer sur les vignettes actives</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="799"/>
        <source>Okay, do it!</source>
        <translation>OK, changer!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="804"/>
        <source>Nooo, don&apos;t!</source>
        <translation>Non, annuler!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="106"/>
        <source>Window Manager</source>
        <translation>Gestionnaire de fenêtres</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="260"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="401"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <translation>Plusieurs options d&apos;image peuvent être ajustées pour l&apos;image de fond.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="86"/>
        <source>Set as Wallpaper:</source>
        <translation>Utiliser comme fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="114"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="192"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="219"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="250"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <translation>Attention: &apos;gsettings&apos; ne semble pas disponible! Êtes-vous sûr(e) que Gnome/Unity est installé?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="337"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <translation>Attention: &apos;xfconf-query&apos; ne semble pas disponible! Êtes-vous sûr(e) que XFCE4 est installé?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="350"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="505"/>
        <source>The wallpaper can be set to either of the available monitors (or any combination).</source>
        <translation>Le fond d&apos;écran peut être ajusté pour l&apos;un des écrans disponibles (ou toute combinaison).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="366"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="521"/>
        <source>Screen #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="483"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console &gt; Add-ons &gt; Modules &gt; System.</source>
        <translation>Attention: il semble que le module &apos;msgbus&apos; (DBUS) n&apos;est pas activé! Il peut être activé dans les réglages: console &gt; Extensions &gt; Modules &gt; Système</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="494"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <translation>Attention: &apos;enlightenment_remote&apos; ne semble pas disponible! Êtes-vous sûr(e) que Enlightenment est installé?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="556"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="576"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="578"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="579"/>
        <source>Workspace #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="636"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <translation>Attention: &apos;feh&apos; ne semble pas installé!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="647"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <translation>Attention: &apos;nitrogen&apos; ne semble pas installé!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="658"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <translation>Attention: ni &apos;feh&apos; ni &apos;nitrogen&apos; ne semblent installés!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="669"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.&lt;br&gt;This is intended particularly for window managers that don&apos;t natively support wallpapers (e.g., like Openbox).</source>
        <translation>PhotoQt peut utiliser &apos;feh&apos; ou &apos;nitrogen&apos; pour changer le fond d&apos;écran.&lt;br&gt; Ceci est particulièrement utile pour les gestionnaires de fenêtres qui ne supportent pas nativement les fonds d&apos;écran (e.g., comme Openbox).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="685"/>
        <source>Use &apos;feh&apos;</source>
        <extracomment>feh is an application, do not translate</extracomment>
        <translation>Utiliser &apos;feh&apos;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="692"/>
        <source>Use &apos;nitrogen&apos;</source>
        <extracomment>nitrogen is an application, do not translate</extracomment>
        <translation>Utiliser &apos;nitrogen&apos;</translation>
    </message>
</context>
</TS>
