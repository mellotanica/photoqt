<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/fadein/About.qml" line="113"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>PhotoQT è un semplice visualizzatore di immagini, creato per essere gradevole nell&apos;aspetto, molto configurabile e allo stesso tempo facile e veloce da utilizzare.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="115"/>
        <source>With PhotoQt I try to be different than other image viewers (after all, there are plenty of good image viewers already out there). Its interface is kept very simple, yet there is an abundance of settings to customize the look and feel to make PhotoQt YOUR image viewer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="117"/>
        <source>I&apos;m not a trained programmer. I&apos;m a simple Maths student that loves doing stuff like this. Most of my programming knowledge I taught myself over the past 10-ish years, and it has been developing a lot since I started PhotoQt. During my studies in university I learned a lot about the basics of programming that I was missing. And simply working on PhotoQt gave me a lot of invaluable experience. So the code of PhotoQt might in places not quite be done in the best of ways, but I think it&apos;s getting better and better with each release.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="121"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>Non dimenticare di visitare il sito ufficiale:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="123"/>
        <source>If you find a bug or if you have a question or suggestion, tell me. I&apos;m open to any feedback I get :)</source>
        <translation>Infine, se noti qualche errore o se hai qualche domanda o suggerimento, contattami! Sono aperto a ricevere qualsiasi tipo di feedback :)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="174"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don&apos;t forget to add the %1 in your translation!!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="119"/>
        <source>I heard a number of times people saying, that PhotoQt is a &apos;copy&apos; of Picasa&apos;s image viewer. Well, it&apos;s not. In fact, I myself have never used Picasa. I have seen it in use though by others, and I can&apos;t deny that it influenced the basic design idea a little. But I&apos;m not trying to do something &apos;like Picasa&apos;. I try to do my own thing, and to do it as good as I can.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="158"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>website:</source>
        <translation>sito internet:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>Licensed under GPLv2 or later, without any guarantee</source>
        <translation>Rilasciato sotto licenza GPLv2 o successiva, senza alcuna garanzia</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="52"/>
        <source>Move:</source>
        <extracomment>as in: &quot;Move file...&quot;</extracomment>
        <translation>Sposta:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="62"/>
        <source>Previous</source>
        <extracomment>Go to previous file</extracomment>
        <translation>Precedente</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="68"/>
        <source>Next</source>
        <extracomment>Go to next file</extracomment>
        <translation>Successiva</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="99"/>
        <source>Rotate:</source>
        <extracomment>As in: Rotate file</extracomment>
        <translation>Ruota:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="104"/>
        <source>Left</source>
        <extracomment>As in: rotate LEFT</extracomment>
        <translation>Sinistra</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="111"/>
        <source>Right</source>
        <extracomment>As in: Rotate RIGHT</extracomment>
        <translation>Destra</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="140"/>
        <source>Flip:</source>
        <extracomment>As in: Flip file</extracomment>
        <translation>Ribalta:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="145"/>
        <source>Horizontal</source>
        <extracomment>As in: Flip file HORIZONTALLY</extracomment>
        <translation>Orizzontale</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="152"/>
        <source>Vertical</source>
        <extracomment>As in: Flip file VERTICALLY</extracomment>
        <translation>Verticale</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="180"/>
        <source>Zoom:</source>
        <extracomment>Zoom file</extracomment>
        <translation>Zoom:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="185"/>
        <source>In</source>
        <extracomment>As in: Zoom IN</extracomment>
        <translation>Aumenta</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="191"/>
        <source>Out</source>
        <translation>Diminuisci</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="197"/>
        <source>Actual</source>
        <extracomment>As in: Zoom to ACTUAL size</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="203"/>
        <source>Reset</source>
        <extracomment>As in: Reset zoom</extracomment>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="224"/>
        <source>Scale Image</source>
        <translation type="unfinished">Scala immagine</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="234"/>
        <source>Open in default File Manager</source>
        <translation>Apri nel Gestore File di default</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="266"/>
        <source>Rename File</source>
        <translation>Rinomina file</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="276"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="301"/>
        <source>Copy File</source>
        <translation>Copia File</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="312"/>
        <source>Move File</source>
        <translation>Muovi File</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="11"/>
        <source>Confirm me?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="12"/>
        <source>Do you really want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="13"/>
        <source>Yes, do it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="14"/>
        <source>No, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="110"/>
        <source>Don&apos;t ask again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomDetectShortcut</name>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="72"/>
        <source>Detect key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="90"/>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="127"/>
        <source>Press keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="107"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>CustomExternalCommand</name>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="71"/>
        <source>External Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (with path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (without path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="122"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="135"/>
        <source>Save it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="159"/>
        <source>Select Executeable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomMouseShortcut</name>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="69"/>
        <source>Set Mouse Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="114"/>
        <source>Don&apos;t set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="127"/>
        <source>Set Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="68"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="98"/>
        <source>Do you really want to delete this file?</source>
        <translation>Vuoi veramente eliminare questo file?</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Move to Trash</source>
        <translation>Sposta nel Cestino</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="137"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="150"/>
        <source>Delete permanently</source>
        <translation>Elimina permanentemente</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <location filename="../qml/mainview/Display.qml" line="481"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="507"/>
        <source>Open a file to begin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="558"/>
        <source>No results found...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="565"/>
        <source>Rotate Image?</source>
        <translation>Ruotare Immagine?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>The Exif data of this image says, that this image is supposed to be rotated.</source>
        <translation>I dati Exif di questa immagine dicono che questa dovrebbe essere ruotata.</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>Do you want to apply the rotation?</source>
        <translation>Vuoi applicare la rotazione?</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="567"/>
        <source>Yes, do it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="568"/>
        <source>No, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="68"/>
        <source>Filter images in current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="83"/>
        <source>Enter here the term you want to search for. Seperate multiple terms by a space.</source>
        <translation>Digita qui quello che vuoi cercare. Separa le parole con uno spazio.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="90"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>Se vuoi filtrare in base all&apos;estensione, metti un punto davanti a questa.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="137"/>
        <source>Filter</source>
        <translation>Filtra</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="145"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="160"/>
        <source>Remove Filter</source>
        <translation>Rimuovi Filtro</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="10"/>
        <source>Edit with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="13"/>
        <source>Open in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="317"/>
        <source>Unknown</source>
        <translation>Sconosciuta</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="320"/>
        <source>Daylight</source>
        <translation>Luce diurna</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="323"/>
        <source>Fluorescent</source>
        <translation>Fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="326"/>
        <source>Tungsten (incandescent light)</source>
        <translation>Tungsteno (luce incandescente)</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="329"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="332"/>
        <source>Fine weather</source>
        <translation>Bel tempo</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="335"/>
        <source>Cloudy Weather</source>
        <translation>Nuvoloso</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="338"/>
        <source>Shade</source>
        <translation>Ombra</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="341"/>
        <source>Daylight fluorescent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="344"/>
        <source>Day white fluorescent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="347"/>
        <source>Cool white fluorescent</source>
        <translation>Fluorescente bianco freddo</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="350"/>
        <source>White fluorescent</source>
        <translation>Bianco fluorescente</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="353"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="356"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="359"/>
        <source>Standard light</source>
        <translation>Luce standard</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="371"/>
        <source>D50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="374"/>
        <source>ISO studio tungsten</source>
        <translation>ISO studio tungsteno</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="377"/>
        <source>Other light source</source>
        <translation>Altra sorgente di luce</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="380"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="389"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired</extracomment>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn&apos;t fired</extracomment>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="393"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash</extracomment>
        <translation>No flash</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="395"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luce di ritorno stroboscopica non rilevata</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luce di ritorno stroboscopica rilevata</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="399"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>modalità flash obbligatoria</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="401"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>modalità automatica</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>riduzione occhi rossi</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="405"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luce di ritorno rilevata</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="407"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>luce di ritorno non rilevata</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="454"/>
        <source>Invalid flash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="467"/>
        <source>Landscape</source>
        <translation>Paesaggio</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>Portrait</source>
        <translation>Ritratto</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="473"/>
        <source>Night Scene</source>
        <translation>Notturno</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="25"/>
        <source>Open File</source>
        <translation>Apri File</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="26"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="27"/>
        <source>Set as Wallpaper</source>
        <translation>Imposta come Sfondo</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="28"/>
        <source>Start Slideshow</source>
        <translation>Fai partire una presentazione</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="29"/>
        <source>Filter Images in Folder</source>
        <translation>Filtra immagini della cartella</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="30"/>
        <source>Show/Hide Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="31"/>
        <source>About PhotoQt</source>
        <translation>About PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="32"/>
        <source>Hide (System Tray)</source>
        <translation>Nascondi (Barra di Sistema)</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="33"/>
        <source>Quit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="120"/>
        <source>Quickstart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="47"/>
        <source>Open image file</source>
        <translation>Apri un&apos;immagine</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="121"/>
        <location filename="../cplusplus/mainwindow.cpp" line="122"/>
        <location filename="../cplusplus/mainwindow.cpp" line="124"/>
        <source>Images</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="126"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="537"/>
        <source>Image Viewer</source>
        <translation>Visualizzatore Immagini</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="542"/>
        <source>Hide/Show PhotoQt</source>
        <translation>Nascondi/Mostra PhotoQt</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="44"/>
        <source>No File Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="62"/>
        <source>File Format Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="80"/>
        <source>Invalid File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="121"/>
        <source>Keep Open</source>
        <translation>Tieni aperto</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="191"/>
        <source>Filesize</source>
        <translation>Dimensione file</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="193"/>
        <location filename="../qml/slidein/MetaData.qml" line="196"/>
        <source>Dimensions</source>
        <translation>Dimensioni</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="201"/>
        <source>Make</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="202"/>
        <source>Model</source>
        <translation>Modello</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="203"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="205"/>
        <source>Time Photo was Taken</source>
        <translation>Data dello scatto</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="206"/>
        <source>Exposure Time</source>
        <translation>Tempo di Esposizione</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="207"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="208"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="209"/>
        <source>Scene Type</source>
        <translation>Tipo di Scena</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="210"/>
        <source>Focal Length</source>
        <translation>Lunghezza Focale</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="211"/>
        <source>F Number</source>
        <translation>Rapporto Focale</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="212"/>
        <source>Light Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="214"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="215"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="216"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="218"/>
        <source>GPS Position</source>
        <translation>Posizione GPS</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="116"/>
        <source>Hide Counter</source>
        <translation>Nascondi contatore</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="183"/>
        <source>Hide Filepath, leave Filename</source>
        <translation>Nascondi il percorso del file, lascia il nome</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="191"/>
        <source>Hide both, Filename and Filepath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="236"/>
        <source>Filter:</source>
        <extracomment>As in: FILTER images</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickSettings</name>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="46"/>
        <source>Quick Settings</source>
        <translation>Impostazioni rapide</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="55"/>
        <source>Change settings with one click. They are saved and applied immediately. If you&apos;re unsure what a setting does, check the full settings for descriptions.</source>
        <translation>Cambia le impostazioni con un click. Esse vengono salvate e applicate immediatamente. Se non sei sicuro su cosa fare, vai nelle impostazioni avanzate e controlla la descrizione.</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="77"/>
        <source>Sort by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Natural Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="161"/>
        <source>Loop through folder</source>
        <translation>Loop della cartella</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="182"/>
        <source>Window mode</source>
        <translation>Modalità finestra</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="192"/>
        <source>Show window decoration</source>
        <translation>Mostra decorazione finestra</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="214"/>
        <source>Close on click on background</source>
        <translation>Chiudi se viene fatto click sullo sfondo</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="235"/>
        <source>Keep thumbnails visible</source>
        <translation>Mantieni visibili le miniature</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Normal thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Dynamic thumbnails</source>
        <translation>Miniature dinamiche</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="280"/>
        <source>Enable &apos;Quick Settings&apos;</source>
        <translation>Abilita &apos;Impostazioni rapide&apos;</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Smart thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>No tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Hide to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="305"/>
        <source>Show full settings</source>
        <translation>Mosta impostazioni avanzate</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="68"/>
        <source>Rename File</source>
        <translation>Rinomina file</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="148"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="67"/>
        <source>Scale Image</source>
        <translation>Scala immagine</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="83"/>
        <source>Current Size:</source>
        <translation>Dimensioni attuali:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="112"/>
        <source>Error! Something went wrong, unable to save new dimension...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="136"/>
        <source>New width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="143"/>
        <source>New height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="248"/>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="298"/>
        <source>Scale into new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="315"/>
        <source>Don&apos;t scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="217"/>
        <source>Aspect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="284"/>
        <source>Scale in place</source>
        <translation>Scala su questo file</translation>
    </message>
</context>
<context>
    <name>SettingsItem</name>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="59"/>
        <source>Look and Feel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="72"/>
        <location filename="../qml/settings/SettingsItem.qml" line="124"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="93"/>
        <location filename="../qml/settings/SettingsItem.qml" line="143"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="115"/>
        <source>Thumbnails</source>
        <translation>Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="174"/>
        <source>Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="194"/>
        <source>Other Settings</source>
        <translation>Altre Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="219"/>
        <source>Filetypes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="240"/>
        <source>Shortcuts</source>
        <translation>Scorciatoie</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="313"/>
        <source>Restore Default Settings</source>
        <translation>Ripristina le impostazioni predefinite</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="326"/>
        <source>Exit and Discard Changes</source>
        <translation>Esci e annulla le modifiche</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="344"/>
        <source>Save Changes and Exit</source>
        <translation>Salva le modifiche e esci</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="358"/>
        <source>Clean Database</source>
        <translation>Pulisci il Database</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>Do you really want to clean up the database?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This process might take a little while.</source>
        <translation>Questo processo potrebbe richiedere un po&apos; di tempo.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="360"/>
        <source>Yes, clean is good</source>
        <translation>Sì, pulire è buona cosa</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="361"/>
        <source>No, don&apos;t have time for that</source>
        <translation>No, non ho tempo per questo</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="368"/>
        <source>Erase Database</source>
        <translation>Cancella il Database</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>Do you really want to ERASE the entire database?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This removes every single item in the database! This step should never really be necessarily. After that, every thumbnail has to be newly re-created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This step cannot be reversed!</source>
        <translation>Questa operazione non può essere annullata!</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="370"/>
        <source>Yes, get rid of it all</source>
        <translation>Sì, sbarazzati di tutto</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="371"/>
        <source>Nooo, I want to keep it</source>
        <translation>Nooo, voglio tenerlo così com&apos;è</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="378"/>
        <source>Set Default Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="379"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="380"/>
        <source>Yes, please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="381"/>
        <source>Nah, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Slideshow</name>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="86"/>
        <source>Start a Slideshow</source>
        <translation>Fai partire una presentazione</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="97"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that is played in the background.</source>
        <translation>Qui ci sono alcune impostazioni riguardanti le presentazioni, ad esempio il tempo che intercorre tra ogni immagine, la durata della transizione e la scelta del file che può essere usato come musica di sottofondo.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="104"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>Una volta impostate le opzioni desiderate, potrai far partire una presentazione tramite &quot;Quickstart&quot;, saltando questa finestra.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="114"/>
        <source>Time in between</source>
        <translation>Tempo di visualizzazione</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="121"/>
        <source>Adjust the time between the images. The time specified here is the amount of time the image will be completely visible, i.e. the transitioning (if set) is not part of this time.</source>
        <translation>Regola l&apos;intervallo di tempo in cui l&apos;immagine sarà completamente visibile. Il valore specificato non comprende la durata della transizione (se impostata).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="161"/>
        <source>Smooth Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="168"/>
        <source>Here you can set, if you want the images to fade into each other, and how fast they are to do that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="182"/>
        <source>No Transition</source>
        <translation>Nessuna transizione</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="198"/>
        <source>Long Transition</source>
        <translation>Transizione lunga</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="210"/>
        <source>Shuffle and Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="217"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note, that no image will be shown twice before every image has been shown once.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="223"/>
        <source>Loop over images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="229"/>
        <source>Shuffle images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="241"/>
        <source>Hide Quickinfo</source>
        <translation>Nascondi la Quickinfo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="249"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>A seconda delle impostazioni, PhotoQt mostra alcune informazioni sul margine superiore, come la posizione nella cartella corrente o il percorso/nome file. Qui puoi disattivarle temporaneamente per la presentazione.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="254"/>
        <source>Hide Quickinfos</source>
        <translation>Nascondi le Quickinfo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="266"/>
        <source>Background Music</source>
        <translation>Musica di sottofondo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="273"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>Ad alcuni potrebbe piacere ascoltare un po&apos; di musica mentre la presentazione va avanti. Qui puoi selezionare un file da riprodurre in sottofondo.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="280"/>
        <source>Enable Music</source>
        <translation>Attiva Musica</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="310"/>
        <source>Click here to select music file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="344"/>
        <source>Okay, lets start</source>
        <translation>Okay, partiamo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="348"/>
        <source>Wait, maybe later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="352"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Select music file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Music Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Play Slideshow</source>
        <translation>Continua presentazione</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Pause Slideshow</source>
        <translation>Metti in pausa la presentazione</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="49"/>
        <source>Music Volume:</source>
        <translation>Volume musica:</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="77"/>
        <source>Exit Slideshow</source>
        <translation>Esci dalla presentazione</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully installed</source>
        <translation>PhotoQt è stato installato con successo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully updated</source>
        <translation>PhotoQt è stato aggiornato con successo</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>Welcome to PhotoQt. PhotoQt is an image viewer, aimed at being fast and reliable, highly customisable and good looking.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>This app started out more than three and a half years ago, and it has developed quite a bit since then. It has become very efficient, reliable, and highly flexible (check out the settings). I&apos;m convinced it can hold up to the more &apos;traditional&apos; image viewers out there in every way.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Welcome back to PhotoQt. It hasn&apos;t been that long since the last release of PhotoQt. Yet, it changed pretty much entirely, as it now is based on QtQuick rather than QWidgets. A large quantity of the code had to be re-written, while some chunks could be re-used. Thus, it is now more reliable than ever before and overall simply feels well rounded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>Many File Formats</source>
        <translation>Molti tipi di File</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>PhotoQt can make use of GraphicsMagick, an image library, to display many different image formats. Currently, there are up to 72 different file formats supported (exact number depends on your system)! You can find a list of it in the settings (Tab &apos;Other&apos;). There you can en-/disable different ones and also add custom file endings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>Make PhotoQt your own</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>PhotoQt has an extensive settings area. By default you can call it with the shortcut &apos;e&apos; or through the dropdown menu at the top edge towards the top right corner. You can adjust almost everything in PhotoQt, and it&apos;s certainly worth having a look there. Each setting usually comes with a little explanation text. Some of the most often used settings can also be conveniently adjusted in a slide-in widget, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Most images store some additional information within the file&apos;s metadata. PhotoQt can read and display a selection of this data. You can find this information in the slide-in window hidden behind the left edge of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>PhotoQt also brings a slideshow feature. When you start a slideshow, it starts at the currently displayed image. There are a couple of settings that can be set, like transition, speed, loop, and shuffle. Plus, you can set a music file that is played in the background. When the slideshow takes longer than the music file, then PhotoQt starts the music file all over from the beginning. At anytime during the slideshow, you can move the mouse cursor to the top edge of the screen to get a little bar, where you can pause/exit the slideshow and adjust the music volume.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>Thumbnails</source>
        <translation>Miniature</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Here below you find a short overview of a selection of a few things PhotoQt has to offer, but feel free to skip it and just get started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>What would be an image viewer without thumbnails support? It would only be half as good. Whenever you load an image, PhotoQt loads the other images in the directory in the background (by default, it tries to be smart about it and only loads the ones that are needed). It lines them up in a row at the bottom edge (move your mouse there to see them). There are many settings just for the thumbnails, like, e.g., size, liftup, en-/disabled, type, filename, permanently shown/hidden, etc. PhotoQt&apos;s quite flexible with that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>Shortcuts</source>
        <translation>Scorciatoie</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>One of the many strengths of PhotoQt is the ability to easily set a shortcut for almost anything. Even mouse shortcuts are possible! You can choose from a huge number of internal functions, or you can run any custom script or command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Image Information (Exif/IPTC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>Slideshow</source>
        <translation>Presentazione</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>Localisation</source>
        <translation>Localizzazione</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>PhotoQt comes with a number of translations. Many have taken some of their time to create/update one of them (Thank you!). Not all of them are complete... do you want to help?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="355"/>
        <source>There are many many more features. Best is, you just give it a go. Don&apos;t forget to check out the settings to make PhotoQt YOUR image viewer. Enjoy :-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="387"/>
        <source>Okay, I got enough now. Lets start!</source>
        <translation>Okay, ne ho abbastanza. Cominciamo!</translation>
    </message>
</context>
<context>
    <name>TabDetails</name>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="55"/>
        <source>Image Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="74"/>
        <source>PhotoQt can display different information of and about each image. The widget for this information is on the left outside the screen and slides in when mouse gets close to it and/or when the set shortcut (default Ctrl+E) is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <translation>Photo può mostrare le informazioni meta (exif) delle immagini. Il widget per queste informazioni è situato nella parte sinistra dello schermo e comparirà quando il mouse si avvicinerà ad esso o tramite la scorciatoia da tastiera (di default è Ctrl+E). Su richiesta, la comparsa tramite il mouse può essere disabilitata selezionando la casella sottostante.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Trigger Widget on Mouse Hovering</source>
        <translation>Mostra elemento al passaggio del mouse</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut. This can come in handy, if you get annoyed by accidentally opening the widget occasionally.</source>
        <translation>Di default il pannello informazioni può essere mostrato in due modi: muovendo il mouse sul bordo sinistro dello schermo per farlo comparire temporaneamente (fin quando il cursore rimane sul bordo), o fissandolo cliccando sulla casella (lo stato di questa rimane tale solo durante la sessione, non può essere salvato permanentemente!). In alternativa il pannello può essere attivato tramite shortcut. Su richiesta l&apos;attivazione da mouse puà essere disattivata, in modo che potrà essere reso visibile solo tramite shortcut. Questo può essere utile se ti dà fastidio che a volte si possa aprire accidentalmente.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="118"/>
        <source>Turn mouse triggering OFF</source>
        <translation>Disattiva la visualizzazione al passaggio del mouse</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>Which items are shown?</source>
        <translation>Quali elementi vengono mostrati?</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>PhotoQt can display a number of information about the image (often called &apos;Exif data&apos;&apos;). However, you might not be interested in all of them, hence you can choose to disable some of them here.</source>
        <translation>PhotoQt può mostrare informazioni riguardo l&apos;immagine (di solito chiamate dati Exif). Potresti non essere interessato ad alcuni di essi, quindi da qui puoi disabilitare quelli che non vuoi che vengano mostrati.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="149"/>
        <source>Enable ALL</source>
        <translation>Attiva Tutti</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="155"/>
        <source>Disable ALL</source>
        <translation>Disattiva Tutti</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="179"/>
        <source>Filesize</source>
        <translation>Dimensione file</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="180"/>
        <source>Dimensions</source>
        <translation>Dimensioni</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="181"/>
        <source>Make</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="182"/>
        <source>Model</source>
        <translation>Modello</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="183"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="184"/>
        <source>Time Photo was Taken</source>
        <translation>Data dello scatto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="185"/>
        <source>Exposure Time</source>
        <translation>Tempo di Esposizione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="186"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="187"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="188"/>
        <source>Scene Type</source>
        <translation>Tipo di Scena</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="189"/>
        <source>Focal Length</source>
        <translation>Lunghezza Focale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="190"/>
        <source>F-Number</source>
        <translation>Rapporto Focale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="191"/>
        <source>Light Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="192"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="193"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="194"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="195"/>
        <source>GPS Position</source>
        <translation>Posizione GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Adjusting Font Size</source>
        <translation>Regolazione Dimensioni Font</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Computers can have very different resolutions. On some of them, it might be nice to increase the font size of the labels to have them easier readable. Often, a size of 8 or 9 should be working quite well...</source>
        <translation>I computer possono avere risoluzioni molto diverse. Su alcuni potrebbe essere meglio aumentare le dimensioni delle etichette in modo che risultino meglio leggibili. Di solito, dimensioni di 8 o 9 dovrebbero andar bene...</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Rotating/Flipping Image according to Exif Data</source>
        <translation>Ruota/RIbalta l&apos;immagine in base ai dati Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly. When asking PhotoQt to always rotate images automatically without asking, it already does so at image load (including thumbnails).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="294"/>
        <source>Never rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="300"/>
        <source>Always rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="305"/>
        <source>Always ask</source>
        <translation>Chiedi sempre</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>Online map for GPS</source>
        <translation>Mappe online per il GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>If you&apos;re image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>Se le tue immagini includono una locazione GPS, un clic su tale locazione aprirà la mappa nel tuo browser di default. Qui puoi scegliere quale mappa online vuoi utilizzare (i suggerimenti per altri servizi di mappe online sono sempre graditi).</translation>
    </message>
</context>
<context>
    <name>TabFiletypes</name>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="55"/>
        <source>Filetypes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>File Types - Qt</source>
        <translation>Formati File - Qt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.&lt;br&gt;If a file ending for one of the formats is missing, you can add it below, formatted like &apos;*.ending&apos; (without single quotation marks), multiple entries seperated by commas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="102"/>
        <source>Extra File Types:</source>
        <translation>Formati file aggiuntivi:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="117"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="166"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="214"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="337"/>
        <source>Mark None</source>
        <translation>Non contrassegnare niente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="123"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="172"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="220"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="343"/>
        <source>Mark All</source>
        <translation>Contrassegna Tutti</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>File Types - GraphicsMagick</source>
        <translation>Formati File - GraphicsMagick</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. If you prefer not to have one or the other enabled in PhotoQt, you can simply disable individual formats below.&lt;br&gt;There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>File Types - GraphicsMagick (requires Ghostscript)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and work. However, they require Ghostscript to be installed on the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>File Types - Other tools required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>If an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <source>Gimp&apos;s XCF file format.</source>
        <extracomment>&apos;Makes use of&apos; is in connection with an external tool (i.e., it &apos;makes use of&apos; tool abc)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Makes use of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Adobe Photoshop PSD and PSB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>File Types - GraphicsMagick (Untested)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>The following file types are generally supported by GraphicsMagick, but I wasn&apos;t able to test them in PhotoQt (due to lack of test images). They might very well be working, but I simply can&apos;t say. If you decide to enable some of the, the worst that could happen ist, that you see an error image instead of the actual image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>If you happen to have an image in one of those formats and don&apos;t mind sending it to me, that&apos;d be really cool...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelAdvanced</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="54"/>
        <source>Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>Background of PhotoQt</source>
        <translation>Sfondo di PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above.&lt;br&gt;Note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="91"/>
        <source>Use (half-)transparent background</source>
        <translation>Usa sfondo semi-trasparente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="97"/>
        <source>Use faked transparency</source>
        <translation>Usa falsa trasparenza</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="102"/>
        <source>Use custom background image</source>
        <translation>Usa un&apos;immagine di sfondo personalizzata</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="107"/>
        <source>Use one-coloured, non-transparent background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="164"/>
        <source>No image selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="188"/>
        <source>Scale to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="194"/>
        <source>Scale and Crop to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="199"/>
        <source>Stretch to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="204"/>
        <source>Center image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="209"/>
        <source>Tile image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Background/Overlay Color</source>
        <translation>Colore di Sfondo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>Qui puoi scegliere il colore di sfondo di PhotoQt (della parte non occupata dall&apos;immagine). Se usi il compositing o un&apos;immagine di sfondo fissa, puoi anche specificare un valore alpha, ovvero il livello di trasparenza del livello colorato. Se non vengono usati né il compositing né un&apos;immagine di sfondo, il colore selezionato sarà usato come sfondo non trasparente.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="259"/>
        <source>Red:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="281"/>
        <source>Green:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="303"/>
        <source>Blue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="325"/>
        <source>Alpha:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="387"/>
        <source>Preview colour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Border Around Image</source>
        <translation>Bordo intorno all&apos;immagine</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels (looks better). Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>Ogni volta che un&apos; immagine viene caricata, essa non viene visualizzata sull&apos;intero schermo, o meglio, non occupa lo schermo da bordo a bordo. Vi è invece un piccolo margine di alcuni pixel attorno all&apos;immagine (per motivi estetici). Qua puoi regolare lo spessore di questo margine ( 0 per disabilitarlo).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Close on Click in empty area</source>
        <translation>Chiudi al click su un&apos;area vuota</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. It can be a nice feature, PhotoQt will feel even more like a &quot;floating layer&quot;. However, you might at times close PhotoQt accidentally.</source>
        <translation>Questa opzione rende PhotoQt simile ai visualizzatori di immagini Javascript che si trovano su molti siti web. Un click al di fuori dell&apos;immagine, sullo sfondo vuoto, chiuderà l&apos;applicazione. Può essere una funzione interessante, PhotoQt sarà ancora più simile ad un &quot;livello fluttuante&quot;. Tuttavia, potrebbe capitare che l&apos;utente chiuda PhotoQt accidentalmente.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Note: If you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>Nota: Se usi già il click del mouse come shortcut, questa opzione non avrà nessun effetto!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="475"/>
        <source>Close on click in empty area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>Looping Through Folder</source>
        <translation>Visualizzazione ricorsiva all&apos;interno delle cartelle</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>When you load the last image in a directory and select &apos;Next&apos;, PhotoQt automatically jumps to the first image (and vice versa: if you select &apos;Previous&apos; while having the first image loaded, PhotoQt jumps to the last image). Disabling this option makes PhotoQt stop at the first/last image (i.e. selecting &apos;Next&apos;/&apos;Previous&apos; will have no effect in these two special cases).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="494"/>
        <source>Loop through folder</source>
        <translation>Loop della cartella</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Smooth Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced by the new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="526"/>
        <source>No Transition</source>
        <translation>Nessuna transizione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="546"/>
        <source>Long Transition</source>
        <translation>Transizione lunga</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Menu Sensitivity</source>
        <translation>Sensibilità menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Here you can adjust the sensitivity of the drop-down menu. The menu opens when your mouse cursor gets close to the right side of the upper edge. Here you can adjust how close you need to get for it to open.</source>
        <translation>Qui puoi regolare la sensibilità del menu a discesa. Il menu si apre quando il cursore si avvicina alla parte destra del bordo superiore dello schermo. Qui puoi impostare quanto devi avvicinarti perché esso si apra.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="583"/>
        <source>Low Sensitivity</source>
        <translation>Bassa sensibilità</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="603"/>
        <source>High Sensitivity</source>
        <translation>Alta sensibilità</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Here you can adjust the sensitivity of the mouse wheel. For example, if you have set the mouse wheel up/down for switching back and forth between images, then a lower sensitivity means that you will have to scroll further for triggering a shortcut. Per default it is set to the highest sensitivity, i.e. every single wheel movement is evaluated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="640"/>
        <source>Very sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="660"/>
        <source>Not at all sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>Remember per session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>If you would like PhotoQt to remember the rotation/flipping and/or zoom level per session (not permanent), then you can enable it here. If not set, then every time a new image is displayed, it is displayed neither zoomed nor rotated nor flipped (one could say, it is displayed &apos;normal&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="697"/>
        <source>Remember Rotation/Flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="702"/>
        <source>Remember Zoom Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation and Window Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="748"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation of fade-in widgets (like, e.g., Settings or About Widget)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="738"/>
        <source>Animate all fade-in elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="743"/>
        <source>Save and restore window geometry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelBasic</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Sort Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Here you can adjust, how the images in a folder are supposed to be sorted. You can sort them by Filename, Natural Name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), File Size, and Date. Also, you can reverse the sorting order from ascending to descending if wanted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Hint: You can also change this setting very quickly from the &apos;Quick Settings&apos; window, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="90"/>
        <source>Sort by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Natural Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Filesize</source>
        <translation>Dimensione file</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="105"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="113"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>Window Mode</source>
        <translation>Modalità Finestra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>PhotoQt is designed with the space of a fullscreen app in mind. That&apos;s why it by default runs as fullscreen. However, some might prefer to have it as a normal window, e.g. so that they can see the panel.</source>
        <translation>PhotoQt è progettato per lo spazio di uno schermo intero. È per questo che di default usa lo schermo intero. Comunque alcuni potrebbero preferirlo in una normale finestra, in modo che per esempio possano vedere e usare il pannello.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="151"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>Avvia PhotoQt in Modalità Finestra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="159"/>
        <source>Show Window Decoration</source>
        <translation>Mostra Decorazione Finestra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>Hide to Tray Icon</source>
        <translation>Riduci nella barra di sistema</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>When started PhotoQt creates a tray icon in the system tray. If desired, you can set PhotoQt to minimise to the tray instead of quitting. This causes PhotoQt to be almost instantaneously available when an image is opened.&lt;br&gt;It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>No tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Hide to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>Closing &apos;X&apos; (top right)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>There are two looks for the closing &apos;x&apos; at the top right: a normal &apos;x&apos;, or a slightly more fancy &apos;x&apos;. Here you can switch back and forth between both of them, and also change their size. If you prefer not to have a closing &apos;x&apos; at all, see below for an option to hide it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="216"/>
        <source>Normal Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="223"/>
        <source>Fancy Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="249"/>
        <source>Small Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="264"/>
        <source>Large Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>Fit Image in Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can zoom those images to make them fir into the window. However, keep in mind, that such images will look pixelated to a certain degree (depending on each image).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="287"/>
        <source>Fit Images in Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Hide Quickinfo (Text Labels)</source>
        <translation>Nascondi le etichette di testo (Quickinfo)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Here you can hide the text labels shown in the main area: The Counter in the top left corner, the file path/name following the counter, and the &quot;X&quot; displayed in the top right corner. The labels can also be hidden by simply right-clicking on them and selecting &quot;Hide&quot;.</source>
        <translation>Qui puoi nascondere le etichette di testo mostrate nell&apos;area principale: il contatore nella parte alta-sinistra, il nome del file che segue il contatore e la &quot;X&quot; mostrata nella parte alta-destra. Le etichette possono anche essere nascoste semplicemente cliccando col tasto destro del mouse e selezionando &quot;Nascondi&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="321"/>
        <source>Hide Counter</source>
        <translation>Nascondi contatore</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="326"/>
        <source>Hide Filepath (Shows only file name)</source>
        <translation>Nascondi il percorso del file (mostra solo il nome)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="331"/>
        <source>Hide Filename (Including file path)</source>
        <translation>Nascondi il nome del file (incluso il percorso)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="336"/>
        <source>Hide &quot;X&quot; (Closing)</source>
        <translation>Nascondi la &quot;X&quot; (chiusura)</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="55"/>
        <source>Other Settings</source>
        <translation>Altre Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>Choose Language</source>
        <translation>Scegli la Lingua</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>Sono disponibili diverse lingue per PhotoQt. Grazie a tutti quelli che hanno usato il loro tempo per tradurlo!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>Quick Settings</source>
        <translation>Impostazioni rapide</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>The &apos;Quick Settings&apos; is a widget hidden on the right side of the screen. When you move the cursor there, it shows up, and you can adjust a few simple settings on the spot without having to go through this settings dialog. Of course, only a small subset of settings is available (the ones needed most often). Here you can disable the dialog so that it doesn&apos;t show on mouse movement anymore.</source>
        <translation>&apos;Impostazioni rapide&apos; è un widget nascosto sul lato destro dello schermo. Viene mostrato portando sopra il cursore, e da lì puoi accedere ad alcune impostazioni senza dover andare al pannello impostazioni. Solo le impostazioni usate più spesso sono disponibili. Da qui puoi disattivare il pannello in modo che non venga più mostrato.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="129"/>
        <source>Show &apos;Quick Settings&apos; on mouse hovering</source>
        <translation>Mostra &apos;Impostazioni rapide&apos; al passaggio del mouse</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Adjust Context Menu</source>
        <translation>Regola Menu Contestuale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Here you can adjust the context menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>Qui puoi modificare il menu contestuale. Puoi semplicemente trascinare le voci, aggiungerle, modificarle o rimuoverle.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="183"/>
        <source>Executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="199"/>
        <source>Menu Text</source>
        <translation>Testo Menu</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="223"/>
        <source>Add new context menu entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="231"/>
        <source>(Re-)set automatically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabOtherContext</name>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="116"/>
        <source>Click here to drag</source>
        <translation>Clicca qui per spostare</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="186"/>
        <source>quit</source>
        <translation>esci</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="54"/>
        <source>Shortcuts</source>
        <translation>Scorciatoie</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="73"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available function you can either double click on the tile or click the &quot;+&quot; button. This automatically opens another widget where you can set a key combination.</source>
        <translation>Qui puoi modificare le scorciatoie da tastiera, aggiungerne nuove, rimuoverle o cambiare la combinazione di tasti. Le shortcuts sono raggruppate in 4 differenti categorie per i comandi interni più una per i comandi esterni. I riquadri a destra contengono tutti i possibili comandi. Per aggiungere una shortcut puoi fare doppio click su un elemento oppure cliccare sul pulsante &quot;+&quot;. Questo apre una finestra dove puoi impostare una combinazione di tasti.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="89"/>
        <source>Set default shortcuts</source>
        <translation>Ripristina le scorciatoie di default</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="96"/>
        <source>Navigation</source>
        <translation>Navigazione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Open New File</source>
        <translation>Apri un nuovo file</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Filter Images in Folder</source>
        <translation>Filtra immagini della cartella</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Next Image</source>
        <translation>Immagine successiva</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Previous Image</source>
        <translation>Immagine precedente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to first Image</source>
        <translation>Vai alla prima immagine</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to last Image</source>
        <translation>Vai all&apos;ultima immagine</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Hide to System Tray</source>
        <translation>Nascondi nella Barra di Sistema</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Quit PhotoQt</source>
        <translation>Esci da PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="104"/>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom In</source>
        <translation>Ingrandisci</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom Out</source>
        <translation>Rimpicciolisci</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom to Actual Size</source>
        <translation>Zoom alla dimensione reale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Zoom</source>
        <translation>Reimposta Zoom</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Right</source>
        <translation>Ruota a destra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Left</source>
        <translation>Ruota a sinistra</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Rotation</source>
        <translation>Ripristina rotazione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Horizontally</source>
        <translation>Capovolgi orizzontalmente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Vertically</source>
        <translation>Capovolgi verticalmente</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Scale Image</source>
        <translation type="unfinished">Scala immagine</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="112"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Rename File</source>
        <translation>Rinomina file</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Copy File to a New Location</source>
        <translation>Copia file in una nuova posizione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Move File to a New Location</source>
        <translation>Muovi file in una nuova posizione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="120"/>
        <source>Other</source>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Interrupt Thumbnail Creation</source>
        <translation>Interrompi la creazione di miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Reload Thumbnails</source>
        <translation>Ricarica Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Hide/Show Exif Info</source>
        <translation>Nascondi/Mostra informazioni Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Context Menu</source>
        <translation>Mostra Menu contestuale</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Settings</source>
        <translation>Mostra Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow</source>
        <translation>Fai partire una presentazione</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Fai partire una presentazione (Quickstart)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>About PhotoQt</source>
        <translation>About PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Set as Wallpaper</source>
        <translation>Imposta come Sfondo</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="128"/>
        <source>Extern</source>
        <translation>Esterno</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="132"/>
        <source>EXTERN</source>
        <extracomment>Is the shortcut tile text for EXTERNal shortcuts</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabShortcutsCategories</name>
    <message>
        <location filename="../qml/settings/TabShortcutsCategories.qml" line="32"/>
        <source>Category:</source>
        <translation>Categoria:</translation>
    </message>
</context>
<context>
    <name>TabShortcutsTilesAvail</name>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="98"/>
        <source>key</source>
        <extracomment>tile text for KEY shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>tasto</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="154"/>
        <source>mouse</source>
        <extracomment>tile text for MOUSE shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabThumbnailsAdvanced</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="52"/>
        <source>Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Change Thumbnail Position</source>
        <translation>Cambia la posizione delle miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Per default the bar with the thumbnails is shown at the lower edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge, so that&apos;s what can be changed here.</source>
        <translation>Di default la barra delle miniature è posizionata in basso, ma qualcuno potrebbe trovare più bello o comodo averla in alto, e qui puoi decidere dove averla.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="86"/>
        <source>Show at lower edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="93"/>
        <source>Show at upper edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>Filename? Dimension? Or both?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="132"/>
        <source>Write Filename</source>
        <translation>Mostra Nome File</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>Use file-name-only Thumbnails</source>
        <translation>Usa Miniature solo con il Nome File</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a file-name-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>Se non vuoi che PhotoQt carichi sempre in background le immagini per le miniature, ma vuoi comunque avere un modo per muoverti all&apos;interno delle cartelle, puoi impostare le miniature con i soli nomi dei file, senza che vengano create immagini per le miniature. Puoi anche regolare le dimensioni del testo.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>When thumbnails are displayed at the top/bottom, PhotoQt usually writes the filename on them (if not disabled). You can also use the slider below to adjust the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="213"/>
        <source>Use filename-only thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>Disable Thumbnails</source>
        <translation>Disabilita Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This option can also be toggled remotely via command line (run &apos;photoqt --help&apos; for more information on that). This might increase the speed of PhotoQt a good bit, however, navigating through a folder might be a little harder without thumbnails.</source>
        <translation>Se non hai bisogno o non vuoi nessuna miniatura, qui puoi disabilitarle completamente. Questa opzione può essere anche attivata remotamente tramite linea di comando (esegui &apos;photoqt --help&apos; per ulteriori informazioni). Questo potrebbe migliorare visibilimente la reattività di PhotoQt, tuttavia navigare nelle cartelle risulterà molto più scomodo.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="287"/>
        <source>Disable Thumbnails altogether</source>
        <translation>Disabilita completamente Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnail Cache</source>
        <translation>Cache Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnails can be cached in two different ways:&lt;br&gt;1) File Caching (following the freedesktop.org standard) or&lt;br&gt;2) Database Caching (better performance and management, default option).</source>
        <translation>Le Miniature possono essere memorizzate in due modi differenti:&lt;br&gt;1) Caching su file (in accordo con lo standard di freedesktop.org) o&lt;br&gt;2)Caching in database (migliori performance e gestione, opzione predefinita).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <translation>Entrambi i metodi hanno vantaggi e svantaggi:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file. However, it&apos;s not possible to check for obsolete thumbnails (thus this may lead to many unneeded thumbnail files).</source>
        <translation>Il caching su file viene usato in accordo agli standard di freedesktop.org e quindi diverse applicazioni possono condividere la miniatura per la stessa immagine. Tuttavia, non è possibile fare un controllo per miniature obsolete (e questo potrebbe causare la presenza di molte miniature inutili).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it brings a slightly better performance, and it allows a better handling of existing thumbnails (e.g. deleting obsolete thumbnails).</source>
        <translation>Il caching su database non ha il vantaggio di mettere a disposizione le miniature ad altri programmi (e viceversa le miniature create da altri programmi devono essere ricreate da PhotoQt), ma offre maggiori prestazioni e permette una migliore gestione delle miniature esistenti (per esempio eliminando quelle obsolete).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>PhotoQt works with either option, though the second way is set as default.</source>
        <translation>PhotoQt può lavorare con entrambe le opzioni, la seconda è quella predefinita.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, that does affect the performance and usability of PhotoQt, since thumbnails have to be newly re-created every time they are needed.</source>
        <translation>Anche se conviene usare almeno una delle due opzioni, il caching delle miniature può essere anche completamente disabilitato. Tuttavia, questo avrà effetto sulle performance e sull&apos;usabilità di PhotoQt (le miniature dovranno essere create nuovamente ogni volta che ve ne è bisogno).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="327"/>
        <source>Enable Thumbnail Cache</source>
        <translation>Abilita la cache delle miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="348"/>
        <source>File Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="354"/>
        <source>Database Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="379"/>
        <source>Current database filesize:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="405"/>
        <source>Entries in database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="426"/>
        <source>CLEAN UP database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="438"/>
        <source>ERASE database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabThumbnailsBasic</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Thumbnail Size</source>
        <translation>Dimensione Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but with different screen resolutions it might be nice to have them larger/smaller.</source>
        <translation>Qui puoi regolare la grandezza delle miniature. Puoi impostare una qualsiasi dimensione tra 20 e 256 pixel. Il valore predefinito è 80 pixel, ma con risoluzioni differenti potrebbe essere più bello averle più grandi o più piccole.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>Spacing Between Thumbnail Images</source>
        <translation>Spazio tra le Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them, however exactly that can be changed here.</source>
        <translation>Le miniature vengono mostrate allineate in una barra orizzontale sul bordo superiore o inferiore (a seconda delle impostazioni). Di default, non ci sono spazi fra di esse, ma questo può essere modificato qui.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>Lift-up of Thumbnail Images on Hovering</source>
        <translation>Innalzamento delle Miniature al passaggio del mouse</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels (default 10). Here you can increase/decrease this value according to your personal preference.</source>
        <translation>Quando il cursore passa sopra una miniatura, questa si alza di alcuni pixel (di default 10). Qui puoi aumentare/diminuire questo valore in base alle tue preferenze.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Keep Thumbnails Visible</source>
        <translation>Mantieni visibili le Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Per default the Thumbnails slide out over the edge of the screen. Here you can force them to stay visible. The big image is shrunk to fit into the empty space. Note, that the thumbnails will be hidden (and only shown on mouse hovering) once you zoomed the image in/out. Resetting the zoom restores the original visibility of the thumbnails.</source>
        <translation>Normalmente le miniature scorrono a partire dal bordo dello schermo quando richiamate. Qui puoi forzarle a restare visibili. L&apos;immagine principale verrà ridimensionata per adattarsi allo spazio rimasto. Nota che le miniature verranno nascoste (e mostrate solo tramite il passaggio del mouse) quando si ingrandirà l&apos;immagine. Riportare l&apos;immagine all&apos;ingrandimento originario farà tornare le miniature permanenti.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="259"/>
        <source>Keep Thumnails Visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic Thumbnail Creation</source>
        <translation>Creazione Dinamica Miniature</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic thumbnail creation means, that PhotoQt only sets up those thumbnail images that are actually needed, i.e. it stops once it reaches the end of the visible area and sits idle until you scroll left/right.</source>
        <translation>Attivare le Miniature dinamiche significa che PhotoQt crea solo le miniature necessarie, ovvero si ferma alla fine dell&apos;area visibile della barra delle miniature fino a quando non scorri a destra/sinistra.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Smart thumbnails are similar in nature. However, they make use of the fast, that once a thumbnail has been created, it can be loaded very quickly and efficiently. It also first loads all of the currently visible thumbnails, but it doesn&apos;t stop there: Any thumbnails (even if invisible at the moment) that once have been created are loaded. This is a nice compromise between efficiency and usability.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Enabling either the smart or dynamic option is recommended, as it increases the performance of PhotoQt significantly, while preserving the usability.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="295"/>
        <source>Normal Thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="301"/>
        <source>Dynamic Thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="307"/>
        <source>Smart Thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>Always center on Active Thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>If this option is set, then the active thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="333"/>
        <source>Center on Active Thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="799"/>
        <source>Okay, do it!</source>
        <translation>Ok, facciamolo!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="804"/>
        <source>Nooo, don&apos;t!</source>
        <translation>Nooo, non farlo!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="106"/>
        <source>Window Manager</source>
        <translation>Gestore Finestre</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="260"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="401"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <translation>Ci sono diverse opzioni che possono essere scelte per l&apos;immagine di sfondo.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="86"/>
        <source>Set as Wallpaper:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="114"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="192"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="219"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="250"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="337"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="350"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="505"/>
        <source>The wallpaper can be set to either of the available monitors (or any combination).</source>
        <translation>Lo sfondo può essere impostato per tutti i monitor disponibili (o qualsiasi combinazione).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="366"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="521"/>
        <source>Screen #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="483"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console &gt; Add-ons &gt; Modules &gt; System.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="494"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="556"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="576"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="578"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="579"/>
        <source>Workspace #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="636"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="647"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="658"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="669"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.&lt;br&gt;This is intended particularly for window managers that don&apos;t natively support wallpapers (e.g., like Openbox).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="685"/>
        <source>Use &apos;feh&apos;</source>
        <extracomment>feh is an application, do not translate</extracomment>
        <translation>Usa &apos;feh&apos;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="692"/>
        <source>Use &apos;nitrogen&apos;</source>
        <extracomment>nitrogen is an application, do not translate</extracomment>
        <translation>Usa &apos;nitrogen&apos;</translation>
    </message>
</context>
</TS>
