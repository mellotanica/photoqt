<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/fadein/About.qml" line="113"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>Το PhotoQt είναι ένας απλός προβολέας εικόνων, σχεδιασμένος ώστε να είναι καλαίσθητος, σε υψηλό βαθμό πραγματοποιήσιμος, πάντα εύκολος στη χρήση και γρήγορος.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="115"/>
        <source>With PhotoQt I try to be different than other image viewers (after all, there are plenty of good image viewers already out there). Its interface is kept very simple, yet there is an abundance of settings to customize the look and feel to make PhotoQt YOUR image viewer.</source>
        <translation>Με το PhotoQt προσπαθώ να διαφοροποιηθώ από άλλους προβολείς εικόνων (εν τέλει, υπάρχει πληθώρα καλών προβολέων εικόνων στη σκηνή). Το περιβάλλον χρήστη έχει διατηρηθεί απλοποιημένο, χωρίς να στερείται από πληρότητα ρυθμίσεων ώστε να προσαρμόσετε την εμφάνιση και αίσθηση στα μέτρα σας και να κάνετε το PhotoQt ΤΟΝ προβολέα των εικόνων σας.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="117"/>
        <source>I&apos;m not a trained programmer. I&apos;m a simple Maths student that loves doing stuff like this. Most of my programming knowledge I taught myself over the past 10-ish years, and it has been developing a lot since I started PhotoQt. During my studies in university I learned a lot about the basics of programming that I was missing. And simply working on PhotoQt gave me a lot of invaluable experience. So the code of PhotoQt might in places not quite be done in the best of ways, but I think it&apos;s getting better and better with each release.</source>
        <translation>Δεν έχω λάβει εκπαίδευση ως προγραμματιστής. Είμαι ένας απλός φοιτητής Μαθηματικών που αγαπά να κάνει τέτοια πράγματα. Απέκτησα μόνος μου τις προγραμματιστικές μου γνώσεις την τελευταία δεκαετία, και τις ανέπτυξα περισσότερο αφού ξεκίνησα το PhotoQt. Κατά τη διάρκεια των σπουδών μου στο πανεπιστήμιο έμαθα πολλά για τις βασικές αρχές προγραμματισμού που μου έλειπαν. Και απλά δουλεύοντας στο PhotoQt μου δόθηκε η ευκαιρία να αποκτήσω μια ανεκτίμητης αξίας εμπειρία. Ως εκ τούτου, ο κώδικας του PhotoQt μπορεί σε μερικά σημεία να μην είναι ο ιδανικότερος, αλλά πιστεύω ότι βελτιώνεται όλο και περισσότερο σε κάθε έκδοση.
</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="121"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>Μην ξεχάσετε να επισκεφτείτε την ιστοσελίδα μας: </translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="123"/>
        <source>If you find a bug or if you have a question or suggestion, tell me. I&apos;m open to any feedback I get :)</source>
        <translation>Αν συναντήσετε ένα σφάλμα ή έχετε μια ερώτηση ή σύσταση, μη διστάσετε να μου το αναφέρετε. Είμαι ανοιχτός σε οποιαδήποτε πρόταση λαμβάνω :)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="174"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don&apos;t forget to add the %1 in your translation!!</extracomment>
        <translation>Θέλετε να συμμετέχετε στην ομάδα και να ασχοληθείτε με κάτι, πχ. να μεταφράσετε το PhotoQt σε μια άλλη γλώσσα; Στείλτε μου ένα μήνυμα (%1), και όσο για τις μεταφράσεις ρίξτε μια ματιά στη σελίδα του έργου στο transifex:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="119"/>
        <source>I heard a number of times people saying, that PhotoQt is a &apos;copy&apos; of Picasa&apos;s image viewer. Well, it&apos;s not. In fact, I myself have never used Picasa. I have seen it in use though by others, and I can&apos;t deny that it influenced the basic design idea a little. But I&apos;m not trying to do something &apos;like Picasa&apos;. I try to do my own thing, and to do it as good as I can.</source>
        <translation>Άκουσα πολλές φορές να λένε ότι το PhotoQt είναι μια «αντιγραφή» του προβολέα εικόνων του Picasa&apos;. Ε λοιπόν δεν είναι. Προσωπικά δεν έχω χρησιμοποιήσει ποτέ το Picasa. Το έχω δει να το χρησιμοποιούν άλλοι, και δεν μπορώ να αρνηθώ ότι επηρέασε λίγο τη βασική ιδέα σχεδιασμού. Αλλά, δεν προσπαθώ να κάνω κάτι «τύπου Picasa». Προσπαθώ να κάνω κάτι δικό μου, και όσο καλύτερα γίνεται.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="158"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation>Ευχαριστώ όλους όσους συνεισέφεραν στο PhotoQt και/ή μετέφρασαν το PhotoQt σε μια άλλη γλώσσα! Είστε σπουδαίοι!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>website:</source>
        <translation>ιστοχώρος:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>Licensed under GPLv2 or later, without any guarantee</source>
        <translation>Υπόκειται στην άδεια χρήσης GPLv2 ή νεότερη, χωρίς εγγύηση</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="52"/>
        <source>Move:</source>
        <extracomment>as in: &quot;Move file...&quot;</extracomment>
        <translation>Μετακίνηση:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="62"/>
        <source>Previous</source>
        <extracomment>Go to previous file</extracomment>
        <translation>Προηγούμενο</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="68"/>
        <source>Next</source>
        <extracomment>Go to next file</extracomment>
        <translation>Επόμενο</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="99"/>
        <source>Rotate:</source>
        <extracomment>As in: Rotate file</extracomment>
        <translation>Περιστροφή:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="104"/>
        <source>Left</source>
        <extracomment>As in: rotate LEFT</extracomment>
        <translation>Αριστερά</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="111"/>
        <source>Right</source>
        <extracomment>As in: Rotate RIGHT</extracomment>
        <translation>Δεξιά</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="140"/>
        <source>Flip:</source>
        <extracomment>As in: Flip file</extracomment>
        <translation>Αναστροφή:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="145"/>
        <source>Horizontal</source>
        <extracomment>As in: Flip file HORIZONTALLY</extracomment>
        <translation>Οριζόντια</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="152"/>
        <source>Vertical</source>
        <extracomment>As in: Flip file VERTICALLY</extracomment>
        <translation>Κατακόρυφα</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="180"/>
        <source>Zoom:</source>
        <extracomment>Zoom file</extracomment>
        <translation>Εστίαση:</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="185"/>
        <source>In</source>
        <extracomment>As in: Zoom IN</extracomment>
        <translation>Μεγέθυνση</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="191"/>
        <source>Out</source>
        <translation>Σμίκρυνση</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="197"/>
        <source>Actual</source>
        <extracomment>As in: Zoom to ACTUAL size</extracomment>
        <translation>Τρέχον</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="203"/>
        <source>Reset</source>
        <extracomment>As in: Reset zoom</extracomment>
        <translation>Επαναφορά</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="224"/>
        <source>Scale Image</source>
        <translation>Κλιμάκωση εικόνας</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="234"/>
        <source>Open in default File Manager</source>
        <translation>Άνοιγμα στον προκαθορισμένο διαχειριστή αρχείων</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="266"/>
        <source>Rename File</source>
        <translation>Μετονομασία αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="276"/>
        <source>Delete File</source>
        <translation>Διαγραφή αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="301"/>
        <source>Copy File</source>
        <translation>Αντιγραφή αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="312"/>
        <source>Move File</source>
        <translation>Μετακίνηση αρχείου</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="11"/>
        <source>Confirm me?</source>
        <translation>Το επιβεβαιώνετε;</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="12"/>
        <source>Do you really want to do this?</source>
        <translation>Θέλετε πραγματικά να το κάνετε αυτό;</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="13"/>
        <source>Yes, do it</source>
        <translation>Ναι, κάνε το</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="14"/>
        <source>No, don&apos;t</source>
        <translation>Όχι, μην το κάνεις</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="110"/>
        <source>Don&apos;t ask again</source>
        <translation>Να μην ξαναγίνει η ερώτηση</translation>
    </message>
</context>
<context>
    <name>CustomDetectShortcut</name>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="72"/>
        <source>Detect key combination</source>
        <translation>Εντοπισμός συνδυασμού πλήκτρων</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="90"/>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="127"/>
        <source>Press keys</source>
        <translation>Πατήστε τα πλήκτρα</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="107"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
</context>
<context>
    <name>CustomExternalCommand</name>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="71"/>
        <source>External Command</source>
        <translation>Εξωτερική εντολή</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (with path)</source>
        <translation>τρέχον αρχείο (με τη διαδρομή)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (without path)</source>
        <translation>τρέχον αρχείο (δίχως τη διαδρομή)</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current directory</source>
        <translation>τρέχων κατάλογος</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="122"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="135"/>
        <source>Save it</source>
        <translation>Αποθήκευσέ το</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="159"/>
        <source>Select Executeable</source>
        <translation>Επιλογή εκτελέσιμου</translation>
    </message>
</context>
<context>
    <name>CustomMouseShortcut</name>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="69"/>
        <source>Set Mouse Shortcut</source>
        <translation>Ορισμός συντόμευσης ποντικιού</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="114"/>
        <source>Don&apos;t set</source>
        <translation>Να μη ρυθμιστεί</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="127"/>
        <source>Set Shortcut</source>
        <translation>Ρύθμιση συντόμευσης</translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="68"/>
        <source>Delete File</source>
        <translation>Διαγραφή αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="98"/>
        <source>Do you really want to delete this file?</source>
        <translation>Σίγουρα θέλετε να διαγράψετε αυτό το αρχείο;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Move to Trash</source>
        <translation>Μετακίνηση στα απορρίμματα</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Delete</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="137"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="150"/>
        <source>Delete permanently</source>
        <translation>Οριστική διαγραφή</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation>Enter = Μετακίνηση στα απορρίμματα, Shift+Enter = Μόνιμη διαγραφή, Escape = Ακύρωση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation>Enter = Διαγραφή, Escape = Ακύρωση</translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <location filename="../qml/mainview/Display.qml" line="481"/>
        <source>Hide</source>
        <translation>Απόκρυψη</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="507"/>
        <source>Open a file to begin</source>
        <translation>Ανοίξτε ένα αρχείο για να ξεκινήσετε</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="558"/>
        <source>No results found...</source>
        <translation>Κανένα αποτέλεσμα...</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="565"/>
        <source>Rotate Image?</source>
        <translation>Περιστροφή της εικόνας;</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>The Exif data of this image says, that this image is supposed to be rotated.</source>
        <translation>Τα δεδομένα Exif της εικόνας αναφέρουν ότι θα πρέπει να είναι περιστραμμένη.</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>Do you want to apply the rotation?</source>
        <translation>Επιθυμείτε την εφαρμογή της περιστροφής;</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="567"/>
        <source>Yes, do it</source>
        <translation>Ναι, κάνε το</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="568"/>
        <source>No, don&apos;t</source>
        <translation>Όχι, μην το κάνεις</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="68"/>
        <source>Filter images in current directory</source>
        <translation>Φιλτράρισμα εικόνων στον τρέχον φάκελο</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="83"/>
        <source>Enter here the term you want to search for. Seperate multiple terms by a space.</source>
        <translation>Εισάγετε εδώ τον όρο αναζήτησης. Διαχωρίστε τους όρους με κενό.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="90"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>Αν θέλετε να περιορίσετε έναν όρο σε επεκτάσεις ονόματος αρχείων, πληκτρολογήστε μια τελεία «.» πριν τον όρο.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="137"/>
        <source>Filter</source>
        <translation>Φίλτρο</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="145"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="160"/>
        <source>Remove Filter</source>
        <translation>Αφαίρεση φίλτρου</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="10"/>
        <source>Edit with</source>
        <translation>Επεξεργασία με</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="13"/>
        <source>Open in</source>
        <translation>Άνοιγμα με</translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="317"/>
        <source>Unknown</source>
        <translation>Άγνωστο</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="320"/>
        <source>Daylight</source>
        <translation>Φως ημέρας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="323"/>
        <source>Fluorescent</source>
        <translation>Φθορίζον</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="326"/>
        <source>Tungsten (incandescent light)</source>
        <translation>Βολφραμίου (φως πυρακτώσεως)</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="329"/>
        <source>Flash</source>
        <translation>Φλας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="332"/>
        <source>Fine weather</source>
        <translation>Καλοκαιρία</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="335"/>
        <source>Cloudy Weather</source>
        <translation>Συννεφιά</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="338"/>
        <source>Shade</source>
        <translation>Σκίαση</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="341"/>
        <source>Daylight fluorescent</source>
        <translation>Φθορίζον φως ημέρας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="344"/>
        <source>Day white fluorescent</source>
        <translation>Φθορίζον λευκό ημέρας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="347"/>
        <source>Cool white fluorescent</source>
        <translation>Φθορίζον λευκό ψυχρό</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="350"/>
        <source>White fluorescent</source>
        <translation>Φθορίζον λευκό</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="353"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="356"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="359"/>
        <source>Standard light</source>
        <translation>Τυπικό φως</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="371"/>
        <source>D50</source>
        <translation>D50</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="374"/>
        <source>ISO studio tungsten</source>
        <translation>Βολφραμίου ISO θαλάμου</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="377"/>
        <source>Other light source</source>
        <translation>Άλλη πηγή φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="380"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source</extracomment>
        <translation>Μη έγκυρη πηγή φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="389"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired</extracomment>
        <translation>ναι</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn&apos;t fired</extracomment>
        <translation>όχι</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="393"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash</extracomment>
        <translation>Χωρίς λειτουργία φλας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="395"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>δεν ανιχνεύτηκε επιστροφή στροβοσκοπικού φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>ανιχνεύτηκε επιστροφή στροβοσκοπικού φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="399"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>λειτουργία αναγκαστικού φλας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="401"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>αυτόματη λειτουργία</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>λειτουργία μείωσης κόκκινων ματιών</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="405"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>επιστροφή στροβοσκοπικού φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="407"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>επιστροφή στροβοσκοπικού φωτός</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="454"/>
        <source>Invalid flash</source>
        <translation>Μη έγκυρο φλας</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>Standard</source>
        <translation>Τυπικό</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="467"/>
        <source>Landscape</source>
        <translation>Τοπίο</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>Portrait</source>
        <translation>Πορτραίτο</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="473"/>
        <source>Night Scene</source>
        <translation>Νυχτερινή σκηνή</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene</extracomment>
        <translation>Μη έγκυρος τύπος σκηνής</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="25"/>
        <source>Open File</source>
        <translation>Άνοιγμα αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="26"/>
        <source>Settings</source>
        <translation>Ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="27"/>
        <source>Set as Wallpaper</source>
        <translation>Ορισμός ως ταπετσαρία</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="28"/>
        <source>Start Slideshow</source>
        <translation>Εκκίνηση παρουσίασης διαφάνειας</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="29"/>
        <source>Filter Images in Folder</source>
        <translation>Φιλτράρισμα εικόνων στον φάκελο </translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="30"/>
        <source>Show/Hide Metadata</source>
        <translation>Εμφάνιση/Απόκρυψη Μεταδεδομένων</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="31"/>
        <source>About PhotoQt</source>
        <translation>Σχετικά με το PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="32"/>
        <source>Hide (System Tray)</source>
        <translation>Απόκρυψη (πλαίσιο συστήματος)</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="33"/>
        <source>Quit</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="120"/>
        <source>Quickstart</source>
        <translation>Γρήγορη εκκίνηση</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="47"/>
        <source>Open image file</source>
        <translation>Άνοιγμα αρχείου εικόνας</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="121"/>
        <location filename="../cplusplus/mainwindow.cpp" line="122"/>
        <location filename="../cplusplus/mainwindow.cpp" line="124"/>
        <source>Images</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="126"/>
        <source>All Files</source>
        <translation>Όλα τα αρχεία</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="537"/>
        <source>Image Viewer</source>
        <translation>Προβολέας εικόνων</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="542"/>
        <source>Hide/Show PhotoQt</source>
        <translation>Απόκρυψη/Εμφάνιση PhotoQt</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="44"/>
        <source>No File Loaded</source>
        <translation>Δεν φορτώθηκε αρχείο</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="62"/>
        <source>File Format Not Supported</source>
        <translation>Η μορφή του αρχείου δεν υποστηρίζεται</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="80"/>
        <source>Invalid File</source>
        <translation>Μη έγκυρο αρχείο</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="121"/>
        <source>Keep Open</source>
        <translation>Διατήρηση ανοιχτό</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="191"/>
        <source>Filesize</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="193"/>
        <location filename="../qml/slidein/MetaData.qml" line="196"/>
        <source>Dimensions</source>
        <translation>Διαστάσεις</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="201"/>
        <source>Make</source>
        <translation>Μάρκα</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="202"/>
        <source>Model</source>
        <translation>Μοντέλο</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="203"/>
        <source>Software</source>
        <translation>Λογισμικό</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="205"/>
        <source>Time Photo was Taken</source>
        <translation>Ημερομηνία λήψης</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="206"/>
        <source>Exposure Time</source>
        <translation>Χρόνος έκθεσης</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="207"/>
        <source>Flash</source>
        <translation>Φλας</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="208"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="209"/>
        <source>Scene Type</source>
        <translation>Τύπος σκηνής</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="210"/>
        <source>Focal Length</source>
        <translation>Εστιακή απόσταση</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="211"/>
        <source>F Number</source>
        <translation>Αριθμός F</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="212"/>
        <source>Light Source</source>
        <translation>Πηγή φωτός</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="214"/>
        <source>Keywords</source>
        <translation>Λέξεις κλειδιά</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="215"/>
        <source>Location</source>
        <translation>Τοποθεσία</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="216"/>
        <source>Copyright</source>
        <translation>Πνευματικά δικαιώματα</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="218"/>
        <source>GPS Position</source>
        <translation>Θέση GPS</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="116"/>
        <source>Hide Counter</source>
        <translation>Απόκρυψη μετρητή</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="183"/>
        <source>Hide Filepath, leave Filename</source>
        <translation>Απόκρυψη της διαδρομής του αρχείου, να φαίνεται μόνο το όνομα</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="191"/>
        <source>Hide both, Filename and Filepath</source>
        <translation>Απόκρυψη και των δυο, του ονόματος και της διαδρομής του αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="236"/>
        <source>Filter:</source>
        <extracomment>As in: FILTER images</extracomment>
        <translation>Φίλτρο:</translation>
    </message>
</context>
<context>
    <name>QuickSettings</name>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="46"/>
        <source>Quick Settings</source>
        <translation>Γρήγορες ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="55"/>
        <source>Change settings with one click. They are saved and applied immediately. If you&apos;re unsure what a setting does, check the full settings for descriptions.</source>
        <translation>Αλλάξτε τις ρυθμίσεις με ένα κλικ. Θα αποθηκευτούν και θα εφαρμοστούν αμέσως. Αν δεν είστε σίγουρος -η για το τι κάνει μια ρύθμιση, ανατρέξτε στις πλήρης ρυθμίσεις για τις λεπτομέρειες.</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="77"/>
        <source>Sort by</source>
        <translation>Ταξινόμηση κατά</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Name</source>
        <translation>Όνομα</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Natural Name</source>
        <translation>Φυσικό όνομα</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>File Size</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="161"/>
        <source>Loop through folder</source>
        <translation>Επανάληψη στον φάκελο</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="182"/>
        <source>Window mode</source>
        <translation>Λειτουργία παραθύρου </translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="192"/>
        <source>Show window decoration</source>
        <translation>Εμφάνιση της διακόσμησης του παραθύρου</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="214"/>
        <source>Close on click on background</source>
        <translation>Έξοδος με κλικ στο φόντο</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="235"/>
        <source>Keep thumbnails visible</source>
        <translation>Διατήρηση των εικόνων επισκόπησης ορατές</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Normal thumbnails</source>
        <translation>Τυπικές εικόνες επισκόπησης
</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Dynamic thumbnails</source>
        <translation>Δυναμικές εικόνες επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="280"/>
        <source>Enable &apos;Quick Settings&apos;</source>
        <translation>Ενεργοποίηση των «Γρήγορων ρυθμίσεων»</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Smart thumbnails</source>
        <translation>Έξυπνες εικόνες επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>No tray icon</source>
        <translation>Χωρίς εικονίδιο στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Hide to tray icon</source>
        <translation>Απόκρυψη ως εικονίδιο στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation>Εμφάνιση του εικονιδίου συστήματος αλλά να μην γίνει απόκρυψη σε αυτό</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="305"/>
        <source>Show full settings</source>
        <translation>Εμφάνιση πλήρων ρυθμίσεων</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="68"/>
        <source>Rename File</source>
        <translation>Μετονομασία αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="148"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="67"/>
        <source>Scale Image</source>
        <translation>Κλιμάκωση εικόνας</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="83"/>
        <source>Current Size:</source>
        <translation>Τρέχον μέγεθος:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="112"/>
        <source>Error! Something went wrong, unable to save new dimension...</source>
        <translation>Σφάλμα! Κάτι πήγε στραβά, αδύνατη η αποθήκευση των νέων διαστάσεων...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="136"/>
        <source>New width:</source>
        <translation>Νέο πλάτος:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="143"/>
        <source>New height:</source>
        <translation>Νέο ύψος:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="248"/>
        <source>Quality</source>
        <translation>Ποιότητα</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="298"/>
        <source>Scale into new file</source>
        <translation>Κλιμάκωση σε νέο αρχείο</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="315"/>
        <source>Don&apos;t scale</source>
        <translation>Να μην γίνει κλιμάκωση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="217"/>
        <source>Aspect Ratio</source>
        <translation>Αναλογίες διαστάσεων</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="284"/>
        <source>Scale in place</source>
        <translation>Κλιμάκωση επί αυτού</translation>
    </message>
</context>
<context>
    <name>SettingsItem</name>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="59"/>
        <source>Look and Feel</source>
        <translation>Όψη και αίσθηση</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="72"/>
        <location filename="../qml/settings/SettingsItem.qml" line="124"/>
        <source>Basic</source>
        <translation>Βασικό</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="93"/>
        <location filename="../qml/settings/SettingsItem.qml" line="143"/>
        <source>Advanced</source>
        <translation>Προηγμένο</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="115"/>
        <source>Thumbnails</source>
        <translation>Εικόνες επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="174"/>
        <source>Metadata</source>
        <translation>Μεταδεδομένα</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="194"/>
        <source>Other Settings</source>
        <translation>Λοιπές ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="219"/>
        <source>Filetypes</source>
        <translation>Τύποι αρχείων</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="240"/>
        <source>Shortcuts</source>
        <translation>Συντομεύσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="313"/>
        <source>Restore Default Settings</source>
        <translation>Επαναφορά προκαθορισμένων ρυθμίσεων</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="326"/>
        <source>Exit and Discard Changes</source>
        <translation>Έξοδος χωρίς αποθήκευση αλλαγών</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="344"/>
        <source>Save Changes and Exit</source>
        <translation>Αποθήκευση αλλαγών και έξοδος</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="358"/>
        <source>Clean Database</source>
        <translation>Καθαρισμός της βάσης δεδομένων</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>Do you really want to clean up the database?</source>
        <translation>Θέλετε πραγματικά τον καθαρισμό της βάσης δεδομένων;</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation>Αυτή η ενέργεια θα αφαιρέσει όλες τις παρωχημένες επισκοπήσεις, βελτιώνοντας τις επιδόσεις του PhotoQt.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This process might take a little while.</source>
        <translation>Αυτή η διεργασία ίσως να διαρκέσει αρκετά.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="360"/>
        <source>Yes, clean is good</source>
        <translation>Ναι, το καθάρισμα κάνει καλό</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="361"/>
        <source>No, don&apos;t have time for that</source>
        <translation>Όχι, δεν έχω χρόνο τώρα</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="368"/>
        <source>Erase Database</source>
        <translation>Διαγραφή της βάσης δεδομένων</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>Do you really want to ERASE the entire database?</source>
        <translation>Θέλετε πραγματικά τη ΔΙΑΓΡΑΦΗ της βάσης δεδομένων εξ ολοκλήρου;</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This removes every single item in the database! This step should never really be necessarily. After that, every thumbnail has to be newly re-created.</source>
        <translation>Αυτή η ενέργεια θα αφαιρέσει όλες τις εγγραφές τις βάσης δεδομένων. Μετά από αυτό, κάθε εικόνα επισκόπησης θα πρέπει να δημιουργηθεί εκ νέου.</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This step cannot be reversed!</source>
        <translation>Αυτό το βήμα δεν μπορεί να αναιρεθεί!</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="370"/>
        <source>Yes, get rid of it all</source>
        <translation>Ναι, θέλω να τα ξεφορτωθώ όλα</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="371"/>
        <source>Nooo, I want to keep it</source>
        <translation>Όχιιιι, θέλω να τα κρατήσω</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="378"/>
        <source>Set Default Shortcuts</source>
        <translation>Προκαθορισμένες συντομεύσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="379"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation>Είστε σίγουρος-η ότι επιθυμείτε την επαναφορά των συντομεύσεων με τις εξ ορισμού;</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="380"/>
        <source>Yes, please</source>
        <translation>Ναι, παρακαλώ</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="381"/>
        <source>Nah, don&apos;t</source>
        <translation>Μπα, όχι</translation>
    </message>
</context>
<context>
    <name>Slideshow</name>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="86"/>
        <source>Start a Slideshow</source>
        <translation>Έναρξη προβολής διαφανειών</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="97"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that is played in the background.</source>
        <translation>Υπάρχουν μερικές ρυθμίσεις που μπορούν να διαμορφωθούν για μια παρουσίαση διαφανειών, όπως το χρονικό διάστημα μεταξύ των εικόνων, αν θα υπάρχει κίνηση μετάβασης και τη διάρκειά της, όπως και το μουσικό αρχείο που θα αναπαραχθεί στο παρασκήνιο.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="104"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>Αφού έχετε ορίσει τις επιθυμητές επιλογές, μπορείτε να ξεκινήσετε μια παρουσίαση διαφανειών την επόμενη φορά με «Γρήγορη εκκίνηση» πχ παρακάμπτοντας αυτό το παράθυρο ρυθμίσεων. </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="114"/>
        <source>Time in between</source>
        <translation>Χρονική καθυστέρηση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="121"/>
        <source>Adjust the time between the images. The time specified here is the amount of time the image will be completely visible, i.e. the transitioning (if set) is not part of this time.</source>
        <translation>Ρυθμίζει τη χρονική καθυστέρηση μεταξύ των εικόνων. Ο χρόνος που καθορίζετε εδώ είναι αυτός που απαιτείται μέχρι την ολοκληρωτική εμφάνιση της εικόνας, πχ ο χρόνος της μετάβασης (αν υπάρχει μια) δεν συμπεριλαμβάνεται.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="161"/>
        <source>Smooth Transition</source>
        <translation>Ομαλή μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="168"/>
        <source>Here you can set, if you want the images to fade into each other, and how fast they are to do that.</source>
        <translation>Εδώ μπορείτε να ρυθμίσετε αν επιθυμείτε οι εικόνες να έχουν ομαλό σβήσιμο μεταξύ τους, και το πόσο γρήγορα θα γίνεται αυτό.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="182"/>
        <source>No Transition</source>
        <translation>Καμιά μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="198"/>
        <source>Long Transition</source>
        <translation>Αργή μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="210"/>
        <source>Shuffle and Loop</source>
        <translation>Ανακάτεμα και βρόγχος</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="217"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note, that no image will be shown twice before every image has been shown once.</source>
        <translation>Αν επιθυμείτε το βρόγχο στην περιήγηση των εικόνων από το PhotoQt (πχ μετά την εμφάνιση της τελευταίας εικόνας μεταβαίνει στην πρώτη), ή αν επιθυμείτε το PhotoQt να φορτώνει τις εικόνες σε τυχαία σειρά, μπορείτε να ενεργοποιήσετε τα παρακάτω αντίστοιχα πλαίσια ελέγχου. Σημειώστε ότι δεν θα εμφανίζεται μια εικόνα δυο φορές πριν την προβολή όλων των εικόνων μια φορά.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="223"/>
        <source>Loop over images</source>
        <translation>Βρόγχος περιήγησης εικόνων</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="229"/>
        <source>Shuffle images</source>
        <translation>Ανακάτεμα εικόνων</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="241"/>
        <source>Hide Quickinfo</source>
        <translation>Απόκρυψη γρήγορων πληροφοριών</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="249"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>Ανάλογα με την εγκατάστασή σας, το PhotoQt εμφανίζει μερικές πληροφορίες στο πάνω άκρο, όπως τη θέση στον τρέχοντα κατάλογο ή τη διαδρομή/όνομα αρχείου. Εδώ μπορείτε να τα απενεργοποιήσετε προσωρινώς για την παρουσίαση της διαφάνειας.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="254"/>
        <source>Hide Quickinfos</source>
        <translation>Απόκρυψη γρήγορων πληροφοριών</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="266"/>
        <source>Background Music</source>
        <translation>Μουσική παρασκηνίου</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="273"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>Μερικοί μπορεί να θέλουν να ακούν και λίγη μουσική κατά την παρουσίαση των διαφανειών. Εδώ μπορείτε να επιλέξετε το αρχείο μουσικής που θέλετε να αναπαραχθεί στο παρασκήνιο.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="280"/>
        <source>Enable Music</source>
        <translation>Ενεργοποίηση μουσικής</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="310"/>
        <source>Click here to select music file...</source>
        <translation>Κλικ εδώ για επιλογή του αρχείου μουσικής...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="344"/>
        <source>Okay, lets start</source>
        <translation>Εντάξει, ας ξεκινήσουμε</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="348"/>
        <source>Wait, maybe later</source>
        <translation>Στάσου, ίσως αργότερα</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="352"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Select music file...</source>
        <translation>Επιλέξτε αρχείο μουσικής...</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Music Files</source>
        <translation>Αρχεία μουσικής</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>All Files</source>
        <translation>Όλα τα αρχεία</translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Play Slideshow</source>
        <translation>Παρουσίαση διαφανειών</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Pause Slideshow</source>
        <translation>Παύση της παρουσίασης διαφανειών</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="49"/>
        <source>Music Volume:</source>
        <translation>Ένταση μουσικής:</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="77"/>
        <source>Exit Slideshow</source>
        <translation>Έξοδος από την παρουσίαση διαφανειών</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully installed</source>
        <translation>Το PhotoQt εγκαταστάθηκε επιτυχώς</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully updated</source>
        <translation>Το PhotoQt ενημερώθηκε με επιτυχία</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>Welcome to PhotoQt. PhotoQt is an image viewer, aimed at being fast and reliable, highly customisable and good looking.</source>
        <translation>Καλώς ήλθατε στο PhotoQt. Το PhotoQt είναι ένας προβολέας εικόνων, που στοχεύει στην ταχύτητα και την αξιοπιστία, είναι σε υψηλό βαθμό παραμετροποιήσιμος και με ευχάριστη όψη.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>This app started out more than three and a half years ago, and it has developed quite a bit since then. It has become very efficient, reliable, and highly flexible (check out the settings). I&apos;m convinced it can hold up to the more &apos;traditional&apos; image viewers out there in every way.</source>
        <translation>Αυτή η εφαρμογή ξεκίνησε πριν τριάμισι έτη, και έχει αναπτυχθεί σε μεγάλο βαθμό από τότε. Έχει γίνει αρκετά αποτελεσματική, αξιόπιστη, και σε μεγάλο βαθμό προσαρμόσιμη (ανατρέξτε στις ρυθμίσεις). Είμαι πεπεισμένος ότι οριστικά μπορεί να συναγωνιστεί τους περισσότερους «παραδοσιακούς» προβολείς εικόνων στη σκηνή.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Welcome back to PhotoQt. It hasn&apos;t been that long since the last release of PhotoQt. Yet, it changed pretty much entirely, as it now is based on QtQuick rather than QWidgets. A large quantity of the code had to be re-written, while some chunks could be re-used. Thus, it is now more reliable than ever before and overall simply feels well rounded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>Many File Formats</source>
        <translation>Πολλοί τύποι αρχείων</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>PhotoQt can make use of GraphicsMagick, an image library, to display many different image formats. Currently, there are up to 72 different file formats supported (exact number depends on your system)! You can find a list of it in the settings (Tab &apos;Other&apos;). There you can en-/disable different ones and also add custom file endings.</source>
        <translation>Το PhotoQt μπορεί να χρησιμοποιεί το GraphicsMagick, μια βιβλιοθήκη εικόνων, για τη προβολή των διαφόρων μορφών εικόνων. Επί του παρόντος, υποστηρίζονται μέχρι και 72 διαφορετικές μορφές (ο ακριβής αριθμός εξαρτάται από το σύστημά σας)! Μπορείτε να βρείτε μια σχετική λίστα στις ρυθμίσεις (καρτέλα «Άλλα»). Εκεί μπορείτε να απ-/ενεργοποιήσετε πολλές από αυτές όπως και να προσθέσετε προσαρμοσμένα επιθέματα αρχείων.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>Make PhotoQt your own</source>
        <translation>Κάντε το PhotoQt δικό σας</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>PhotoQt has an extensive settings area. By default you can call it with the shortcut &apos;e&apos; or through the dropdown menu at the top edge towards the top right corner. You can adjust almost everything in PhotoQt, and it&apos;s certainly worth having a look there. Each setting usually comes with a little explanation text. Some of the most often used settings can also be conveniently adjusted in a slide-in widget, hidden behind the right screen edge.</source>
        <translation>Το PhotoQt διαθέτει μια εκτεταμένη διαμόρφωση ρυθμίσεων. Μπορείτε να μεταβείτε σε αυτήν μέσω της προκαθορισμένης συντόμευσης «e» ή μέσω του κυλιόμενου μενού στην κορυφή δεξιά. Στο PhotoQt μπορείτε να ρυθμίσετε σχεδόν τα πάντα, και πραγματικά αξίζει να ρίξετε μια ματιά εκεί. Επίσης, κάθε ρύθμιση συνοδεύεται με μια μικρή επεξήγηση. Μερικές από τις συχνότερα χρησιμοποιούμενες ρυθμίσεις μπορούν να ρυθμιστούν στο πλευρικό συστατικό, κρυμμένο πίσω από το άκρο της δεξιά πλευράς της οθόνης.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Most images store some additional information within the file&apos;s metadata. PhotoQt can read and display a selection of this data. You can find this information in the slide-in window hidden behind the left edge of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>PhotoQt also brings a slideshow feature. When you start a slideshow, it starts at the currently displayed image. There are a couple of settings that can be set, like transition, speed, loop, and shuffle. Plus, you can set a music file that is played in the background. When the slideshow takes longer than the music file, then PhotoQt starts the music file all over from the beginning. At anytime during the slideshow, you can move the mouse cursor to the top edge of the screen to get a little bar, where you can pause/exit the slideshow and adjust the music volume.</source>
        <translation>Το PhotoQt παρέχει και ένα χαρακτηριστικό προβολής διαφανειών. Όταν εκκινείτε μια προβολή διαφανειών, ξεκινά από την τρέχουσα προβαλλόμενη εικόνα. Υπάρχουν μερικές ρυθμίσεις που μπορούν να γίνουν ρυθμίσεις όπως για τη μετάβαση, την ταχύτητα, τον βρόγχο και την ανάμιξη. Επιπλέον, μπορείτε να ορίσετε ένα αρχείο μουσικής να αναπαραχθεί στο παρασκήνιο. Όταν η προβολή διαφανειών διαρκεί περισσότερο από το αρχείο μουσικής, το PhotoQt επανεκκινεί το αρχείο μουσικής από την αρχή. Οποιαδήποτε στιγμή της προβολής διαφανειών, μπορείτε να μετακινήσετε τον δρομέα του ποντικιού στην κορυφή ώστε να εμφανίσετε μια μικρή γραμμή, όπου μπορείτε μέσω των κουμπιών ελέγχου να διακόψετε/εγκαταλείψετε την προβολή διαφανειών και να ρυθμίσετε την ένταση της μουσικής.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>Thumbnails</source>
        <translation>Εικόνες επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Here below you find a short overview of a selection of a few things PhotoQt has to offer, but feel free to skip it and just get started.</source>
        <translation>Παρακάτω θα βρείτε μια μικρή επισκόπηση μιας επιλογής χαρακτηριστικών του PhotoQt, αλλά αισθανθείτε ελεύθερα να την παραλείψετε και να ξεκινήσετε άμεσα τη χρήση της εφαρμογής.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>What would be an image viewer without thumbnails support? It would only be half as good. Whenever you load an image, PhotoQt loads the other images in the directory in the background (by default, it tries to be smart about it and only loads the ones that are needed). It lines them up in a row at the bottom edge (move your mouse there to see them). There are many settings just for the thumbnails, like, e.g., size, liftup, en-/disabled, type, filename, permanently shown/hidden, etc. PhotoQt&apos;s quite flexible with that.</source>
        <translation>Τι θα ήταν ένας προβολέας εικόνων χωρίς την υποστήριξη εικόνων επισκόπησης; Θα ήταν απλά καλός στο ήμισυ Κάθε φορά που φορτώνετε νια εικόνα, το PhotoQt φορτώνει τις άλλες εικόνες στον κατάλογο στο παρασκήνιο (εξ ορισμού, προσπαθεί να φανεί σχετικά έξυπνο, και φορτώνει μόνο τις εικόνες που είναι απαραίτητο). Τις στοιχίζει σε μια σειρά στο κάτω μέρος (μετακινήστε το ποντίκι προς τα κάτω για να τις δείτε). Υπάρχουν πολλές ρυθμίσεις για τις εικόνες επισκόπησης, π.χ. μέγεθος, αναπήδηση, (απ)ενεργοποίηση, τύπος, όνομα αρχείου, μόνιμη εμφάνιση/απόκρυψη, κλπ. Το PhotoQt είναι αρκετά ευέλικτο όσον αφορά τις συγκεκριμένες ρυθμίσεις.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>Shortcuts</source>
        <translation>Συντομεύσεις</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>One of the many strengths of PhotoQt is the ability to easily set a shortcut for almost anything. Even mouse shortcuts are possible! You can choose from a huge number of internal functions, or you can run any custom script or command.</source>
        <translation>Ένα από τα πολλά ισχυρά σημεία του PhotoQt είναι η ικανότητα του εύκολου καθορισμού μιας συντόμευσης για σχεδόν οποιαδήποτε ενέργεια. Ακόμα και οι συντομεύσεις ποντικιού είναι εφικτές! Μπορείτε να επιλέξετε μεταξύ ενός μεγάλου αριθμού εσωτερικών λειτουργιών, ή να εκτελέσετε ένα προσαρμοσμένο σενάριο ή εντολή.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Image Information (Exif/IPTC)</source>
        <translation>Πληροφορίες εικόνας (Exif/PTC)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>Slideshow</source>
        <translation>Παρουσίαση διαφανειών</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>Localisation</source>
        <translation>Τοπικότητα</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>PhotoQt comes with a number of translations. Many have taken some of their time to create/update one of them (Thank you!). Not all of them are complete... do you want to help?</source>
        <translation>Το PhotoQt παρέχει ένα πλήθος μεταφράσεων. Πολλοί έχουν αφιερώσει από τον ελεύθερο χρόνο τους για τη δημιουργία ενός αρχείου μετάφρασης ή να ενημερώσουν κάποια από αυτές (Ευχαριστώ!). Δεν είναι όλες ολοκληρωμένες... θέλετε να βοηθήσετε;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="355"/>
        <source>There are many many more features. Best is, you just give it a go. Don&apos;t forget to check out the settings to make PhotoQt YOUR image viewer. Enjoy :-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="387"/>
        <source>Okay, I got enough now. Lets start!</source>
        <translation>Εντάξει τα κατάλαβα όλα. Ας ξεκινήσουμε!</translation>
    </message>
</context>
<context>
    <name>TabDetails</name>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="55"/>
        <source>Image Metadata</source>
        <translation>Μεταδεδομένα εικόνας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="74"/>
        <source>PhotoQt can display different information of and about each image. The widget for this information is on the left outside the screen and slides in when mouse gets close to it and/or when the set shortcut (default Ctrl+E) is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <translation>Το PhotoQt μπορεί να εμφανίσει διάφορες πληροφορίες της εικόνας και σχετικά με αυτή. Το συστατικό για αυτές τις πληροφορίες είναι στην αριστερή άκρη της οθόνης και εξέρχεται όταν πλησιάσει σε αυτήν το ποντίκι και/ή όταν η ορισθείσα συντόμευση (εξ&apos; ορισμού η Ctrl+E) ενεργοποιείται. Αν το θέλετε, η ενεργοποίηση μέσω της κίνησης του ποντικιού μπορεί να απενεργοποιηθεί επιλέγοντας το παρακάτω πλαίσιο.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Trigger Widget on Mouse Hovering</source>
        <translation>Ενεργοποίηση του Συστατικού με το πέρασμα του ποντικιού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut. This can come in handy, if you get annoyed by accidentally opening the widget occasionally.</source>
        <translation>Εξ&apos; ορισμού το συστατικό πληροφοριών μπορεί να εμφανιστεί με δυο τρόπους: Με μετακίνηση του ποντικιού στην αριστερή άκρη της οθόνης για προσωρινή εμφάνιση (όσο βρίσκεται το ποντίκι στη συγκεκριμένη θέση), ή με κλικ στο πλαίσιο ελέγχου (το πλαίσιο ελέγχου απομνημονεύεται μόνο ανά συνεδρία, και όχι μόνιμα!). Εναλλακτικά το συστατικό μπορεί να ενεργοποιηθεί μέσω συντόμευσης. Αν το θέλετε η ενεργοποίηση μέσω του ποντικιού μπορεί να απενεργοποιηθεί, και το συστατικό θα εμφανίζεται μέσω της συντόμευσης μόνο. Αυτό μπορεί να σας φανεί χρήσιμο αν σας συμβαίνει να ανοίγετε κατά λάθος το συστατικό και σας είναι ενοχλητικό.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="118"/>
        <source>Turn mouse triggering OFF</source>
        <translation>Κλείσιμο της ενεργοποίησης μέσω του ποντικιού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>Which items are shown?</source>
        <translation>Ποια αντικείμενα εμφανίζονται;</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>PhotoQt can display a number of information about the image (often called &apos;Exif data&apos;&apos;). However, you might not be interested in all of them, hence you can choose to disable some of them here.</source>
        <translation>Το PhotoQt μπορεί να προβάλλει έναν αριθμό πληροφοριών σχετικά με την εικόνα (συχνά ονομάζονται «δεδομένα Exif»). Ωστόσο, ίσως να μην σας ενδιαφέρουν όλα αυτά, και για αυτό, μπορείτε εδώ να επιλέξετε την απενεργοποίηση μερικών.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="149"/>
        <source>Enable ALL</source>
        <translation>Ενεργοποίηση όλων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="155"/>
        <source>Disable ALL</source>
        <translation>Απενεργοποίηση όλων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="179"/>
        <source>Filesize</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="180"/>
        <source>Dimensions</source>
        <translation>Διαστάσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="181"/>
        <source>Make</source>
        <translation>Μάρκα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="182"/>
        <source>Model</source>
        <translation>Μοντέλο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="183"/>
        <source>Software</source>
        <translation>Λογισμικό</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="184"/>
        <source>Time Photo was Taken</source>
        <translation>Ημερομηνία λήψης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="185"/>
        <source>Exposure Time</source>
        <translation>Χρόνος έκθεσης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="186"/>
        <source>Flash</source>
        <translation>Φλας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="187"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="188"/>
        <source>Scene Type</source>
        <translation>Τύπος σκηνής</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="189"/>
        <source>Focal Length</source>
        <translation>Εστιακή απόσταση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="190"/>
        <source>F-Number</source>
        <translation>Αριθμός F</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="191"/>
        <source>Light Source</source>
        <translation>Πηγή φωτός</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="192"/>
        <source>Keywords</source>
        <translation>Λέξεις κλειδιά</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="193"/>
        <source>Location</source>
        <translation>Τοποθεσία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="194"/>
        <source>Copyright</source>
        <translation>Πνευματικά δικαιώματα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="195"/>
        <source>GPS Position</source>
        <translation>Θέση GPS</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Adjusting Font Size</source>
        <translation>Προσαρμογή μεγέθους γραμματοσειράς</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Computers can have very different resolutions. On some of them, it might be nice to increase the font size of the labels to have them easier readable. Often, a size of 8 or 9 should be working quite well...</source>
        <translation>Οι υπολογιστές μπορεί να έχουν πολύ διαφορετικές αναλύσεις. Σε μερικούς είναι απαραίτητο η μεγέθυνση της γραμματοσειράς των ετικετών ώστε να είναι ευανάγνωστες. Το μέγεθος 8 ή 9 συνήθως δουλεύει καλά...</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Rotating/Flipping Image according to Exif Data</source>
        <translation>Περιστροφή/Αναστροφή της εικόνας βάσει των δεδομένων Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly. When asking PhotoQt to always rotate images automatically without asking, it already does so at image load (including thumbnails).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="294"/>
        <source>Never rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="300"/>
        <source>Always rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="305"/>
        <source>Always ask</source>
        <translation>Ερώτηση πάντα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>Online map for GPS</source>
        <translation>Χάρτης GPS στο διαδίκτυο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>If you&apos;re image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>Αν οι εικόνες σας περιέχουν τοποθεσία GPS, τότε με ένα κλικ στην τοποθεσία θα την ανοίξετε σε ένα δικτυακό χάρτη στον προκαθορισμένος σας περιηγητή. Εδώ μπορείτε να επιλέξετε τη διαδικτυακή υπηρεσία που επιθυμείτε να χρησιμοποιήσετε (συστάσεις για άλλους διαδικτυακούς χάρτες είναι ευπρόσδεκτες).</translation>
    </message>
</context>
<context>
    <name>TabFiletypes</name>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="55"/>
        <source>Filetypes</source>
        <translation>Τύποι αρχείων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>File Types - Qt</source>
        <translation>Τύποι αρχείων - Qt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.&lt;br&gt;If a file ending for one of the formats is missing, you can add it below, formatted like &apos;*.ending&apos; (without single quotation marks), multiple entries seperated by commas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="102"/>
        <source>Extra File Types:</source>
        <translation>Επιπλέον τύποι αρχείων:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="117"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="166"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="214"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="337"/>
        <source>Mark None</source>
        <translation>Σήμανση κανενός</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="123"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="172"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="220"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="343"/>
        <source>Mark All</source>
        <translation>Σήμανση όλων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>File Types - GraphicsMagick</source>
        <translation>Τύποι αρχείων - GraphicsMagick</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. If you prefer not to have one or the other enabled in PhotoQt, you can simply disable individual formats below.&lt;br&gt;There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>File Types - GraphicsMagick (requires Ghostscript)</source>
        <translation>Τύποι αρχείων - GraphicsMagick (απαιτείται το Ghostscript)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and work. However, they require Ghostscript to be installed on the system.</source>
        <translation>Οι παρακάτω τύποι αρχείων υποστηρίζονται από το GraphicsMagick, και έχουν δοκιμαστεί επιτυχώς. Ωστόσο, απαιτούν το Ghostscript εγκατεστημένο στο σύστημά σας.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>File Types - Other tools required</source>
        <translation>Τύποι αρχείων - Απαιτούνται άλλα εργαλεία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation>Οι παρακάτω τύποι αρχείων υποστηρίζονται μέσω τρίτων εργαλείων. Θα πρέπει να τα έχετε εγκαταστήσει για να τα χρησιμοποιήσετε.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>Note</source>
        <translation>Σημείωση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>If an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation>Αν μια μορφή εικόνας παρέχεται επίσης από το GraphicsMagick/Qt, τότε το PhotoQt επιλέγει με προτεραιότητα το εξωτερικό εργαλείο (αν είναι ενεργοποιημένο).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <source>Gimp&apos;s XCF file format.</source>
        <extracomment>&apos;Makes use of&apos; is in connection with an external tool (i.e., it &apos;makes use of&apos; tool abc)</extracomment>
        <translation>Μορφή αρχείων Gimp XCF.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Makes use of</source>
        <translation>Χρησιμοποιεί</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Adobe Photoshop PSD and PSB.</source>
        <translation>Adobe Photoshop PSD και PSB.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>File Types - GraphicsMagick (Untested)</source>
        <translation>Τύποι αρχείων - Graphicsmagick (Αδοκίμαστα)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>The following file types are generally supported by GraphicsMagick, but I wasn&apos;t able to test them in PhotoQt (due to lack of test images). They might very well be working, but I simply can&apos;t say. If you decide to enable some of the, the worst that could happen ist, that you see an error image instead of the actual image.</source>
        <translation>Οι παρακάτω τύποι αρχείων υποστηρίζονται γενικά από το GraphicsMagick, αλλά δεν ήμουν σε θέση να τους δοκιμάσω στο PhotoQt (λόγω έλλειψης εικόνων δοκιμής). Μπορεί να δουλεύουν καλά, αλλά δεν μπορώ να το επιβεβαιώσω. Αν ενεργοποιήσετε μερικούς από αυτούς, στη χειρότερη θα λάβετε ένα μήνυμα σφάλματος αντί της τρέχουσας εικόνας.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>If you happen to have an image in one of those formats and don&apos;t mind sending it to me, that&apos;d be really cool...</source>
        <translation>Αν τυχόν έχετε κάποια εικόνα αυτού του τύπου και δεν σας πειράζει να μου τη στείλετε, θα ήμουν ευγνώμων...</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelAdvanced</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="54"/>
        <source>Advanced Settings</source>
        <translation>Προηγμένες ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>Background of PhotoQt</source>
        <translation>Φόντο του PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above.&lt;br&gt;Note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="91"/>
        <source>Use (half-)transparent background</source>
        <translation>Χρήση (ημι-)διαφανές φόντο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="97"/>
        <source>Use faked transparency</source>
        <translation>Χρήση ψευδό-διαφάνειας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="102"/>
        <source>Use custom background image</source>
        <translation>Χρήση της τρέχουσας ταπετσαρίας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="107"/>
        <source>Use one-coloured, non-transparent background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="164"/>
        <source>No image selected</source>
        <translation>Δεν έχει επιλεγεί εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="188"/>
        <source>Scale to fit</source>
        <translation>Προσαρμογή στην οθόνη</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="194"/>
        <source>Scale and Crop to fit</source>
        <translation>Κλιμάκωση και αποκοπή για προσαρμογή</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="199"/>
        <source>Stretch to fit</source>
        <translation>Έκταση για προσαρμογή στην οθόνη</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="204"/>
        <source>Center image</source>
        <translation>Κεντράρισμα εικόνας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="209"/>
        <source>Tile image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Background/Overlay Color</source>
        <translation>Χρώμα παρασκηνίου/επικάλυψης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>Εδώ μπορείτε να ρυθμίσετε το χρώμα παρασκηνίου του PhotoQt (του τμήματος που δεν καλύπτεται από κάποια εικόνα). Όταν χρησιμοποιείτε τη σύνθεση ή μια ταπετσαρία, μπορείτε να ορίσετε επίσης μια τιμή alpha, πχ: τη διαφάνεια του χρώματος της στρώσης επικάλυψης. Αν δεν είναι ενεργοποιημένη η σύνθεση ή δεν έχει οριστεί μια ταπετσαρία, τότε αυτό το χρώμα θα είναι και το αδιαφανές φόντο του PhotoQt.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="259"/>
        <source>Red:</source>
        <translation>Κόκκινο:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="281"/>
        <source>Green:</source>
        <translation>Πράσινο:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="303"/>
        <source>Blue:</source>
        <translation>Κυανό:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="325"/>
        <source>Alpha:</source>
        <translation>Άλφα:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="387"/>
        <source>Preview colour</source>
        <translation>Προεπισκόπηση χρώματος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Border Around Image</source>
        <translation>Περιθώριο γύρω από την εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels (looks better). Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>Κατά τη φόρτωση μιας εικόνας, αυτή δεν εμφανίζεται εντελώς σε πλήρη οθόνη από προεπιλογή. Υπάρχει ένα μικρό περιθώριο γύρω από την εικόνα μερικών pixel (δείχνει καλύτερα). Εδώ μπορείτε να ρυθμίσετε το πλάτος του περιθωρίου (0 για απενεργοποίηση).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Close on Click in empty area</source>
        <translation>Κλείσιμο με κλικ σε μια κενή περιοχή</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. It can be a nice feature, PhotoQt will feel even more like a &quot;floating layer&quot;. However, you might at times close PhotoQt accidentally.</source>
        <translation>Αυτή η επιλογή κάνει το PhotoQt να συμπεριφέρεται σαν έναν προβολέα JavaScript που συναντάμε σε πολλές ιστοσελίδες. Ένα κλικ έξω από την εικόνα στον κενό χώρο του παρασκηνίου θα κλείσει την εφαρμογή. Μπορεί να είναι ένα ωραίο χαρακτηριστικό, το PhotoQt θα συμπεριφέρεται ως μια «πλωτή στρώση». Ωστόσο, μπορεί μερικές φορές να κλείσετε το PhotoQt κατά λάθος.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Note: If you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>Σημείωση: Αν χρησιμοποιείτε το κλικ του ποντικιού ως συντόμευση, τότε αυτή η επιλογή δεν θα έχει κάποιο αποτέλεσμα!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="475"/>
        <source>Close on click in empty area</source>
        <translation>Κλείσιμο με κλικ σε μια κενή περιοχή</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>Looping Through Folder</source>
        <translation>Επανάληψη στον φάκελο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>When you load the last image in a directory and select &apos;Next&apos;, PhotoQt automatically jumps to the first image (and vice versa: if you select &apos;Previous&apos; while having the first image loaded, PhotoQt jumps to the last image). Disabling this option makes PhotoQt stop at the first/last image (i.e. selecting &apos;Next&apos;/&apos;Previous&apos; will have no effect in these two special cases).</source>
        <translation>Όταν ανοίγετε την τελευταία εικόνα σε έναν φάκελο και επιλέξετε «Επόμενο», το PhotoQt αυτομάτως μεταπηδά στην πρώτη εικόνα (και αντιστρόφως: αν επιλέξετε «Προηγούμενο» ενώ έχετε ανοίξει την πρώτη εικόνα, το PhotoQt μεταπηδά στην τελευταία εικόνα). Αν απενεργοποιήσετε αυτήν την επιλογή, το PhotoQt θα σταματά στην πρώτη/τελευταία εικόνα. (πχ επιλέγοντας «Επόμενο/Προηγούμενο» δεν θα έχει καμιά επίδραση σε αυτές τις ειδικές περιπτώσεις). </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="494"/>
        <source>Loop through folder</source>
        <translation>Επανάληψη στον φάκελο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Smooth Transition</source>
        <translation>Ομαλή μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced by the new image.</source>
        <translation>Η μετάβαση μεταξύ των εικόνων μπορεί να γίνεται με ομαλό τρόπο, η νέα εικόνα μπορεί να αντικαταστήσει την παλιά με ομαλό σβήσιμο. «Καμιά μετάβαση» σημαίνει, ότι η προηγούμενη εικόνα αντικαθίσταται απλώς με από τη νέα.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="526"/>
        <source>No Transition</source>
        <translation>Καμιά μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="546"/>
        <source>Long Transition</source>
        <translation>Αργή μετάβαση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Menu Sensitivity</source>
        <translation>Ευαισθησία μενού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Here you can adjust the sensitivity of the drop-down menu. The menu opens when your mouse cursor gets close to the right side of the upper edge. Here you can adjust how close you need to get for it to open.</source>
        <translation>Εδώ μπορείτε να καθορίσετε την ευαισθησία του αναπτυσσόμενου μενού. Το μενού ανοίγει με το πέρασμα του δρομέα κοντά στη δεξιά πλευρά της κορυφής. Εδώ μπορείτε να ρυθμίσετε το πόσο κοντά θα πρέπει να πλησιάζετε για να το ανοίξετε.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="583"/>
        <source>Low Sensitivity</source>
        <translation>Χαμηλή ευαισθησία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="603"/>
        <source>High Sensitivity</source>
        <translation>Υψηλή ευαισθησία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation>Ευαισθησία τροχού ποντικού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Here you can adjust the sensitivity of the mouse wheel. For example, if you have set the mouse wheel up/down for switching back and forth between images, then a lower sensitivity means that you will have to scroll further for triggering a shortcut. Per default it is set to the highest sensitivity, i.e. every single wheel movement is evaluated.</source>
        <translation>Εδώ μπορείτε να ρυθμίσετε την ευαισθησία του τροχού του ποντικιού. Για παράδειγμα, αν έχετε ορίσει το πάνω/κάτω του τροχού για την εναλλαγή πίσω και εμπρός μεταξύ των εικόνων, τότε μια μικρότερη ευαισθησία σημαίνει ότι θα πρέπει να κυλήσετε περισσότερο για την ενεργοποίηση μιας συντόμευσης. Εξ ορισμού είναι ορισμένο στην υψηλότερης ευαισθησίας, πχ η κάθε μια από τις μετακινήσεις του τροχού λαμβάνεται υπόψιν.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="640"/>
        <source>Very sensitive</source>
        <translation>Πολύ ευαίσθητος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="660"/>
        <source>Not at all sensitive</source>
        <translation>Καθόλου ευαίσθητο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>Remember per session</source>
        <translation>Απομνημόνευση ανά συνεδρία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>If you would like PhotoQt to remember the rotation/flipping and/or zoom level per session (not permanent), then you can enable it here. If not set, then every time a new image is displayed, it is displayed neither zoomed nor rotated nor flipped (one could say, it is displayed &apos;normal&apos;).</source>
        <translation>Αν επιθυμείτε το PhotoQt να απομνημονεύει την περιστροφή/αναστροφή και/ή το επίπεδο εστίασης ανά συνεδρία (όχι στο διηνεκές), τότε μπορείτε να το ενεργοποιήσετε εδώ. Αν δεν οριστεί, οι εικόνες εμφανίζονται ως έχουν.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="697"/>
        <source>Remember Rotation/Flip</source>
        <translation>Απομνημόνευση της περιστροφής/αναστροφής</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="702"/>
        <source>Remember Zoom Level</source>
        <translation>Απομνημόνευση του επιπέδου εστίασης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation and Window Geometry</source>
        <translation>Εφέ και γεωμετρία παραθύρου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="748"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation of fade-in widgets (like, e.g., Settings or About Widget)</source>
        <translation>Εφέ της εξομαλισμένης εισαγωγής (όπως για παράδειγμα τα συστατικά Ρυθμίσεις ή Σχετικά)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation>Αποθήκευση και ανάκτηση της γεωμετρίας του παραθύρου: Εγκαταλείποντας το PhotoQt, αποθηκεύει το μέγεθος και τη θέση του παραθύρου και μπορεί να την ανακτήσει στην επόμενη φορά. </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="738"/>
        <source>Animate all fade-in elements</source>
        <translation>Εφέ εξομάλυνσης εισαγωγής για όλα τα αντικείμενα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="743"/>
        <source>Save and restore window geometry</source>
        <translation>Αποθήκευση και ανάκτηση της γεωμετρίας του παραθύρου</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelBasic</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Βασικές ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Sort Images</source>
        <translation>Ταξινόμηση εικόνων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Here you can adjust, how the images in a folder are supposed to be sorted. You can sort them by Filename, Natural Name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), File Size, and Date. Also, you can reverse the sorting order from ascending to descending if wanted.</source>
        <translation>Εδώ μπορείτε να διαμορφώσετε τον τρόπο ταξινόμησης των εικόνων σε ένα φάκελο. Μπορείτε να τις ταξινομήσετε ανά όνομα αρχείου, φυσικό όνομα (πχ αρχείο10.jpg έρχεται μετά από το αρχείο9.jpg και όχι μετά από το αρχείο1.jpg), μέγεθος αρχείου, και ημερομηνία. Επίσης έχετε τη δυνατότητα να επιλέξετε μεταξύ αύξουσας και φθίνουσας ταξινόμησης.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Hint: You can also change this setting very quickly from the &apos;Quick Settings&apos; window, hidden behind the right screen edge.</source>
        <translation>Υπόδειξη: Μπορείτε επίσης να αλλάξετε αυτήν τη ρύθμιση στα γρήγορα από το παράθυρο «Γρήγορες ρυθμίσεις», καταχωνιασμένο πίσω από το δεξί άκρο της οθόνης.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="90"/>
        <source>Sort by:</source>
        <translation>Ταξινόμηση κατά:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Name</source>
        <translation>Όνομα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Natural Name</source>
        <translation>Φυσικό όνομα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Filesize</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="105"/>
        <source>Ascending</source>
        <translation>Αύξουσα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="113"/>
        <source>Descending</source>
        <translation>Φθίνουσα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>Window Mode</source>
        <translation>Λειτουργία παραθύρου </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>PhotoQt is designed with the space of a fullscreen app in mind. That&apos;s why it by default runs as fullscreen. However, some might prefer to have it as a normal window, e.g. so that they can see the panel.</source>
        <translation>Το PhotoQt έχει σχεδιαστεί με την ιδέα να λειτουργεί ως μια εφαρμογή πλήρους οθόνης. Για αυτόν το λόγο και τρέχει σε πλήρης οθόνη εξ ορισμού. Ωστόσο, μερικοί μπορεί να προτιμούν τη λειτουργία σε κανονικό παράθυρο, πχ για να μπορούν να βλέπουν τον πίνακα εφαρμογών.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="151"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>Εκτέλεση του PhotoQt σε λειτουργία παραθύρου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="159"/>
        <source>Show Window Decoration</source>
        <translation>Εμφάνιση της διακόσμησης του παραθύρου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>Hide to Tray Icon</source>
        <translation>Απόκρυψη ως εικονίδιο στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>When started PhotoQt creates a tray icon in the system tray. If desired, you can set PhotoQt to minimise to the tray instead of quitting. This causes PhotoQt to be almost instantaneously available when an image is opened.&lt;br&gt;It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>No tray icon</source>
        <translation>Χωρίς εικονίδιο στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Hide to tray icon</source>
        <translation>Απόκρυψη ως εικονίδιο στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation>Εμφάνιση του εικονιδίου συστήματος αλλά να μην γίνει απόκρυψη σε αυτό</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>Closing &apos;X&apos; (top right)</source>
        <translation>«X» εξόδου (πάνω δεξιά)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>There are two looks for the closing &apos;x&apos; at the top right: a normal &apos;x&apos;, or a slightly more fancy &apos;x&apos;. Here you can switch back and forth between both of them, and also change their size. If you prefer not to have a closing &apos;x&apos; at all, see below for an option to hide it.</source>
        <translation>Υπάρχουν δυο μορφές για το «X» εξόδου στην κορυφή δεξιά: ένα τυπικό «Χ» ή ένα ελαφρώς πιο εντυπωσιακό «X». Από εδώ μπορείτε να επιλέξετε το ένα ή το άλλο καθώς και να αλλάξετε το μέγεθός τους. Αν προτιμάτε να μην έχετε καθόλου το «X» εξόδου, δείτε παρακάτω μια επιλογή απόκρυψής του.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="216"/>
        <source>Normal Look</source>
        <translation>Τυπική εμφάνιση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="223"/>
        <source>Fancy Look</source>
        <translation>Εντυπωσιακή εμφάνιση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="249"/>
        <source>Small Size</source>
        <translation>Μικρό μέγεθος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="264"/>
        <source>Large Size</source>
        <translation>Μεγάλο μέγεθος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>Fit Image in Window</source>
        <translation>Προσαρμογή της εικόνας στο παράθυρο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can zoom those images to make them fir into the window. However, keep in mind, that such images will look pixelated to a certain degree (depending on each image).</source>
        <translation>Αν οι διαστάσεις της εικόνας είναι μικρότερες από τις διαστάσεις της οθόνης, το PhotoQt, μπορεί να εστιάσει αυτές τις εικόνες και να τις προσαρμόσει στο παράθυρο. Ωστόσο έχετε υπόψη ότι τέτοιες εικόνες θα φαίνονται σαν ψηφιδοποιημένες ως ένα βαθμό (εξαρτάται την κάθε εικόνα).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="287"/>
        <source>Fit Images in Window</source>
        <translation>Προσαρμογή των εικόνων στο παράθυρο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Hide Quickinfo (Text Labels)</source>
        <translation>Απόκρυψη Γρήγορων πληροφοριών (Ετικέτες κειμένου)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Here you can hide the text labels shown in the main area: The Counter in the top left corner, the file path/name following the counter, and the &quot;X&quot; displayed in the top right corner. The labels can also be hidden by simply right-clicking on them and selecting &quot;Hide&quot;.</source>
        <translation>Εδώ μπορείτε να αποκρύψετε τις ετικέτες κειμένου που εμφανίζονται στην κύρια περιοχή: Τον μετρητή στην επάνω αριστερή γωνία, τη διαδρομή/όνομα του αρχείου μετά τον μετρητή, και το «X» που εμφανίζεται στην επάνω δεξιά γωνία. Μπορείτε επίσης να αποκρύψετε τις ετικέτες με ένα απλό δεξί κλικ σε αυτές και επιλέγοντας «Απόκρυψη».</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="321"/>
        <source>Hide Counter</source>
        <translation>Απόκρυψη μετρητή</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="326"/>
        <source>Hide Filepath (Shows only file name)</source>
        <translation>Απόκρυψη της διαδρομής του αρχείου (Εμφάνιση μόνο του ονόματος)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="331"/>
        <source>Hide Filename (Including file path)</source>
        <translation>Απόκρυψη του ονόματος αρχείου (συμπεριλαμβανομένης της διαδρομής)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="336"/>
        <source>Hide &quot;X&quot; (Closing)</source>
        <translation>Απόκρυψη του «X» (Έξοδος)</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="55"/>
        <source>Other Settings</source>
        <translation>Λοιπές ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>Choose Language</source>
        <translation>Επιλογή γλώσσας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>Υπάρχει ένας αρκετά καλός αριθμός διαθέσιμων μεταφράσεων. Ευχαριστίες σε όλους που βρήκαν τον χρόνο να μεταφράσουν το PhotoQt!</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>Quick Settings</source>
        <translation>Γρήγορες ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>The &apos;Quick Settings&apos; is a widget hidden on the right side of the screen. When you move the cursor there, it shows up, and you can adjust a few simple settings on the spot without having to go through this settings dialog. Of course, only a small subset of settings is available (the ones needed most often). Here you can disable the dialog so that it doesn&apos;t show on mouse movement anymore.</source>
        <translation>Οι «Γρήγορες ρυθμίσεις» είναι ένα συστατικό στη δεξιά πλευρά της οθόνης. Εμφανίζεται όταν μετακινείτε τον δρομέα σε αυτό το σημείο, και σας δίνει τη δυνατότητα να πραγματοποιήσετε μερικές απλές ρυθμίσεις σε πραγματικό χρόνο χωρίς να χρειάζεται να ανοίξετε τον διάλογο ρυθμίσεων. Φυσικά, μόνο ένα μικρό υποσύνολο ρυθμίσεων είναι διαθέσιμο (αυτό που χρειάζονται περισσότερο). Εδώ μπορείτε να απενεργοποιήσετε τον διάλογο ώστε να μην εμφανίζεται με το πέρασμα του ποντικιού.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="129"/>
        <source>Show &apos;Quick Settings&apos; on mouse hovering</source>
        <translation>Εμφάνιση των «Γρήγορων ρυθμίσεων» με το πέρασμα του ποντικιού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Adjust Context Menu</source>
        <translation>Προσαρμογή του σχετικού μενού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Here you can adjust the context menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>Εδώ μπορείτε να προσαρμόσετε το σχετικό μενού. Μπορείτε να κάνετε μεταφορά και απόθεση των καταχωρήσεων, να τις επεξεργαστείτε, να προσθέσετε μια νέα ή να αφαιρέσετε μια υπάρχουσα.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="183"/>
        <source>Executable</source>
        <translation>Εκτελέσιμο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="199"/>
        <source>Menu Text</source>
        <translation>Κείμενο μενού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="223"/>
        <source>Add new context menu entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="231"/>
        <source>(Re-)set automatically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabOtherContext</name>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="116"/>
        <source>Click here to drag</source>
        <translation>Κλικ εδώ για σύρσιμο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="186"/>
        <source>quit</source>
        <translation>έξοδος</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="54"/>
        <source>Shortcuts</source>
        <translation>Συντομεύσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="73"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available function you can either double click on the tile or click the &quot;+&quot; button. This automatically opens another widget where you can set a key combination.</source>
        <translation>Εδώ μπορείτε να προσαρμόσετε τις συντομεύσεις, να προσθέσετε νέες ή να αφαιρέσετε μια υπάρχουσα, η να τροποποιήσετε έναν συνδυασμό πλήκτρων. Οι συντομεύσεις διαχωρίζονται σε 4 διαφορετικές κατηγορίες για εσωτερικές εντολές συν μια κατηγορία εξωτερικών εντολών. Τα πλαίσια στη δεξιά πλευρά περιέχουν όλες τις πιθανές εντολές. Για να προσθέσετε μια συντόμευση για τη διαθέσιμη λειτουργία μπορείτε είτε να κάνετε διπλό κλικ στο πλακίδιο ή με κλικ στο κουμπί «+». Με αυτόν τον τρόπο θα ανοίει ένα καινούργιο γραφικό συστατικό από όπου μπορείτε μα ορίσετε έναν συνδυασμό πλήκτρων.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="89"/>
        <source>Set default shortcuts</source>
        <translation>Επαναφορά των προκαθορισμένων συντομεύσεων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="96"/>
        <source>Navigation</source>
        <translation>Πλοήγηση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Open New File</source>
        <translation>Άνοιγμα νέου αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Filter Images in Folder</source>
        <translation>Φιλτράρισμα εικόνων στον φάκελο </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Next Image</source>
        <translation>Επόμενη εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Previous Image</source>
        <translation>Προηγούμενη εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to first Image</source>
        <translation>Μετάβαση στην πρώτη εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to last Image</source>
        <translation>Μετάβαση στην τελευταία εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Hide to System Tray</source>
        <translation>Απόκρυψη  στο πλαίσιο συστήματος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Quit PhotoQt</source>
        <translation>Έξοδος του PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="104"/>
        <source>Image</source>
        <translation>Εικόνα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom In</source>
        <translation>Μεγέθυνση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom Out</source>
        <translation>Σμίκρυνση</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom to Actual Size</source>
        <translation>Εστίαση στο πραγματικό μέγεθος</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Zoom</source>
        <translation>Επαναφορά εστίασης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Right</source>
        <translation>Περιστροφή δεξιά</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Left</source>
        <translation>Περιστροφή αριστερά</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Rotation</source>
        <translation>Επαναφορά περιστροφής</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Horizontally</source>
        <translation>Αναστροφή οριζόντια</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Vertically</source>
        <translation>Αναστροφή κατακόρυφα</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Scale Image</source>
        <translation>Κλιμάκωση εικόνας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="112"/>
        <source>File</source>
        <translation>Αρχείο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Rename File</source>
        <translation>Μετονομασία αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Delete File</source>
        <translation>Διαγραφή αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Copy File to a New Location</source>
        <translation>Αντιγραφή του αρχείου σε μια νέα τοποθεσία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Move File to a New Location</source>
        <translation>Μετακίνηση του αρχείου σε μια νέα τοποθεσία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="120"/>
        <source>Other</source>
        <translation>Άλλο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Interrupt Thumbnail Creation</source>
        <translation>Διακοπή δημιουργίας των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Reload Thumbnails</source>
        <translation>Επαναφόρτωση των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Hide/Show Exif Info</source>
        <translation>Εμφάνιση/Απόκρυψη πληροφοριών Exif</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Context Menu</source>
        <translation>Εμφάνιση σχετικού μενού</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Settings</source>
        <translation>Εμφάνιση ρυθμίσεων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow</source>
        <translation>Εκκίνηση παρουσίασης διαφάνειας</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Εκκίνηση παρουσίασης διαφάνειας (Γρήγορη εκκίνηση)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>About PhotoQt</source>
        <translation>Σχετικά με το PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Set as Wallpaper</source>
        <translation>Ορισμός ως ταπετσαρία</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="128"/>
        <source>Extern</source>
        <translation>Εξωτερικό</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="132"/>
        <source>EXTERN</source>
        <extracomment>Is the shortcut tile text for EXTERNal shortcuts</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabShortcutsCategories</name>
    <message>
        <location filename="../qml/settings/TabShortcutsCategories.qml" line="32"/>
        <source>Category:</source>
        <translation>Κατηγορία:</translation>
    </message>
</context>
<context>
    <name>TabShortcutsTilesAvail</name>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="98"/>
        <source>key</source>
        <extracomment>tile text for KEY shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>πλήκτρο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="154"/>
        <source>mouse</source>
        <extracomment>tile text for MOUSE shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>ποντίκι</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsAdvanced</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="52"/>
        <source>Advanced Settings</source>
        <translation>Προηγμένες ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Change Thumbnail Position</source>
        <translation>Αλλαγή της θέσης των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Per default the bar with the thumbnails is shown at the lower edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge, so that&apos;s what can be changed here.</source>
        <translation>Εξ ορισμού, η γραμμή με τις εικόνες επισκόπησης εμφανίζεται στο κάτω άκρο. Ωστόσο, μερικοί μπορεί να βρίσκουν ότι είναι καλύτερα οι εικόνες επισκόπησης να βρίσκονται στο πάνω άκρο, και αυτό είναι που μπορείτε να αλλάξετε εδώ.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="86"/>
        <source>Show at lower edge</source>
        <translation>Εμφάνιση στο κάτω άκρο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="93"/>
        <source>Show at upper edge</source>
        <translation>Εμφάνιση στο πάνω άκρο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>Filename? Dimension? Or both?</source>
        <translation>Όνομα αρχείου; Διαστάσεις; ή και τα δυο;</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="132"/>
        <source>Write Filename</source>
        <translation>Εμφάνιση του ονόματος αρχείου</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>Use file-name-only Thumbnails</source>
        <translation>Χρήση επισκοπήσεων με όνομα αρχείου μόνο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a file-name-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>Αν δεν επιθυμείτε το PhotoQt να φορτώνει πάντα την τρέχουσα εικόνα επισκόπησης στο παρασκήνιο, αλλά θέλετε να υπάρχει πάντα κάτι για βοήθεια στην πλοήγηση, τότε μπορείτε να ορίσετε μια επισκόπηση με το όνομα του αρχείου μόνο, πχ το PhotoQt δεν θα φορτώσει τις επισκοπήσεις αλλά θα εμφανίσει το όνομα του αρχείου στο πλαίσιο. Μπορείτε επίσης να προσαρμόσετε το μέγεθος του συγκεκριμένου κειμένου.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>When thumbnails are displayed at the top/bottom, PhotoQt usually writes the filename on them (if not disabled). You can also use the slider below to adjust the font size.</source>
        <translation>Όταν οι εικόνες επισκόπησης προβάλλονται στην κορυφή/πάτος, το PhotoQt συνήθως γράφει σε αυτές το όνομα αρχείου (αν δεν είναι απενεργοποιημένο). Μπορείτε να ρυθμίσετε το μέγεθος της γραμματοσειράς με την παρακάτω κύλιση.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="213"/>
        <source>Use filename-only thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>Disable Thumbnails</source>
        <translation>Απενεργοποίηση επισκοπήσεων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This option can also be toggled remotely via command line (run &apos;photoqt --help&apos; for more information on that). This might increase the speed of PhotoQt a good bit, however, navigating through a folder might be a little harder without thumbnails.</source>
        <translation>Αν απλά δεν επιθυμείτε καμιά επισκόπηση, μπορείτε να την απενεργοποιήσετε εντελώς από εδώ. Αυτή η επιλογή να εναλλαχτεί εξωτερικά μέσω της γραμμής εντολών (εκτελέστε &apos;photoqt --help&apos; για περισσότερες πληροφορίες σχετικά). Αυτό μπορεί να αυξήσει την ταχύτητα του PhotoQt αρκετά, ωστόσο, η πλοήγηση σε έναν φάκελο μπορεί να είναι λίγο δύσκολο χωρίς επισκοπήσεις.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="287"/>
        <source>Disable Thumbnails altogether</source>
        <translation>Καθολική απενεργοποίηση των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnail Cache</source>
        <translation>Λανθάνουσα μνήμη εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnails can be cached in two different ways:&lt;br&gt;1) File Caching (following the freedesktop.org standard) or&lt;br&gt;2) Database Caching (better performance and management, default option).</source>
        <translation>Οι εικόνες επισκόπησης μπορούν να αποθηκευτούν προσωρινά με δυο διαφορετικούς τρόπους:&lt;br&gt;1) Λανθάνουσα μνήμη σε αρχείο (σύμφωνα με τα πρότυπα του freedesktop.org) ή&lt;br&gt;2) Λανθάνουσα μνήμη σε βάση δεδομένων (καλύτερες επιδόσεις, η εξ ορισμού επιλογή).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <translation>Και οι δυο τρόποι έχουν τα μειονεκτήματά και τα πλεονεκτήματά τους:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file. However, it&apos;s not possible to check for obsolete thumbnails (thus this may lead to many unneeded thumbnail files).</source>
        <translation>Η λανθάνουσα μνήμη σε αρχείο πραγματοποιείται σύμφωνα με τα πρότυπα του freedesktop.org επιτρέποντας την κοινή χρήση των ίδιων εικόνων επισκόπησης από άλλες εφαρμογές. Ωστόσο, δεν είναι δυνατός ο έλεγχος για ξεπερασμένες εικόνες επισκόπησης (μπορεί να έχει ως αποτέλεσμα πολλές μη χρησιμοποιούμενες εικόνες επισκόπησης).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it brings a slightly better performance, and it allows a better handling of existing thumbnails (e.g. deleting obsolete thumbnails).</source>
        <translation>Η λανθάνουσα μνήμη σε βάση δεδομένων δεν έχει το πλεονέκτημα της κοινής χρήσης των εικόνων επισκόπησης με άλλες εφαρμογές (επομένως κάθε εικόνα επισκόπησης θα πρέπει να δημιουργηθεί εκ νέου για το PhotoQt), αλλά παρέχει ελαφρώς καλύτερες επιδόσεις, και επιτρέπει την καλύτερη διαχείριση των υπαρχόντων επισκοπήσεων (πχ τη διαγραφή των ξεπερασμένων εικόνων επισκόπησης).</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>PhotoQt works with either option, though the second way is set as default.</source>
        <translation>Το PhotoQt μπορεί να δουλέψει και με τους δυο τρόπους, αν και ο δεύτερος τρόπος είναι και ο προκαθορισμένος.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, that does affect the performance and usability of PhotoQt, since thumbnails have to be newly re-created every time they are needed.</source>
        <translation>Παρόλο που προτρέπουμε τη χρήση ενός εκ των δύο μεθόδων, η λανθάνουσα μνήμη μπορεί να απενεργοποιηθεί εντελώς. Αυτό όμως θα επηρεάσει τις επιδόσεις και τη χρηστικότητα του PhotoQt, λόγω του ότι οι εικόνες επισκόπησης θα πρέπει να δημιουργούνται εκ νέου όταν σε κάθε αίτηση.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="327"/>
        <source>Enable Thumbnail Cache</source>
        <translation>Ενεργοποίηση της λανθάνουσας μνήμης των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="348"/>
        <source>File Caching</source>
        <translation>Λανθάνουσα μνήμη σε αρχείο</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="354"/>
        <source>Database Caching</source>
        <translation>Λανθάνουσα μνήμη σε βάση δεδομένων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="379"/>
        <source>Current database filesize:</source>
        <translation>Τρέχον μέγεθος αρχείου της βάσης δεδομένων:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="405"/>
        <source>Entries in database:</source>
        <translation>Καταχωρήσεις στη βάση δεδομένων:</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="426"/>
        <source>CLEAN UP database</source>
        <translation>ΚΑΘΑΡΙΣΜΟΣ της βάσης δεδομένων</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="438"/>
        <source>ERASE database</source>
        <translation>ΔΙΑΓΡΑΦΗ της βάσης δεδομένων</translation>
    </message>
</context>
<context>
    <name>TabThumbnailsBasic</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation>Βασικές ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Thumbnail Size</source>
        <translation>Μέγεθος εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but with different screen resolutions it might be nice to have them larger/smaller.</source>
        <translation>Εδώ μπορείτε να προσαρμόσετε το μέγεθος των εικόνων επισκόπησης. Μπορείτε να καθορίσετε οποιαδήποτε τιμή μεταξύ 20 και 256 εικονοστοιχεία (pixel). Η προκαθορισμένη τιμή είναι 80 εικονοστοιχεία, αλλά με διαφορετικές αναλύσεις οθόνης μπορεί να είναι καλύτερα να το ορίσετε μεγαλύτερο/μικρότερο.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>Spacing Between Thumbnail Images</source>
        <translation>Διάστημα μεταξύ εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them, however exactly that can be changed here.</source>
        <translation>Οι εικόνες επισκόπησης εμφανίζονται σε μια γραμμή στο πάνω ή κάτω άκρο (ανάλογα με το σύστημά σας). Εμφανίζονται σε σειρά η μια δίπλα στην άλλη. Εξ ορισμού, δεν υπάρχει κενό διάστημα μεταξύ τους, ωστόσο μπορείτε να το αλλάξετε εδώ.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>Lift-up of Thumbnail Images on Hovering</source>
        <translation>Ανασήκωμα των εικόνων επισκόπησης περνώντας το ποντίκι</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels (default 10). Here you can increase/decrease this value according to your personal preference.</source>
        <translation>Όταν το ποντίκι περνά πάνω από μια εικόνα επισκόπησης, αυτή ανασηκώνεται μερικά pixel(10 εξ ορισμού). Εδώ μπορείτε να αυξήσετε/μειώσετε αυτήν την τιμή ανάλογα με τις προσωπικές σας προτιμήσεις.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Keep Thumbnails Visible</source>
        <translation>Διατήρηση των εικόνων επισκόπησης ορατές</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Per default the Thumbnails slide out over the edge of the screen. Here you can force them to stay visible. The big image is shrunk to fit into the empty space. Note, that the thumbnails will be hidden (and only shown on mouse hovering) once you zoomed the image in/out. Resetting the zoom restores the original visibility of the thumbnails.</source>
        <translation>Εξ ορισμού οι εικόνες επισκόπησης «γλιστρούν» έξω από την άκρη της οθόνης. Εδώ μπορείτε να τις εξαναγκάσετε να παραμείνουν ορατές. Η μεγάλη εικόνα συρρικνώνεται ώστε να καταλάβει τον ελεύθερο χώρο. Σημειώστε ότι οι εικόνες επισκόπησης θα είναι κρυφές (και θα εμφανίζονται μόνο κατά το πέρασμα του ποντικιού) κατά τη μεγέθυνση/μείωση της εικόνας. Η επαναφορά της εστίασης επαναφέρει την αρχική ρύθμιση ορατότητας των εικόνων επισκόπησης.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="259"/>
        <source>Keep Thumnails Visible</source>
        <translation>Διατήρηση των εικόνων επισκόπησης ορατές</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic Thumbnail Creation</source>
        <translation>Δυναμική δημιουργία των εικόνων επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic thumbnail creation means, that PhotoQt only sets up those thumbnail images that are actually needed, i.e. it stops once it reaches the end of the visible area and sits idle until you scroll left/right.</source>
        <translation>Δυναμική δημιουργία εικόνων επισκόπησης σημαίνει, ότι το PhotoQt ορίζει τις εικόνες που πραγματικά χρειάζονται, πχ: σταματά όταν φτάσει στο τέλος της ορατής περιοχής και παραμένει μέχρι να κάνετε μια κύλιση αριστερά/δεξιά.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Smart thumbnails are similar in nature. However, they make use of the fast, that once a thumbnail has been created, it can be loaded very quickly and efficiently. It also first loads all of the currently visible thumbnails, but it doesn&apos;t stop there: Any thumbnails (even if invisible at the moment) that once have been created are loaded. This is a nice compromise between efficiency and usability.</source>
        <translation>Οι έξυπνες εικόνες επισκόπησης χρησιμοποιούν την ταχύτητα της δημιουργίας εικόνων σε πραγματικό χρόνο, και οι υπόλοιπες εικόνες φορτώνονται στο παρασκήνιο (μη ορατές ακόμα).
Αυτός είναι ένας καλός συμβιβασμός χρηστικότητας και αποτελεσματικότητας.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Enabling either the smart or dynamic option is recommended, as it increases the performance of PhotoQt significantly, while preserving the usability.</source>
        <translation>Συνιστάται η ενεργοποίηση της έξυπνης ή της δυναμικής επιλογής, λόγω της δραματικής αύξησης των επιδόσεων του PhotoQt, διατηρώντας τη χρηστικότητα.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="295"/>
        <source>Normal Thumbnails</source>
        <translation>Τυπικές εικόνες επισκόπησης
</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="301"/>
        <source>Dynamic Thumbnails</source>
        <translation>Δυναμικές εικόνες επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="307"/>
        <source>Smart Thumbnail</source>
        <translation>Έξυπνες Εικόνα επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>Always center on Active Thumbnail</source>
        <translation>Κεντράρισμα πάντα της ενεργούς εικόνας επισκόπησης</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>If this option is set, then the active thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation>Αν αυτή η επιλογή είναι ενεργή, τότε η ενεργή εικόνα επισκόπησης (πχ η εικόνα επισκόπησης της τρέχουσας προβαλλόμενης εικόνας) θα διατηρείται πάντα στο κέντρο της γραμμής των εικόνων επισκόπησης (στα όρια του δυνατού). Αν αυτή η επιλογή δεν είναι ενεργοποιημένη, τότε η ενεργή εικόνα επισκόπησης απλούστατα θα διατηρείται ορατή, αλλά όχι κατ&apos; ανάγκη στο κέντρο.</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="333"/>
        <source>Center on Active Thumbnails</source>
        <translation>Κεντράρισμα των ενεργών εικόνων επισκόπησης</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="799"/>
        <source>Okay, do it!</source>
        <translation>Εντάξει, κάνε το !</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="804"/>
        <source>Nooo, don&apos;t!</source>
        <translation>Όχι, μη!</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="106"/>
        <source>Window Manager</source>
        <translation>Διαχειριστής παραθύρων</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="260"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="401"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <translation>Υπάρχουν μερικές επιλογές εικόνων που μπορούν να ορισθούν για την εικόνα ταπετσαρίας.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="86"/>
        <source>Set as Wallpaper:</source>
        <translation>Ορισμός ως ταπετσαρία:</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="114"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation>Το PhotoQt προσπαθεί να ανιχνεύσει τον διαχειριστή παραθύρων σας ανάλογα με τις ρυθμισμένες μεταβλητές του συστήματός σας. Αν ο εντοπισμός είναι εσφαλμένος, μπορείτε να αλλάξετε τον διαχειριστή παραθύρων από εδώ χειροκίνητα.</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="192"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="219"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="250"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="337"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="350"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="505"/>
        <source>The wallpaper can be set to either of the available monitors (or any combination).</source>
        <translation>Η ταπετσαρία μπορεί να οριστεί σε κάθε μια από τις διαθέσιμες οθόνες (ή έναν συνδυασμό).</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="366"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="521"/>
        <source>Screen #</source>
        <translation>Οθόνη #</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="483"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console &gt; Add-ons &gt; Modules &gt; System.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="494"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="556"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="576"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="578"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="579"/>
        <source>Workspace #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="636"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="647"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="658"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="669"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.&lt;br&gt;This is intended particularly for window managers that don&apos;t natively support wallpapers (e.g., like Openbox).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="685"/>
        <source>Use &apos;feh&apos;</source>
        <extracomment>feh is an application, do not translate</extracomment>
        <translation>Χρήση &apos;feh&apos;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="692"/>
        <source>Use &apos;nitrogen&apos;</source>
        <extracomment>nitrogen is an application, do not translate</extracomment>
        <translation>Χρήση &apos;nitrogen&apos;</translation>
    </message>
</context>
</TS>
