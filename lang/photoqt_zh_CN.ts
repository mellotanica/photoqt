<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/fadein/About.qml" line="113"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>PhotoQt 是一個簡潔的圖片查看器，設計宗旨是：好看，高度可定制，易用和快捷。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="115"/>
        <source>With PhotoQt I try to be different than other image viewers (after all, there are plenty of good image viewers already out there). Its interface is kept very simple, yet there is an abundance of settings to customize the look and feel to make PhotoQt YOUR image viewer.</source>
        <translation>PhotoQt 不同于其他图片查看器（毕竟已经有太多优秀的图片查看器了）。它的界面非常简洁，不过有很多设置选项供您制定属于自己的界面。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="117"/>
        <source>I&apos;m not a trained programmer. I&apos;m a simple Maths student that loves doing stuff like this. Most of my programming knowledge I taught myself over the past 10-ish years, and it has been developing a lot since I started PhotoQt. During my studies in university I learned a lot about the basics of programming that I was missing. And simply working on PhotoQt gave me a lot of invaluable experience. So the code of PhotoQt might in places not quite be done in the best of ways, but I think it&apos;s getting better and better with each release.</source>
        <translation>我不是一个专业的程序员。我只是一个喜欢干这些事的数学 c&apos;vb系学生。很多编程知识都是我10年多自学的，从 PhotoQt 开发至今，已经拥有了很多东西。在大学的学习过程中，我补缺了很多编程基础知识。将所学知识都用在 PhotoQt 上给我带来了很多宝贵的经验。虽然 PhotoQt 的代码还不够完美，实现方法也不够好，但我想每个版本都在不断改进。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="121"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>不要忘了訪問我們的網站：</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="123"/>
        <source>If you find a bug or if you have a question or suggestion, tell me. I&apos;m open to any feedback I get :)</source>
        <translation>如果你發現 Bug，或有什么問題或建議，請告訴我。任何反饋我都會樂意接收的  :)</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="174"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don&apos;t forget to add the %1 in your translation!!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="119"/>
        <source>I heard a number of times people saying, that PhotoQt is a &apos;copy&apos; of Picasa&apos;s image viewer. Well, it&apos;s not. In fact, I myself have never used Picasa. I have seen it in use though by others, and I can&apos;t deny that it influenced the basic design idea a little. But I&apos;m not trying to do something &apos;like Picasa&apos;. I try to do my own thing, and to do it as good as I can.</source>
        <translation>我经常听到别人说 PhotoQt 是 Picasa 的图片查看器的 “克隆” 。我想说，不是！事实上我都没用过 Picasa。曾看到别人使用它，我不否认说 PhotoQt 没受其设计思想的影响。但是我尽量的不去做的“像 Picasa&quot;。我尝试做我自己的东西， 并尽力做到最好。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="158"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation>感谢所有为 PhotoQt 贡献过的人，且/或将 PhotoQt 翻译成其他语言的人！你们最棒了 !</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>website:</source>
        <translation>網站：</translation>
    </message>
    <message>
        <location filename="../qml/fadein/About.qml" line="68"/>
        <source>Licensed under GPLv2 or later, without any guarantee</source>
        <translation>使用 GPLv2 及后續版本許可，無任何保證</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="52"/>
        <source>Move:</source>
        <extracomment>as in: &quot;Move file...&quot;</extracomment>
        <translation>移动：</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="62"/>
        <source>Previous</source>
        <extracomment>Go to previous file</extracomment>
        <translation>上一个</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="68"/>
        <source>Next</source>
        <extracomment>Go to next file</extracomment>
        <translation>下一个</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="99"/>
        <source>Rotate:</source>
        <extracomment>As in: Rotate file</extracomment>
        <translation>旋转：</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="104"/>
        <source>Left</source>
        <extracomment>As in: rotate LEFT</extracomment>
        <translation>向左</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="111"/>
        <source>Right</source>
        <extracomment>As in: Rotate RIGHT</extracomment>
        <translation>向右</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="140"/>
        <source>Flip:</source>
        <extracomment>As in: Flip file</extracomment>
        <translation>翻转：</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="145"/>
        <source>Horizontal</source>
        <extracomment>As in: Flip file HORIZONTALLY</extracomment>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="152"/>
        <source>Vertical</source>
        <extracomment>As in: Flip file VERTICALLY</extracomment>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="180"/>
        <source>Zoom:</source>
        <extracomment>Zoom file</extracomment>
        <translation>缩放：</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="185"/>
        <source>In</source>
        <extracomment>As in: Zoom IN</extracomment>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="191"/>
        <source>Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="197"/>
        <source>Actual</source>
        <extracomment>As in: Zoom to ACTUAL size</extracomment>
        <translation>实际大小</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="203"/>
        <source>Reset</source>
        <extracomment>As in: Reset zoom</extracomment>
        <translation>重设</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="224"/>
        <source>Scale Image</source>
        <translation>缩放图片</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="234"/>
        <source>Open in default File Manager</source>
        <translation>使用默认文件管理器打开</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="266"/>
        <source>Rename File</source>
        <translation>重命名文件</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="276"/>
        <source>Delete File</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="301"/>
        <source>Copy File</source>
        <translation>复制文件</translation>
    </message>
    <message>
        <location filename="../qml/mainview/ContextMenu.qml" line="312"/>
        <source>Move File</source>
        <translation>移动文件</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="11"/>
        <source>Confirm me?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="12"/>
        <source>Do you really want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="13"/>
        <source>Yes, do it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="14"/>
        <source>No, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomConfirm.qml" line="110"/>
        <source>Don&apos;t ask again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomDetectShortcut</name>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="72"/>
        <source>Detect key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="90"/>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="127"/>
        <source>Press keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomDetectShortcut.qml" line="107"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>CustomExternalCommand</name>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="71"/>
        <source>External Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (with path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current file (without path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="86"/>
        <source>current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="122"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="135"/>
        <source>Save it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomExternalCommand.qml" line="159"/>
        <source>Select Executeable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomMouseShortcut</name>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="69"/>
        <source>Set Mouse Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="114"/>
        <source>Don&apos;t set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/CustomMouseShortcut.qml" line="127"/>
        <source>Set Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="68"/>
        <source>Delete File</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="98"/>
        <source>Do you really want to delete this file?</source>
        <translation>你真的想删除此文件？</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Move to Trash</source>
        <translation>移动到回收站</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="126"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="137"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="150"/>
        <source>Delete permanently</source>
        <translation>永久删除</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Delete.qml" line="170"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <location filename="../qml/mainview/Display.qml" line="481"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="507"/>
        <source>Open a file to begin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="558"/>
        <source>No results found...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="565"/>
        <source>Rotate Image?</source>
        <translation>旋转图片？</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>The Exif data of this image says, that this image is supposed to be rotated.</source>
        <translation>根据图片的 Exif 信息，此图片需要被旋转。</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="566"/>
        <source>Do you want to apply the rotation?</source>
        <translation>你想应用旋转？</translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="567"/>
        <source>Yes, do it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/Display.qml" line="568"/>
        <source>No, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="68"/>
        <source>Filter images in current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="83"/>
        <source>Enter here the term you want to search for. Seperate multiple terms by a space.</source>
        <translation>在此输入搜索关键词，关键词间请用空格隔开</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="90"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>如果你想过滤出指定的文件类型，请在关键词前面加 ‘.’</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="137"/>
        <source>Filter</source>
        <translation>过滤</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="145"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Filter.qml" line="160"/>
        <source>Remove Filter</source>
        <translation>移除过滤</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="10"/>
        <source>Edit with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getanddostuff/context.cpp" line="13"/>
        <source>Open in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="317"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="320"/>
        <source>Daylight</source>
        <translation>日光</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="323"/>
        <source>Fluorescent</source>
        <translation>荧光灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="326"/>
        <source>Tungsten (incandescent light)</source>
        <translation>钨丝灯(白炽灯)</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="329"/>
        <source>Flash</source>
        <translation>闪光灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="332"/>
        <source>Fine weather</source>
        <translation>晴天</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="335"/>
        <source>Cloudy Weather</source>
        <translation>多云</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="338"/>
        <source>Shade</source>
        <translation>昏暗</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="341"/>
        <source>Daylight fluorescent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="344"/>
        <source>Day white fluorescent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="347"/>
        <source>Cool white fluorescent</source>
        <translation>白色冷光荧光灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="350"/>
        <source>White fluorescent</source>
        <translation>白色荧光灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="353"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="356"/>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="359"/>
        <source>Standard light</source>
        <translation>标准光源</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="371"/>
        <source>D50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="374"/>
        <source>ISO studio tungsten</source>
        <translation>ISO 摄影室钨丝灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="377"/>
        <source>Other light source</source>
        <translation>其他光源</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="380"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="389"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired</extracomment>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn&apos;t fired</extracomment>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="393"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash</extracomment>
        <translation>无闪光</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="395"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>未检测到频闪灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>检测到频闪灯</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="399"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>强制闪光模式</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="401"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>自动模式</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>防红眼模式</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="405"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>回光检测开</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="407"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode</extracomment>
        <translation>回光检测关</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="454"/>
        <source>Invalid flash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="467"/>
        <source>Landscape</source>
        <translation>风景</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>Portrait</source>
        <translation>人像</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="473"/>
        <source>Night Scene</source>
        <translation>夜景</translation>
    </message>
    <message>
        <location filename="../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="25"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="26"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="27"/>
        <source>Set as Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="28"/>
        <source>Start Slideshow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="29"/>
        <source>Filter Images in Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="30"/>
        <source>Show/Hide Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="31"/>
        <source>About PhotoQt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="32"/>
        <source>Hide (System Tray)</source>
        <translation>隐藏 (系统托盘)</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="33"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MainMenu.qml" line="120"/>
        <source>Quickstart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="47"/>
        <source>Open image file</source>
        <translation>打開圖像文件</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="121"/>
        <location filename="../cplusplus/mainwindow.cpp" line="122"/>
        <location filename="../cplusplus/mainwindow.cpp" line="124"/>
        <source>Images</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="126"/>
        <source>All Files</source>
        <translation>所有文件</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="537"/>
        <source>Image Viewer</source>
        <translation>圖片瀏覽器</translation>
    </message>
    <message>
        <location filename="../cplusplus/mainwindow.cpp" line="542"/>
        <source>Hide/Show PhotoQt</source>
        <translation>隱藏/顯示 PhotoQt</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="44"/>
        <source>No File Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="62"/>
        <source>File Format Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="80"/>
        <source>Invalid File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="121"/>
        <source>Keep Open</source>
        <translation>保持打开</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="191"/>
        <source>Filesize</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="193"/>
        <location filename="../qml/slidein/MetaData.qml" line="196"/>
        <source>Dimensions</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="201"/>
        <source>Make</source>
        <translation>制造商</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="202"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="203"/>
        <source>Software</source>
        <translation>软件</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="205"/>
        <source>Time Photo was Taken</source>
        <translation>拍摄时间</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="206"/>
        <source>Exposure Time</source>
        <translation>曝光时间</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="207"/>
        <source>Flash</source>
        <translation>闪光灯</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="208"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="209"/>
        <source>Scene Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="210"/>
        <source>Focal Length</source>
        <translation>焦距</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="211"/>
        <source>F Number</source>
        <translation>光圈值</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="212"/>
        <source>Light Source</source>
        <translation>光源</translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="214"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="215"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="216"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/MetaData.qml" line="218"/>
        <source>GPS Position</source>
        <translation>GPS 位置</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="116"/>
        <source>Hide Counter</source>
        <translation>隐藏计数器</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="183"/>
        <source>Hide Filepath, leave Filename</source>
        <translation>隐藏文件路径，只显示文件名</translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="191"/>
        <source>Hide both, Filename and Filepath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/mainview/QuickInfo.qml" line="236"/>
        <source>Filter:</source>
        <extracomment>As in: FILTER images</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickSettings</name>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="46"/>
        <source>Quick Settings</source>
        <translation>快速設置</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="55"/>
        <source>Change settings with one click. They are saved and applied immediately. If you&apos;re unsure what a setting does, check the full settings for descriptions.</source>
        <translation>一鍵改變設置。立即保存并生效。如果你不確定某設置的作用是什么，請查看完整設置了解其描述。</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="77"/>
        <source>Sort by</source>
        <translation>排序</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Natural Name</source>
        <translation>自然排序</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="84"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="161"/>
        <source>Loop through folder</source>
        <translation>循環文件夾</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="182"/>
        <source>Window mode</source>
        <translation>窗口模式</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="192"/>
        <source>Show window decoration</source>
        <translation>顯示窗口裝飾</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="214"/>
        <source>Close on click on background</source>
        <translation>點擊空白區域退出</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="235"/>
        <source>Keep thumbnails visible</source>
        <translation>保持縮略圖可見</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Normal thumbnails</source>
        <translation>普通缩略图</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Dynamic thumbnails</source>
        <translation>動態縮略圖</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="280"/>
        <source>Enable &apos;Quick Settings&apos;</source>
        <translation>啟用‘快速設置’</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="258"/>
        <source>Smart thumbnails</source>
        <translation>只能缩略图</translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>No tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Hide to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="141"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/slidein/QuickSettings.qml" line="305"/>
        <source>Show full settings</source>
        <translation>顯示完整設置</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="68"/>
        <source>Rename File</source>
        <translation>重命名文件</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Rename.qml" line="148"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="67"/>
        <source>Scale Image</source>
        <translation>縮放圖片</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="83"/>
        <source>Current Size:</source>
        <translation>當前大小：</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="112"/>
        <source>Error! Something went wrong, unable to save new dimension...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="136"/>
        <source>New width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="143"/>
        <source>New height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="248"/>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="298"/>
        <source>Scale into new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="315"/>
        <source>Don&apos;t scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="217"/>
        <source>Aspect Ratio</source>
        <translation>寬高比</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Scale.qml" line="284"/>
        <source>Scale in place</source>
        <translation>覆蓋縮放</translation>
    </message>
</context>
<context>
    <name>SettingsItem</name>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="59"/>
        <source>Look and Feel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="72"/>
        <location filename="../qml/settings/SettingsItem.qml" line="124"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="93"/>
        <location filename="../qml/settings/SettingsItem.qml" line="143"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="115"/>
        <source>Thumbnails</source>
        <translation>缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="174"/>
        <source>Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="194"/>
        <source>Other Settings</source>
        <translation>其他设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="219"/>
        <source>Filetypes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="240"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="313"/>
        <source>Restore Default Settings</source>
        <translation>恢复默认设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="326"/>
        <source>Exit and Discard Changes</source>
        <translation>不保存设置并退出</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="344"/>
        <source>Save Changes and Exit</source>
        <translation>保存设置并退出</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="358"/>
        <source>Clean Database</source>
        <translation>清理数据库</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>Do you really want to clean up the database?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="359"/>
        <source>This process might take a little while.</source>
        <translation>此操作会费点时间。</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="360"/>
        <source>Yes, clean is good</source>
        <translation>是的，清除</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="361"/>
        <source>No, don&apos;t have time for that</source>
        <translation>不，谢了</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="368"/>
        <source>Erase Database</source>
        <translation>擦除数据库</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>Do you really want to ERASE the entire database?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This removes every single item in the database! This step should never really be necessarily. After that, every thumbnail has to be newly re-created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="369"/>
        <source>This step cannot be reversed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="370"/>
        <source>Yes, get rid of it all</source>
        <translation>是的，删除所有</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="371"/>
        <source>Nooo, I want to keep it</source>
        <translation>不，留着</translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="378"/>
        <source>Set Default Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="379"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="380"/>
        <source>Yes, please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/SettingsItem.qml" line="381"/>
        <source>Nah, don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Slideshow</name>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="86"/>
        <source>Start a Slideshow</source>
        <translation>放映幻灯片</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="97"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that is played in the background.</source>
        <translation>有几个调节幻灯片的选项，如多久切换图片，图片切换时间，还可以设置背景音乐。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="104"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>如果一切设置好，下次放映幻灯片时可以使用&apos;快速开始&apos;，即跳过这个设置窗口直接开始放映。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="114"/>
        <source>Time in between</source>
        <translation>间隔时间</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="121"/>
        <source>Adjust the time between the images. The time specified here is the amount of time the image will be completely visible, i.e. the transitioning (if set) is not part of this time.</source>
        <translation>调整两张图片之间的时间间隔。这里指定的时间是图像完整显示的时间，也就是说，切换时间(如果进行了设置)不在其中。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="161"/>
        <source>Smooth Transition</source>
        <translation>平滑切换</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="168"/>
        <source>Here you can set, if you want the images to fade into each other, and how fast they are to do that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="182"/>
        <source>No Transition</source>
        <translation>无切换特效</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="198"/>
        <source>Long Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="210"/>
        <source>Shuffle and Loop</source>
        <translation>随机+循环</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="217"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note, that no image will be shown twice before every image has been shown once.</source>
        <translation>如果你想让 PhotoQt 循环显示所有图片(即，显示完最后一张后跳到第一张)，或者让 PhotoQt 随机显示图片，你可以勾选下列其中一个或多个选项。注意，在随机模式下，在未显示完所有图片时不会重复显示某张图片。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="223"/>
        <source>Loop over images</source>
        <translation>循环显示图片</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="229"/>
        <source>Shuffle images</source>
        <translation>随机显示图片</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="241"/>
        <source>Hide Quickinfo</source>
        <translation>隐藏快速信息</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="249"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>根据您的设置，PhotoQt 会在屏幕顶端显示一些信息，例如当前目录位置，文件路径/文件名。你可以临时在这里禁用它们。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="254"/>
        <source>Hide Quickinfos</source>
        <translation>隐藏快速信息</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="266"/>
        <source>Background Music</source>
        <translation>背景音乐</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="273"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>有人喜欢在放映幻灯片的时候听音乐。在这里你可以设置背景音乐文件。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="280"/>
        <source>Enable Music</source>
        <translation>启用音乐</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="310"/>
        <source>Click here to select music file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="344"/>
        <source>Okay, lets start</source>
        <translation>好了，开始吧</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="348"/>
        <source>Wait, maybe later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="352"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Select music file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>Music Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Slideshow.qml" line="366"/>
        <source>All Files</source>
        <translation>所有文件</translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Play Slideshow</source>
        <translation>放映幻灯片</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="34"/>
        <source>Pause Slideshow</source>
        <translation>暂停幻灯片</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="49"/>
        <source>Music Volume:</source>
        <translation>音量：</translation>
    </message>
    <message>
        <location filename="../qml/slidein/SlideshowBar.qml" line="77"/>
        <source>Exit Slideshow</source>
        <translation>退出幻灯片</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully installed</source>
        <translation>PhotoQt 已成功安装</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="74"/>
        <source>PhotoQt was successfully updated</source>
        <translation>PhotoQt 已成功更新</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>Welcome to PhotoQt. PhotoQt is an image viewer, aimed at being fast and reliable, highly customisable and good looking.</source>
        <translation>欢迎使用 PhotoQt。PhotoQt 是一个图片查看器，目标是快捷，稳定，高度可定制，且外观好看。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <source>This app started out more than three and a half years ago, and it has developed quite a bit since then. It has become very efficient, reliable, and highly flexible (check out the settings). I&apos;m convinced it can hold up to the more &apos;traditional&apos; image viewers out there in every way.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Welcome back to PhotoQt. It hasn&apos;t been that long since the last release of PhotoQt. Yet, it changed pretty much entirely, as it now is based on QtQuick rather than QWidgets. A large quantity of the code had to be re-written, while some chunks could be re-used. Thus, it is now more reliable than ever before and overall simply feels well rounded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>Many File Formats</source>
        <translation>大量文件格式</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="123"/>
        <source>PhotoQt can make use of GraphicsMagick, an image library, to display many different image formats. Currently, there are up to 72 different file formats supported (exact number depends on your system)! You can find a list of it in the settings (Tab &apos;Other&apos;). There you can en-/disable different ones and also add custom file endings.</source>
        <translation>PhotoQt 使用了一个名为GraphicsMagic的图像库来支持很多图像格式。目前已支持多达72中图片格式(具体数字取决于你的系统)！你可以在设置 (标签：‘其他’) 中找到这些格式的列表。在哪里可以启用或禁用某些格式，或者添加自定义文件后缀。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>Make PhotoQt your own</source>
        <translation>个性化你的 PhotoQt </translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="160"/>
        <source>PhotoQt has an extensive settings area. By default you can call it with the shortcut &apos;e&apos; or through the dropdown menu at the top edge towards the top right corner. You can adjust almost everything in PhotoQt, and it&apos;s certainly worth having a look there. Each setting usually comes with a little explanation text. Some of the most often used settings can also be conveniently adjusted in a slide-in widget, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Most images store some additional information within the file&apos;s metadata. PhotoQt can read and display a selection of this data. You can find this information in the slide-in window hidden behind the left edge of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>PhotoQt also brings a slideshow feature. When you start a slideshow, it starts at the currently displayed image. There are a couple of settings that can be set, like transition, speed, loop, and shuffle. Plus, you can set a music file that is played in the background. When the slideshow takes longer than the music file, then PhotoQt starts the music file all over from the beginning. At anytime during the slideshow, you can move the mouse cursor to the top edge of the screen to get a little bar, where you can pause/exit the slideshow and adjust the music volume.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>Thumbnails</source>
        <translation>缩略图</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="91"/>
        <location filename="../qml/fadein/Startup.qml" line="92"/>
        <source>Here below you find a short overview of a selection of a few things PhotoQt has to offer, but feel free to skip it and just get started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="198"/>
        <source>What would be an image viewer without thumbnails support? It would only be half as good. Whenever you load an image, PhotoQt loads the other images in the directory in the background (by default, it tries to be smart about it and only loads the ones that are needed). It lines them up in a row at the bottom edge (move your mouse there to see them). There are many settings just for the thumbnails, like, e.g., size, liftup, en-/disabled, type, filename, permanently shown/hidden, etc. PhotoQt&apos;s quite flexible with that.</source>
        <translation>图片查看器如果不支持缩略图会是什么样？只能算一半好！当打开图片时，PhotoQt 会在后台加载同目录下其他图片(默认会自动只加载需要的缩略图)。缩略图在屏幕下方列成一排(将鼠标移动到屏幕底部显示)。关于缩略图有很多设置，例如抬起高度，启用/禁用，类型，文件名，永久显示/隐藏等等。PhotoQt 这方面的设置很灵活。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="223"/>
        <source>One of the many strengths of PhotoQt is the ability to easily set a shortcut for almost anything. Even mouse shortcuts are possible! You can choose from a huge number of internal functions, or you can run any custom script or command.</source>
        <translation>PhotoQt 的另一个强大功能是几乎可以给任何东西设置快捷键。甚至是鼠标快捷键！你可以设置成调用软件内部功能，或是执行自定义脚本、命令。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="262"/>
        <source>Image Information (Exif/IPTC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="299"/>
        <source>Slideshow</source>
        <translation>幻灯片</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>Localisation</source>
        <translation>本地化</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="337"/>
        <source>PhotoQt comes with a number of translations. Many have taken some of their time to create/update one of them (Thank you!). Not all of them are complete... do you want to help?</source>
        <translation>PhotoQt 有几种翻译。很多人参与了创建/更新工作(多谢你们！)。有些翻译尚未完成，或许，你可以帮助翻译！</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="355"/>
        <source>There are many many more features. Best is, you just give it a go. Don&apos;t forget to check out the settings to make PhotoQt YOUR image viewer. Enjoy :-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Startup.qml" line="387"/>
        <source>Okay, I got enough now. Lets start!</source>
        <translation>好了，别再提示了，开始吧！</translation>
    </message>
</context>
<context>
    <name>TabDetails</name>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="55"/>
        <source>Image Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="74"/>
        <source>PhotoQt can display different information of and about each image. The widget for this information is on the left outside the screen and slides in when mouse gets close to it and/or when the set shortcut (default Ctrl+E) is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <translation>PhotoQt 可以显示每张图片的详情。信息框会显示在屏幕左侧，当鼠标放到屏幕边缘(或使用默认快捷键Ctrl+E)时会触发显示。你可以根据喜好，选中下面的选项以禁用鼠标触发。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Trigger Widget on Mouse Hovering</source>
        <translation>鼠标滑动触发</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="109"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut. This can come in handy, if you get annoyed by accidentally opening the widget occasionally.</source>
        <translation>默认显示信息框有两种方式：将鼠标移动到屏幕左边边缘可以让信息框临时弹出来(只要鼠标滞留在框内就会一直显示)，你可以勾选保持打开让其一直显示(但是此设置只对当前对话生效，不会保存)。或者，可以使用快捷键调出。你可以根据个人喜好禁用鼠标触发，这样只有使用快捷键才能调出信息框。如果你觉得经常不小心打开信息框很恼人，请禁用此功能。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="118"/>
        <source>Turn mouse triggering OFF</source>
        <translation>关闭鼠标触发</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>Which items are shown?</source>
        <translation>需要显示哪些信息？</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="131"/>
        <source>PhotoQt can display a number of information about the image (often called &apos;Exif data&apos;&apos;). However, you might not be interested in all of them, hence you can choose to disable some of them here.</source>
        <translation>PhotoQt 支持显示部分图片信息(即 Exif 数据)。但是您可能只需要其中一部分，在这里设置需要显示选项。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="149"/>
        <source>Enable ALL</source>
        <translation>启用所有</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="155"/>
        <source>Disable ALL</source>
        <translation>禁用所有</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="179"/>
        <source>Filesize</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="180"/>
        <source>Dimensions</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="181"/>
        <source>Make</source>
        <translation>制造商</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="182"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="183"/>
        <source>Software</source>
        <translation>软件</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="184"/>
        <source>Time Photo was Taken</source>
        <translation>拍摄时间</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="185"/>
        <source>Exposure Time</source>
        <translation>曝光时间</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="186"/>
        <source>Flash</source>
        <translation>闪光灯</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="187"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="188"/>
        <source>Scene Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="189"/>
        <source>Focal Length</source>
        <translation>焦距</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="190"/>
        <source>F-Number</source>
        <translation>F值</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="191"/>
        <source>Light Source</source>
        <translation>光源</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="192"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="193"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="194"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="195"/>
        <source>GPS Position</source>
        <translation>GPS 位置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Adjusting Font Size</source>
        <translation>调整字体大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="210"/>
        <source>Computers can have very different resolutions. On some of them, it might be nice to increase the font size of the labels to have them easier readable. Often, a size of 8 or 9 should be working quite well...</source>
        <translation>电脑的分辨率可能不同。在有些电脑上加大标签的字体会更好看些。通常情况下，字体大小为 8 或者 9 即可。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Rotating/Flipping Image according to Exif Data</source>
        <translation>根据 Exif 信息旋转/翻转图像</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="271"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly. When asking PhotoQt to always rotate images automatically without asking, it already does so at image load (including thumbnails).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="294"/>
        <source>Never rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="300"/>
        <source>Always rotate/flip images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="305"/>
        <source>Always ask</source>
        <translation>始终询问</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>Online map for GPS</source>
        <translation>GPS 在线地图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabDetails.qml" line="323"/>
        <source>If you&apos;re image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>如果您的照片包含 GPS 信息，那点击位置文本后，会打开系统默认浏览器，在在线地图服务中定位此位置。在这里您可以选择使用哪个在线服务(也欢迎您给我们推荐其他在线地图)</translation>
    </message>
</context>
<context>
    <name>TabFiletypes</name>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="55"/>
        <source>Filetypes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>File Types - Qt</source>
        <translation>文件类型 - Qt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="79"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.&lt;br&gt;If a file ending for one of the formats is missing, you can add it below, formatted like &apos;*.ending&apos; (without single quotation marks), multiple entries seperated by commas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="102"/>
        <source>Extra File Types:</source>
        <translation>附加文件类型：</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="117"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="166"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="214"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="337"/>
        <source>Mark None</source>
        <translation>取消标记所有</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="123"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="172"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="220"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="343"/>
        <source>Mark All</source>
        <translation>标记所有</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>File Types - GraphicsMagick</source>
        <translation>文件类型 - GraphicsMagick</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="143"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. If you prefer not to have one or the other enabled in PhotoQt, you can simply disable individual formats below.&lt;br&gt;There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>File Types - GraphicsMagick (requires Ghostscript)</source>
        <translation>文件类型 - GraphicsMagick (需要Ghostscript)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="191"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and work. However, they require Ghostscript to be installed on the system.</source>
        <translation>下方是 GraphicsMagick 支持的格式，并且通过了测试。但是您的系统必须安装 Ghostscript。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>File Types - Other tools required</source>
        <translation>文件类型 - 需要其他工具</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation>下列文件类型是通过第三方工具支持的。你必须先安装他们才能支持这些文件类型。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>Note</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="238"/>
        <source>If an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation>如果一个图片格式也被 GraphicsMagick/Qt 支持，那么PhotoQt 会优先选用第三方工具(如果启用了)。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <source>Gimp&apos;s XCF file format.</source>
        <extracomment>&apos;Makes use of&apos; is in connection with an external tool (i.e., it &apos;makes use of&apos; tool abc)</extracomment>
        <translation>Gimps 的 xcf 文件格式。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="270"/>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Makes use of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="292"/>
        <source>Adobe Photoshop PSD and PSB.</source>
        <translation>Adobe Photoshop PSD 及 PSB。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>File Types - GraphicsMagick (Untested)</source>
        <translation>文件类型 - GraphicsMagick (未测试)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>The following file types are generally supported by GraphicsMagick, but I wasn&apos;t able to test them in PhotoQt (due to lack of test images). They might very well be working, but I simply can&apos;t say. If you decide to enable some of the, the worst that could happen ist, that you see an error image instead of the actual image.</source>
        <translation>下列文件格式是被 GraphicsMagick 支持的，但是我无法通过 PhotoQt 测试(因为缺少样片)。我不确定能否完美支持。你可以选择启用其中某些格式，最坏的情况就是无法显示图片，只会显示一个错误信息。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabFiletypes.qml" line="314"/>
        <source>If you happen to have an image in one of those formats and don&apos;t mind sending it to me, that&apos;d be really cool...</source>
        <translation>如果你刚好有属于下列格式的图片，且不介意发送给我，我会相当感激。。。</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelAdvanced</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="54"/>
        <source>Advanced Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>Background of PhotoQt</source>
        <translation>PhotoQt 背景</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="67"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above.&lt;br&gt;Note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="91"/>
        <source>Use (half-)transparent background</source>
        <translation>使用(半)透明背景</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="97"/>
        <source>Use faked transparency</source>
        <translation>使用假透明</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="102"/>
        <source>Use custom background image</source>
        <translation>使用自定义背景图像</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="107"/>
        <source>Use one-coloured, non-transparent background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="164"/>
        <source>No image selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="188"/>
        <source>Scale to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="194"/>
        <source>Scale and Crop to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="199"/>
        <source>Stretch to fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="204"/>
        <source>Center image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="209"/>
        <source>Tile image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Background/Overlay Color</source>
        <translation>背景/覆盖层颜色</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="228"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>在这里你可以调整 PhotoQt 的背景颜色(未被图片覆盖住的区域)。当使用纯色背景或者背景图片时，你还可以设置一个 Alpha 值，即覆盖层颜色的透明度。当使用图片或假透明时，所谓的透明只是在图片上加一个半透明的颜色，看起来效果像是透明。当你未设置纯色背景或背景图片时，此颜色会成为非透明背景。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="259"/>
        <source>Red:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="281"/>
        <source>Green:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="303"/>
        <source>Blue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="325"/>
        <source>Alpha:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="387"/>
        <source>Preview colour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Border Around Image</source>
        <translation>图像边缘</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="410"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels (looks better). Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>无论你什么时候打开图片，默认图片都不会全屏显示，即图片不会延伸到屏幕的边缘去。相反图像和屏幕边沿间有一小段空隙(更好看)。这里你可以调整此空隙的宽度(设为 0 表示禁用)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Close on Click in empty area</source>
        <translation>点击空白区域退出</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. It can be a nice feature, PhotoQt will feel even more like a &quot;floating layer&quot;. However, you might at times close PhotoQt accidentally.</source>
        <translation>此选项可使 PhotoQt 像很多网页中的 JS 图片查看器一样。点击图片外的空白区域就关闭程序。这是个很不错的功能，使得 PhotoQt 像一个 ”悬浮层 一样。但是，你也有可能意外地关闭掉程序。。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="469"/>
        <source>Note: If you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>注意：如果你已将‘点击鼠标’作为快捷键绑定了，那此选项不会有任何作用。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="475"/>
        <source>Close on click in empty area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>Looping Through Folder</source>
        <translation>循环整个文件夹</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="488"/>
        <source>When you load the last image in a directory and select &apos;Next&apos;, PhotoQt automatically jumps to the first image (and vice versa: if you select &apos;Previous&apos; while having the first image loaded, PhotoQt jumps to the last image). Disabling this option makes PhotoQt stop at the first/last image (i.e. selecting &apos;Next&apos;/&apos;Previous&apos; will have no effect in these two special cases).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="494"/>
        <source>Loop through folder</source>
        <translation>循环文件夹</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Smooth Transition</source>
        <translation>平滑切换</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="507"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced by the new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="526"/>
        <source>No Transition</source>
        <translation>无切换特效</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="546"/>
        <source>Long Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Menu Sensitivity</source>
        <translation>菜单灵敏度</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="564"/>
        <source>Here you can adjust the sensitivity of the drop-down menu. The menu opens when your mouse cursor gets close to the right side of the upper edge. Here you can adjust how close you need to get for it to open.</source>
        <translation>这里你可以调整下滑菜单的触发灵敏度。就是那个鼠标滑动到屏幕右上方弹出的菜单。你可以设置离边沿多远就触发。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="583"/>
        <source>Low Sensitivity</source>
        <translation>低灵敏度</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="603"/>
        <source>High Sensitivity</source>
        <translation>高灵敏度</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation>鼠标滑轮灵敏度</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="621"/>
        <source>Here you can adjust the sensitivity of the mouse wheel. For example, if you have set the mouse wheel up/down for switching back and forth between images, then a lower sensitivity means that you will have to scroll further for triggering a shortcut. Per default it is set to the highest sensitivity, i.e. every single wheel movement is evaluated.</source>
        <translation>在这里你可以调整鼠标滚轮的敏感度。例如，如果，如果你将鼠标滚轮(上/下)设置为上翻/下翻图片，那么低灵敏度意味着你需要滑很多下才能触发翻动动作。默认设置为最高灵敏度，例如，滚轮滑动一次表示一个动作。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="640"/>
        <source>Very sensitive</source>
        <translation>非常灵敏</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="660"/>
        <source>Not at all sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>Remember per session</source>
        <translation>记住每个会话</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="678"/>
        <source>If you would like PhotoQt to remember the rotation/flipping and/or zoom level per session (not permanent), then you can enable it here. If not set, then every time a new image is displayed, it is displayed neither zoomed nor rotated nor flipped (one could say, it is displayed &apos;normal&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="697"/>
        <source>Remember Rotation/Flip</source>
        <translation>记住旋转/翻转</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="702"/>
        <source>Remember Zoom Level</source>
        <translation>记住缩放</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation and Window Geometry</source>
        <translation>动画和窗口位置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="748"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Animation of fade-in widgets (like, e.g., Settings or About Widget)</source>
        <translation>淡入控件的动画(例如“设置框”，“关于”对话框)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation>保存及恢复窗口位置：在退出 PhotoQt时，将窗口的大小和位置进行保存，在下次启动的时候恢复到此设置。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="719"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="738"/>
        <source>Animate all fade-in elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelAdvanced.qml" line="743"/>
        <source>Save and restore window geometry</source>
        <translation>保存及恢复窗口位置</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeelBasic</name>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Sort Images</source>
        <translation>排序图片</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Here you can adjust, how the images in a folder are supposed to be sorted. You can sort them by Filename, Natural Name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), File Size, and Date. Also, you can reverse the sorting order from ascending to descending if wanted.</source>
        <translation>在这里你可以设置文件夹中的图片如何排序。可以按照文件名、自然排序(例如，file10.jpg 会跟在 file9.jpg 后，而不是file1.jpg 之后)，文件大小，日期排序。另外，你也可以选择降序排序。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="65"/>
        <source>Hint: You can also change this setting very quickly from the &apos;Quick Settings&apos; window, hidden behind the right screen edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="90"/>
        <source>Sort by:</source>
        <translation>排序：</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Natural Name</source>
        <translation type="unfinished">自然排序</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="98"/>
        <source>Filesize</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="105"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="113"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>Window Mode</source>
        <translation>窗口模式</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="130"/>
        <source>PhotoQt is designed with the space of a fullscreen app in mind. That&apos;s why it by default runs as fullscreen. However, some might prefer to have it as a normal window, e.g. so that they can see the panel.</source>
        <translation>PhotoQt 起初就设计成一个全屏应用，所以默认以全屏方式运行。但是有人或许喜欢普通的窗口模式，这样他们就能看到任务栏。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="151"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>以窗口模式运行 PhotoQt </translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="159"/>
        <source>Show Window Decoration</source>
        <translation>显示窗口装饰</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>Hide to Tray Icon</source>
        <translation>隐藏到系统托盘</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="174"/>
        <source>When started PhotoQt creates a tray icon in the system tray. If desired, you can set PhotoQt to minimise to the tray instead of quitting. This causes PhotoQt to be almost instantaneously available when an image is opened.&lt;br&gt;It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>No tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Hide to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="182"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>Closing &apos;X&apos; (top right)</source>
        <translation>关闭 &apos;X&apos; (右上方)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="193"/>
        <source>There are two looks for the closing &apos;x&apos; at the top right: a normal &apos;x&apos;, or a slightly more fancy &apos;x&apos;. Here you can switch back and forth between both of them, and also change their size. If you prefer not to have a closing &apos;x&apos; at all, see below for an option to hide it.</source>
        <translation>右上方的关闭键有两种外观，一种是普通的 ‘X‘，另一种略华丽。你可以在这里进行设置，也可以改变他们的大小。如果你不想看到 &apos;X&apos;，也能在下面找到相应的选项来隐藏。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="216"/>
        <source>Normal Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="223"/>
        <source>Fancy Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="249"/>
        <source>Small Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="264"/>
        <source>Large Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>Fit Image in Window</source>
        <translation>图片适应窗口大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="280"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can zoom those images to make them fir into the window. However, keep in mind, that such images will look pixelated to a certain degree (depending on each image).</source>
        <translation>如果图片的尺寸和屏幕的尺寸差不多，PhotoQt 可以将这些图片缩放到适应窗口大小。但是，请记住这样的图片在特定角度看会有像素感(取决于图片)。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="287"/>
        <source>Fit Images in Window</source>
        <translation>图片适应窗口大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Hide Quickinfo (Text Labels)</source>
        <translation>隐藏快速信息 (文本标签)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="300"/>
        <source>Here you can hide the text labels shown in the main area: The Counter in the top left corner, the file path/name following the counter, and the &quot;X&quot; displayed in the top right corner. The labels can also be hidden by simply right-clicking on them and selecting &quot;Hide&quot;.</source>
        <translation>这里您可以选择显示在主区域的文本标签：左上角的计数器，计数器旁边是文件路径/文件名，右上方是一个 ”x“ 按钮。也可以通过右键点击这些标签，选择”隐藏“来隐藏。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="321"/>
        <source>Hide Counter</source>
        <translation>隐藏计数器</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="326"/>
        <source>Hide Filepath (Shows only file name)</source>
        <translation>隐藏文件路径(只显示文件名)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="331"/>
        <source>Hide Filename (Including file path)</source>
        <translation>隐藏文件名(包括路径)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabLookAndFeelBasic.qml" line="336"/>
        <source>Hide &quot;X&quot; (Closing)</source>
        <translation>隐藏 ”X“ (关闭按钮)</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="55"/>
        <source>Other Settings</source>
        <translation>其他设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>Choose Language</source>
        <translation>选择语言</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="69"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>本软件已有多个语言版本，感谢为 PhotoQt 贡献翻译的人！</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>Quick Settings</source>
        <translation>快速设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="119"/>
        <source>The &apos;Quick Settings&apos; is a widget hidden on the right side of the screen. When you move the cursor there, it shows up, and you can adjust a few simple settings on the spot without having to go through this settings dialog. Of course, only a small subset of settings is available (the ones needed most often). Here you can disable the dialog so that it doesn&apos;t show on mouse movement anymore.</source>
        <translation>‘快速设置’ 是一个隐藏在屏幕右侧的控件。当你鼠标移动到上面时会显示出来，然后可以简单地更改一些设置，而无需用这个复杂的设置对话框。当然只有很少的几个子选项可更改(最常用的那些)。你可以在此处禁用此控件，这样鼠标滑动到右面时便不会弹出设置框。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="129"/>
        <source>Show &apos;Quick Settings&apos; on mouse hovering</source>
        <translation>鼠标滑动到上面时显示‘快速设置’</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Adjust Context Menu</source>
        <translation>调整菜单文本</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="142"/>
        <source>Here you can adjust the context menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>在这里你可以调整菜单的文字。操作很简单，拖放条目，编辑，添加或移除～</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="183"/>
        <source>Executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="199"/>
        <source>Menu Text</source>
        <translation>菜单说明</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="223"/>
        <source>Add new context menu entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOther.qml" line="231"/>
        <source>(Re-)set automatically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabOtherContext</name>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="116"/>
        <source>Click here to drag</source>
        <translation>点击这里拖动</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabOtherContext.qml" line="186"/>
        <source>quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="54"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="73"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available function you can either double click on the tile or click the &quot;+&quot; button. This automatically opens another widget where you can set a key combination.</source>
        <translation>在这里您可以调整快捷键，添加/移除项目，或改变快捷键。快捷键被分成 5 类，四类用于软件内部，一类附加的用于外部命令。下放靠右的窗格都包含所有可用的命令。你可以双击项目的标题或点 ”+“ 号添加快捷键。这会自动打开设置快捷键的窗口。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="89"/>
        <source>Set default shortcuts</source>
        <translation>恢复默认</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="96"/>
        <source>Navigation</source>
        <translation>导航</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Open New File</source>
        <translation>打开新文件</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Filter Images in Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Next Image</source>
        <translation>下一张</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Previous Image</source>
        <translation>上一张</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to first Image</source>
        <translation>前往第一张图片</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Go to last Image</source>
        <translation>前往最后一张图片</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Hide to System Tray</source>
        <translation>隐藏到系统托盘</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="99"/>
        <source>Quit PhotoQt</source>
        <translation>退出 PhotoQt</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="104"/>
        <source>Image</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Zoom to Actual Size</source>
        <translation>缩放到真实大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Zoom</source>
        <translation>重置缩放</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Right</source>
        <translation>向右选择</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Rotate Left</source>
        <translation>向左旋转</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Reset Rotation</source>
        <translation>重设旋转</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Horizontally</source>
        <translation>水平翻转</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Flip Vertically</source>
        <translation>垂直翻转</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="107"/>
        <source>Scale Image</source>
        <translation>缩放图片</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="112"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Rename File</source>
        <translation>重命名文件</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Delete File</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Copy File to a New Location</source>
        <translation>复制文件到新位置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="115"/>
        <source>Move File to a New Location</source>
        <translation>移动文件到新位置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="120"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Interrupt Thumbnail Creation</source>
        <translation>中断缩略图创建进程</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Reload Thumbnails</source>
        <translation>重新载入缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Hide/Show Exif Info</source>
        <translation>隐藏/显示 Exif 信息</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Context Menu</source>
        <translation>显示文本菜单</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Show Settings</source>
        <translation>显示设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>放映幻灯片(快速开始)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>About PhotoQt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="123"/>
        <source>Set as Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="128"/>
        <source>Extern</source>
        <translation>附加</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcuts.qml" line="132"/>
        <source>EXTERN</source>
        <extracomment>Is the shortcut tile text for EXTERNal shortcuts</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabShortcutsCategories</name>
    <message>
        <location filename="../qml/settings/TabShortcutsCategories.qml" line="32"/>
        <source>Category:</source>
        <translation>分类：</translation>
    </message>
</context>
<context>
    <name>TabShortcutsTilesAvail</name>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="98"/>
        <source>key</source>
        <extracomment>tile text for KEY shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation>键</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabShortcutsTilesAvail.qml" line="154"/>
        <source>mouse</source>
        <extracomment>tile text for MOUSE shortcut. If multiple translations possible, please try to stick to a short one..</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabThumbnailsAdvanced</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="52"/>
        <source>Advanced Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Change Thumbnail Position</source>
        <translation>改变缩略图位置</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="65"/>
        <source>Per default the bar with the thumbnails is shown at the lower edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge, so that&apos;s what can be changed here.</source>
        <translation>默认缩略图在屏幕下放显示。有人会喜欢让其在屏幕上方显示，在这里更改吧～</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="86"/>
        <source>Show at lower edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="93"/>
        <source>Show at upper edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>Filename? Dimension? Or both?</source>
        <translation>文件名？尺寸？或两者兼得？</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="132"/>
        <source>Write Filename</source>
        <translation>添加文件名</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>Use file-name-only Thumbnails</source>
        <translation>使用‘仅文件名式’缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="207"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a file-name-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>如果你不想让 PhotoQt 一直在后台加载缩略图文件，但是又想获得比较好的导航体验，你可以设置一个”仅文件名式“缩略图，PhotoQt 会仅将文件名放到窗格中。你可以调整此处文本的字体大小。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="110"/>
        <source>When thumbnails are displayed at the top/bottom, PhotoQt usually writes the filename on them (if not disabled). You can also use the slider below to adjust the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="213"/>
        <source>Use filename-only thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>Disable Thumbnails</source>
        <translation>禁用缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="279"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This option can also be toggled remotely via command line (run &apos;photoqt --help&apos; for more information on that). This might increase the speed of PhotoQt a good bit, however, navigating through a folder might be a little harder without thumbnails.</source>
        <translation>如果你完全不想要缩略图，可以在这里禁用。此选项可通过命令行切换(运行 &apos;photoqt --help&apos; 查看)。这能增加 PhotoQt 的运行速度，但是如果没有缩略图，浏览一个文件夹会比较费劲(你不知道哪张在哪)。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="287"/>
        <source>Disable Thumbnails altogether</source>
        <translation>完全禁用缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnail Cache</source>
        <translation>缩略图缓存</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Thumbnails can be cached in two different ways:&lt;br&gt;1) File Caching (following the freedesktop.org standard) or&lt;br&gt;2) Database Caching (better performance and management, default option).</source>
        <translation>缩略图使用两种形式的缓存：&lt;br&gt;1)文件缓存(遵循 freedesktop.org 的标准)，或&lt;br&gt;2)数据库缓存(性能和可管理性更好，默认的选项)。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <translation>两种方式各有千秋：</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file. However, it&apos;s not possible to check for obsolete thumbnails (thus this may lead to many unneeded thumbnail files).</source>
        <translation>文件缓存根据 freedesktop.org 的标准完成，不同的应用可以分享相同文件的缓存。但是无法检查无用的缩略图(因此会产生大片没用的缩略图文件)</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it brings a slightly better performance, and it allows a better handling of existing thumbnails (e.g. deleting obsolete thumbnails).</source>
        <translation>数据库缓存没有像文件缓存那样的共享优势(因此 PhotoQt 的每个缓存都需要自己生成)，但是性能比较高，并允许对已有的缩略图进行管理(例如，删除无用的)。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>PhotoQt works with either option, though the second way is set as default.</source>
        <translation>PhotoQt 必须使用其中一种，默认使用第二个</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="302"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, that does affect the performance and usability of PhotoQt, since thumbnails have to be newly re-created every time they are needed.</source>
        <translation>虽然建议大家使用其中一种，不过你还是可以选择完全禁用。不过这会影响 PhotoQt 的性能和易用性，因为每次需要时都得重新创建缩略图。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="327"/>
        <source>Enable Thumbnail Cache</source>
        <translation>启用缩略图缓存</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="348"/>
        <source>File Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="354"/>
        <source>Database Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="379"/>
        <source>Current database filesize:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="405"/>
        <source>Entries in database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="426"/>
        <source>CLEAN UP database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsAdvanced.qml" line="438"/>
        <source>ERASE database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabThumbnailsBasic</name>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="52"/>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Thumbnail Size</source>
        <translation>缩略图大小</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="65"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but with different screen resolutions it might be nice to have them larger/smaller.</source>
        <translation>在这里可以调整缩略图的大小。允许值在 20 到 256 像素之间。默认为 80 像素，但根据屏幕分辨率不同，调大/调小会更好看。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>Spacing Between Thumbnail Images</source>
        <translation>缩略图间距</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="126"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them, however exactly that can be changed here.</source>
        <translation>缩略图以行的形式显示在屏幕底端或顶端(具体看设置而定)。相互之间紧挨着，默认缩略图之间没用空隙，你可以在这里更改间距。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>Lift-up of Thumbnail Images on Hovering</source>
        <translation>弹出缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="189"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels (default 10). Here you can increase/decrease this value according to your personal preference.</source>
        <translation>当鼠标放到缩略图上方时，缩略图会弹升一定像素(默认 10 像素)。这里你可以根据喜好增加/减小此值。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Keep Thumbnails Visible</source>
        <translation>保持缩略图可见</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="251"/>
        <source>Per default the Thumbnails slide out over the edge of the screen. Here you can force them to stay visible. The big image is shrunk to fit into the empty space. Note, that the thumbnails will be hidden (and only shown on mouse hovering) once you zoomed the image in/out. Resetting the zoom restores the original visibility of the thumbnails.</source>
        <translation>默认缩略图只有在鼠标滑动到屏幕底端时才显示出来。你可以让它们永久显示。比较大的图片会缩放显示在空白区域。注意，缩略图会在你缩放图片的时候隐藏(只有在鼠标滑动底部时才会再次显示)。重置缩放能让缩略图恢复显示。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="259"/>
        <source>Keep Thumnails Visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic Thumbnail Creation</source>
        <translation>动态创建缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Dynamic thumbnail creation means, that PhotoQt only sets up those thumbnail images that are actually needed, i.e. it stops once it reaches the end of the visible area and sits idle until you scroll left/right.</source>
        <translation>动态创建缩略图的意思是， PhotoQt 只会创建真实需要的缩略图，即只创建够在屏幕显示的缩略图，而不会为文件夹所有文件创建，当你滚动图片时继续创建新的。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Smart thumbnails are similar in nature. However, they make use of the fast, that once a thumbnail has been created, it can be loaded very quickly and efficiently. It also first loads all of the currently visible thumbnails, but it doesn&apos;t stop there: Any thumbnails (even if invisible at the moment) that once have been created are loaded. This is a nice compromise between efficiency and usability.</source>
        <translation>智能缩略图很类似于自然缩略图。但是更加快捷，一旦创建缩略图，则能高速地被载入。它会一次性加载所有可视缩略图，但又不仅如此：只要是曾经创建过的缩略图(即使不在可视区域内)也都会被加载。这是快捷和易用性兼得的结果。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="274"/>
        <source>Enabling either the smart or dynamic option is recommended, as it increases the performance of PhotoQt significantly, while preserving the usability.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="295"/>
        <source>Normal Thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="301"/>
        <source>Dynamic Thumbnails</source>
        <translation>动态缩略图</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="307"/>
        <source>Smart Thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>Always center on Active Thumbnail</source>
        <translation>活动缩略图始终居中显示</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="325"/>
        <source>If this option is set, then the active thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation>如果设置此选项，则活动缩略图(即当前显示图片的缩略图)将会保持显示在缩略图栏的中间。如果不设置此项，则活动缩略图仅仅会显示在可见区域中，不会居中。</translation>
    </message>
    <message>
        <location filename="../qml/settings/TabThumbnailsBasic.qml" line="333"/>
        <source>Center on Active Thumbnails</source>
        <translation>居中显示活动缩略图</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="799"/>
        <source>Okay, do it!</source>
        <translation>好的，確定！</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="804"/>
        <source>Nooo, don&apos;t!</source>
        <translation>不，不要！</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="106"/>
        <source>Window Manager</source>
        <translation>窗口管理器</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="260"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="401"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <translation>有以下圖像伸縮類型選項</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="86"/>
        <source>Set as Wallpaper:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="114"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="192"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="219"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="250"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="337"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="350"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="505"/>
        <source>The wallpaper can be set to either of the available monitors (or any combination).</source>
        <translation>可將壁紙設置到任意可用顯示器(或任意組合)。</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="366"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="521"/>
        <source>Screen #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="483"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console &gt; Add-ons &gt; Modules &gt; System.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="494"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="556"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="576"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="578"/>
        <location filename="../qml/fadein/Wallpaper.qml" line="579"/>
        <source>Workspace #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="636"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="647"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="658"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="669"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.&lt;br&gt;This is intended particularly for window managers that don&apos;t natively support wallpapers (e.g., like Openbox).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="685"/>
        <source>Use &apos;feh&apos;</source>
        <extracomment>feh is an application, do not translate</extracomment>
        <translation>使用 &apos;feh&apos;</translation>
    </message>
    <message>
        <location filename="../qml/fadein/Wallpaper.qml" line="692"/>
        <source>Use &apos;nitrogen&apos;</source>
        <extracomment>nitrogen is an application, do not translate</extracomment>
        <translation>使用 &apos;nitrogen&apos;</translation>
    </message>
</context>
</TS>
